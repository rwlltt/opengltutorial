#shader vertex
#version 330 core

layout(location = 0) in vec4 vPos;
layout(location = 1) in vec2 texCoord;
layout(location = 2) in vec3 vCol;

out vec3 v_color;
out vec2 v_TexCoord;

uniform mat4 MVP;
uniform mat4 u_MVP;

void main()
{
	gl_Position = u_MVP * vPos;
	//gl_Position = MVP * vec4(vPos, 0.0, 1.0);
	//gl_Position = vec4(vPos, 0.0, 1.0);
	v_color = vCol;
	v_TexCoord = texCoord;
};

#shader fragment
#version 330 core

layout(location = 0) out vec4 color;

in vec3 v_color;
in vec2 v_TexCoord;

uniform sampler2D u_Texture;

void main()
{
	vec4 texColor = texture(u_Texture, v_TexCoord);
	color = texColor;
};