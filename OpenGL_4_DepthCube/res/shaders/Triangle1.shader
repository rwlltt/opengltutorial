#shader vertex
#version 330 core

layout(location = 0) in vec4 position;
void main()
{
    gl_Position = position;
}

#shader fragment
#version 330 core

out vec4 outputColor;
void main()
{
    float lerpValue = gl_FragCoord.y / 500.0f;
    outputColor = mix(vec4(0.6f, 0.7f, 1.0f, 1.0f), vec4(0.3f, 0.3f, 0.2f, 1.0f), lerpValue);
}