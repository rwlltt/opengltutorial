#shader vertex
#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 color;

smooth out vec4 theColor;

uniform vec3 offset;
uniform mat4 perspectiveMatrix;

void main()
{
	vec4 cameraPos = position + vec4(offset.x, offset.y, offset.z, 0.0);

	gl_Position = perspectiveMatrix * cameraPos;
	theColor = color;
}


#shader fragment
#version 330 core

smooth in vec4 theColor;

out vec4 outputColor;

void main()
{
    outputColor = theColor;
}