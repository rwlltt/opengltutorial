#ifndef TUT_TEST_TRIANGLE
#define TUT_TEST_TRIANGLE

#include <Shader.h>
#include <VertexBuffer.h>
#include <VertexArray.h>
#include <VertexBufferLayout.h>
#include "Test.h"

namespace test {

	class TestTriangle : public Test {
	private:
        float vertexPositions[12];
        Shader shader;
        VertexBuffer vb;
        VertexArray va;
        VertexBufferLayout layout;
        float fXOffset = 0.0f, fYOffset = 0.0f;

    public:
        TestTriangle();
		~TestTriangle();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;

	private:
        void ComputePositionOffsets(float fElapsedTime);
        void AdjustVertexData();

    };


}

#endif /* TUT_TEST_TRIANGLE*/

