#ifndef TUT_TEST_WEDGEDEPTH
#define TUT_TEST_WEDGEDEPTH

#include <Shader.h>
#include <VertexBuffer.h>
#include <VertexArray.h>
#include <VertexBufferLayout.h>
#include "Test.h"

namespace test {

	class Wedge : public Test {
	private:
        float vertexPositions[252];
        unsigned int /*GLshort*/ indexData[24];
        Shader shader;
        VertexBuffer vb;
        VertexArray va;
        VertexArray va2;
        VertexBufferLayout layout;
        IndexBuffer ib;
        float deltaTime = 0.0f;

    public:
        Wedge();
		~Wedge();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;
		Shader & UpdateShader() { return shader;};

	private:
        void ComputePositionOffsets(float fElapsedTime);
        void AdjustVertexData();

    };


}

#endif /* TUT_TEST_WEDGEDEPTH*/

