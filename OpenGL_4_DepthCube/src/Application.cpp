
#include "Application.h"
#include "Renderer.h"
#include "Shader.h"
#include "glm/gtc/matrix_transform.hpp"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
#include <chrono>

#include <future>
#include <tests/TestTriangle.h>
#include <tests/TestSquare.h>
#include <tests/Wedge.h>
#include <tests/WedgeBaseIndex.h>

bool Application::bDepthClampingActive = false;
Application::Application()
{
}

Application::~Application()
{
	std::cout << "App dest\n";
}

void Application::Init() {
	signal(SIGINT, signal_handler); // Catch Ctrl c to stop abrubt end.

	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		throw std::exception("GlfwInit failed");

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(640, 480, "Simple Depth Wedge", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		throw std::exception("GlfwCreate window failed");
	}
	glfwSetKeyCallback(window, key_callback);
    //glfwSetWindowSizeCallback(window, reshape_callback);
	glfwMakeContextCurrent(window);
	auto result = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	GLCall(std::cout << glGetString(GL_VERSION) << "\n");
	glfwSwapInterval(1);

	try {

		WSADATA wsaData;
		if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0) {
			fprintf(stderr, "WSAStartup failed.\n");
			throw std::exception("No WinSock");
		}
		
		m_user = std::make_unique<User>(m_user_name);
		Model::Instance().create_tables();
		SendAppMessage("InitAppRequest");
	}
	catch (const std::exception& e) {
		throw std::exception(e.what());
	}

}

void Application::Run() {
	{

		GLCall(glEnable(GL_BLEND));
		GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

		Renderer renderer;

		ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO(); (void)io;
		ImGui::StyleColorsDark();
		ImGui_ImplGlfw_InitForOpenGL(window, true);
		ImGui_ImplOpenGL3_Init(glsl_version);

		std::string message_in{ "" };

		std::map<std::string, std::future<std::unique_ptr<Response>>> mapped;
		float ratio;
		int width, height;

        test::TestTriangle triangle;
        test::Wedge orthoCube;
        test::WedgeBaseIndex wedgeBaseIndex;
        test::TestSquare squares;

        GLCall(glEnable(GL_CULL_FACE));
        GLCall(glCullFace(GL_BACK));
        GLCall(glFrontFace(GL_CW));

        GLCall(glEnable(GL_DEPTH_TEST));
        GLCall(glDepthMask(GL_TRUE));
        GLCall(glDepthFunc(GL_LEQUAL));
        GLCall(glDepthRange(0.0f, 1.0f));


        // Store start time of drawings
        auto startTimer = std::chrono::high_resolution_clock::now();

		//bool show_demo_window = false;
		while (true)
		{
			if (glfwWindowShouldClose(window)) {
                std::cout << "Window Close\n";
				SendAppMessage("Quit");
				break;
			}

			{
				Lock lock;
				if (messages.size() > 0) {
					message_in = messages.front();
					messages.pop();
					if (message_in.compare("Quit") == 0)
						break;
				}
			}
			if (message_in.compare("") != 0) {
				if (mapped.count(message_in) == 1) { // A completed async task which is complete and is mapped
					if (mapped[message_in].valid()) {
						auto res = mapped[message_in].get();
						m_user->SetResponse(std::move(res));
					}
				}
				else {
					mapped[message_in + "Done"] = DoRequest(m_user_name, message_in);
				}
			}
			// Calculate deltatime from epoch
            auto ticker = std::chrono::high_resolution_clock::now();
            std::chrono::duration<float> diff = ticker-startTimer;
            float deltaTime  = diff.count();


            glfwGetFramebufferSize(window, &width, &height);
			ratio = width / (float)height;
			renderer.ViewPort(width, height);
			renderer.ShaderPerspective(orthoCube.UpdateShader());
            renderer.ShaderPerspective(wedgeBaseIndex.UpdateShader());

			renderer.Clear();

			// For all mapped users do update render and imguiupdate
            m_user->OnUpdate(0.0f);
            m_user->OnRender();

            triangle.OnUpdate(deltaTime);
			triangle.OnRender();

            orthoCube.OnUpdate(deltaTime);
            orthoCube.OnRender();

            wedgeBaseIndex.OnUpdate(deltaTime);
            wedgeBaseIndex.OnRender();

            squares.OnUpdate( deltaTime);
            squares.OnRender();

			// Start the Dear ImGui frame
			ImGui_ImplOpenGL3_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();

            // Render the VertexBuffer with Shader omgui controls
			//m_user->OnImGuiRender(); // TestColour
            //squares.OnImGuiRender(); // TestSquares gui sliders
            wedgeBaseIndex.OnImGuiRender(); // TestSquares gui sliders

            // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()!
            // You can browse its code to learn more about Dear ImGui!).
            //ImGui::ShowDemoWindow(&show_demo_window);
			{
			  ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			}

			ImGui::Render();
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

            //if(available)
            //glEndQuery(GL_TIME_ELAPSED);

            glfwSwapBuffers(window);
			glfwPollEvents();
		}

		glfwSetWindowShouldClose(window, 1);
	}
	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwDestroyWindow(window);
	glfwTerminate();
}

std::future<std::unique_ptr<Response>> Application::DoRequest(const std::string &user, const std::string &request)
{
	auto req = std::unique_ptr<Request>(RequestFactory::Instance().CreateObject(request));
	req->SetUser(user);
	std::future<std::unique_ptr<Response>> result = std::async(
		std::launch::async, [](std::unique_ptr<Request> req, const std::string &user, const std::string &request) {
		                    auto res = req->DoRequest(user);
							res->SetRequest(std::move(req));
							res->Display();
							TheApp::Instance().SendAppMessage(request +"Done");
							return res;
	                  }, std::move(req), user, request);
	return result;
}

void Application::SendAppMessage(const std::string &request) {
	Lock lock;
	messages.push(request);
}
