
#include "Renderer.h"
#include <iostream>
#include <glm/gtc/type_ptr.hpp>


void  GLClearError() {
	while (glGetError() != GL_NO_ERROR);
}

bool GLLogCall(const char* function, const char* file, int line) {
	while (GLenum error = glGetError()) {
		std::cout << "[OpenGl Error](" << std::hex << error << std::dec << "): " <<
			function << " " << file << ":" << line << '\n';
		return false;
	}
	return true;
}

Renderer::Renderer(): fFrustumScale{1.0f}, fzNear{0.5f}, fzFar{3.0f} {
    memset(theMatrix, 0, sizeof(float) * 16);
    theMatrix[0] = fFrustumScale;
    theMatrix[5] = fFrustumScale;
    theMatrix[10] = (fzFar + fzNear) / (fzNear - fzFar);
    theMatrix[14] = (2 * fzFar * fzNear) / (fzNear - fzFar);
    theMatrix[11] = -1.0f;
}

Renderer::~Renderer() {

}

void Renderer::ViewPort(int width, int height)
{
    float ratio = width / (float)height;
    int widthR, heightR;
    // Non stretch view of graphics render
    if (ratio > 1) {
        widthR = height;
        heightR = height;
    } else {
        widthR = width;
        heightR = width;
    }
    // Centre in the view screen width and height of window
    int centrePointX = width/2;
    int centrePointY = height/2;
    int vx,vy;
    vx = centrePointX - widthR/2;
    vy = centrePointY - heightR/2;
	//GLCall(glViewport(vx, vy, (GLsizei)widthR, (GLsizei)heightR));
	// Change to the view port with perspective aspect in shader perperpective
	theMatrix[0] = fFrustumScale / ((widthR-vx) / (float)(heightR-vy));
    theMatrix[5] = fFrustumScale;
    GLCall(glViewport(vx, vy, (GLsizei)widthR, (GLsizei)heightR));

//    theMatrix[0] = fFrustumScale / (width / (float)height);
//    theMatrix[5] = fFrustumScale;
//    GLCall(glViewport(0, 0, (GLsizei)width, (GLsizei)height));
}

void Renderer::ShaderPerspective(Shader &shader) {
    glm::mat4 gCWMatrix = glm::make_mat4(theMatrix);
    shader.Bind();
    shader.SetUniformMat4f("perspectiveMatrix", gCWMatrix);
    shader.UnBind();
}

void Renderer::Clear() const
{
    GLCall(glClearColor(0.0f, 0.0f, 0.0f, 0.0f));
    GLCall(glClearDepth(1.0f));
	GLCall(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
}

void Renderer::Draw(const VertexArray & va, const IndexBuffer & ib, const Shader & shader) const
{
	shader.Bind();
	va.Bind();
	ib.Bind();
	GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));

}
