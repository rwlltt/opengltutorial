#shader vertex
#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 color;

smooth out vec4 theColor;
uniform vec2 offset;

void main()
{
    gl_Position = position + vec4(offset.x, offset.y, 0.0, 0.0);
    theColor = color;
}

#shader fragment
#version 330 core

smooth in vec4 theColor;

out vec4 outputColor;

void main()
{
    outputColor = theColor;
}