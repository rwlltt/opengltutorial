#ifndef TUT_GLOBALUNIFORMBUFFER_
#define TUT_GLOBALUNIFORMBUFFER_

#include <glm/glm.hpp>

class GlobalUniformBuffer {
	unsigned int m_GlobalMatricesUBOID;
	unsigned int m_Count;

public:
    GlobalUniformBuffer(unsigned int count);
	~GlobalUniformBuffer();

	void Bind() const;
	void UnBind() const;

    void SetGlobalUniform(int iGlobalMatricesBindingIndex);
    void SetGlobalUniformDataIndex0(const glm::mat4& matrix);
    void SetGlobalUniformDataIndex1(const glm::mat4& matrix);

	inline unsigned int GetCount() const { return m_Count; }
};


#endif