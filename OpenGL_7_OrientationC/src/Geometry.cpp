//
// Created by rodney on 1/06/2020.
//

#include "Geometry.h"

Geometry::Geometry() {
}

Geometry::~Geometry() {
}

glm::mat3 Geometry::RotateX(float fAngDeg)
{
    float fAngRad = DegToRad(fAngDeg);
    float fCos = cosf(fAngRad);
    float fSin = sinf(fAngRad);

    glm::mat3 theMat(1.0f);
    theMat[1].y = fCos; theMat[2].y = -fSin;
    theMat[1].z = fSin; theMat[2].z = fCos;
    return theMat;
}

glm::mat3 Geometry::RotateY(float fAngDeg)
{
    float fAngRad = DegToRad(fAngDeg);
    float fCos = cosf(fAngRad);
    float fSin = sinf(fAngRad);

    glm::mat3 theMat(1.0f);
    theMat[0].x = fCos; theMat[2].x = fSin;
    theMat[0].z = -fSin; theMat[2].z = fCos;
    return theMat;
}

glm::mat3 Geometry::RotateZ(float fAngDeg)
{
    float fAngRad = DegToRad(fAngDeg);
    float fCos = cosf(fAngRad);
    float fSin = sinf(fAngRad);

    glm::mat3 theMat(1.0f);
    theMat[0].x = fCos; theMat[1].x = -fSin;
    theMat[0].y = fSin; theMat[1].y = fCos;
    return theMat;
}

float Geometry::ComputeAngleRad(float fElapsedTime, float fLoopDuration)
{
    const float fScale = 3.14159f * 2.0f / fLoopDuration;
    float fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);
    return fCurrTimeThroughLoop * fScale;
}
