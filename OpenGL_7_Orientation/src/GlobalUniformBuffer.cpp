
#include "GlobalUniformBuffer.h"
#include "Renderer.h"

GlobalUniformBuffer::GlobalUniformBuffer(unsigned int count): m_Count(count)
{
	ASSERT(sizeof(unsigned int) == sizeof(GLuint));
	GLCall(glGenBuffers(1, &m_GlobalMatricesUBOID));
	GLCall(glBindBuffer(GL_UNIFORM_BUFFER, m_GlobalMatricesUBOID));
	GLCall(glBufferData(GL_UNIFORM_BUFFER, count * sizeof(glm::mat4), NULL, GL_STREAM_DRAW));

}

GlobalUniformBuffer::~GlobalUniformBuffer()
{
	GLCall(glDeleteBuffers(1, &m_GlobalMatricesUBOID));
}

void GlobalUniformBuffer::Bind() const
{
	GLCall(glBindBuffer(GL_UNIFORM_BUFFER, m_GlobalMatricesUBOID));
}

void GlobalUniformBuffer::UnBind() const
{
	GLCall(glBindBuffer(GL_UNIFORM_BUFFER, 0));
}

void GlobalUniformBuffer::SetGlobalUniform(int iGlobalMatricesBindingIndex) {
    GLCall(glBindBufferRange(GL_UNIFORM_BUFFER, iGlobalMatricesBindingIndex, m_GlobalMatricesUBOID, 0, sizeof(glm::mat4) * m_Count));
}

void GlobalUniformBuffer::SetGlobalUniformDataIndex0(const glm::mat4& matrix) {
    GLCall(glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), &matrix[0][0]));
}

void GlobalUniformBuffer::SetGlobalUniformDataIndex1(const glm::mat4& matrix) {
    GLCall(glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), &matrix[0][0]));
}