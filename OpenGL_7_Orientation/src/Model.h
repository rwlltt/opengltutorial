/*
 * Model.h
 *
 *  Created on: 20/07/2017
 *      Author: rodney
 */

#ifndef SRC_MODULE_MODEL_H_
#define SRC_MODULE_MODEL_H_

#include "Module.h"
#include "loki/Singleton.h"
#include "loki/Factory.h"
#include "loki/Threads.h"
#include <string>
#include <vector>
#include <thread>
#include <future>
#include <memory>

using namespace Loki;

namespace Storage {
class DBStorage;
class ResultRetrieval;
namespace Driver {
class DBDriver;
}
}
namespace Module {

/// Model
class Model : public ClassLevelLockable<Model> {

	std::string m_dbVersion{dbVersion};
    std::string m_dbConfigXml{dbConfigXml};
	std::string m_UserName{"TEST"};
	std::string m_UserPassword{"pass"};
	std::string m_Role{"GUEST"};
	
	std::unique_ptr<Storage::DBStorage> m_dbStorage;

	typedef CreateUsingNew<Model> Creator;
	Model();
	virtual ~Model();

public:
	Model(const Model& other) = delete;
	Model& operator= (const Model &other) = delete;
	Model* operator&(Model object) = delete;

	friend class Creator;

	/// Read movie list
	/**
	 * Using a template method to retrieve DataType entities in a DBResult
	 */
	Storage::ResultRetrieval movie_list();
	std::unique_ptr<Storage::Driver::DBDriver> create_dr();
	std::string db_version(Storage::Driver::DBDriver* dr);
    void create_tables();

};

} /* namespace Module */

#endif /* SRC_MODULE_MODEL_H_ */
