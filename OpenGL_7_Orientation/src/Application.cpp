
#include "Application.h"
#include "Renderer.h"
#include "Shader.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
#include "GlobalUniformBuffer.h"
#include <chrono>
#include <future>

bool Application::bDepthClampingActive = false;
std::unique_ptr<test::WorldScene> Application::worldScene{};
#define SMALL_ANGLE_INCREMENT 9.0f

Application::Application()
{
}

Application::~Application()
{
	std::cout << "App dest\n";
}

void Application::Init() {
	signal(SIGINT, signal_handler); // Catch Ctrl c to stop abrubt end.

	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		throw std::exception("GlfwInit failed");

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(640, 480, "Simple Depth Wedge", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		throw std::exception("GlfwCreate window failed");
	}
	glfwSetKeyCallback(window, key_callback);
    //glfwSetWindowSizeCallback(window, reshape_callback);
	glfwMakeContextCurrent(window);
	auto result = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	GLCall(std::cout << glGetString(GL_VERSION) << "\n");
	glfwSwapInterval(1);

	try {

		WSADATA wsaData;
		if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0) {
			fprintf(stderr, "WSAStartup failed.\n");
			throw std::exception("No WinSock");
		}
		
		m_user = std::make_unique<User>(m_user_name);
		Model::Instance().create_tables();
		SendAppMessage("InitAppRequest");
	}
	catch (const std::exception& e) {
		throw std::exception(e.what());
	}

}

void Application::Run() {
	{
		GLCall(glEnable(GL_BLEND));
		GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

		ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO(); (void)io;
		ImGui::StyleColorsDark();
		ImGui_ImplGlfw_InitForOpenGL(window, true);
		ImGui_ImplOpenGL3_Init(glsl_version);
		std::string message_in{ "" };
		std::map<std::string, std::future<std::unique_ptr<Response>>> mapped;
		int width, height;
        //bool show_demo_window = false;
        // Store start time of drawings
        auto startTimer = std::chrono::high_resolution_clock::now();
        Renderer renderer;
        worldScene = std::make_unique<test::WorldScene>();
        GlobalUniformBuffer globalUniformBuffer(2);
        worldScene->SetGlobalUniform(iGlobalMatricesBindingIndex, &globalUniformBuffer);
        globalUniformBuffer.SetGlobalUniform(iGlobalMatricesBindingIndex);
        renderer.RegisterGlobalUniform(&globalUniformBuffer);
        //worldScene->NotifyRenderer(renderer);

        GLCall(glEnable(GL_CULL_FACE));
        GLCall(glCullFace(GL_BACK));
        GLCall(glFrontFace(GL_CW));

        GLCall(glEnable(GL_DEPTH_TEST));
        GLCall(glDepthMask(GL_TRUE));
        GLCall(glDepthFunc(GL_LEQUAL));
        GLCall(glDepthRange(0.0f, 1.0f));
        GLCall(glEnable(GL_DEPTH_CLAMP));

		while (true)
		{
			if (glfwWindowShouldClose(window)) {
                std::cout << "Window Close\n";
				SendAppMessage("Quit");
				break;
			}

			{
				Lock lock;
				if (messages.size() > 0) {
					message_in = messages.front();
					messages.pop();
					if (message_in.compare("Quit") == 0)
						break;
				}
			}
			if (message_in.compare("") != 0) {
				if (mapped.count(message_in) == 1) { // A completed async task which is complete and is mapped
					if (mapped[message_in].valid()) {
						auto res = mapped[message_in].get();
						m_user->SetResponse(std::move(res));
					}
				}
				else {
					mapped[message_in + "Done"] = DoRequest(m_user_name, message_in);
				}
			}
			// Calculate deltatime from epoch
            auto ticker = std::chrono::high_resolution_clock::now();
            std::chrono::duration<float> diff = ticker-startTimer;
            float deltaTime  = diff.count();

            glfwGetFramebufferSize(window, &width, &height);
			renderer.ViewPort(width, height);
			renderer.Clear();

			// For all mapped users do update render and imguiupdate
            m_user->OnUpdate(0.0f);
            m_user->OnRender();

            worldScene->OnUpdate(deltaTime);
            worldScene->OnRender();

            // Start the Dear ImGui frame
			ImGui_ImplOpenGL3_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();

            worldScene->OnImGuiRender();

            // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()!
            // You can browse its code to learn more about Dear ImGui!).
            //ImGui::ShowDemoWindow(&show_demo_window);
			{
			  ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			}

			ImGui::Render();
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

            glfwSwapBuffers(window);
			glfwPollEvents();
		}

		glfwSetWindowShouldClose(window, 1);
	}
	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwDestroyWindow(window);
	glfwTerminate();
}

std::future<std::unique_ptr<Response>> Application::DoRequest(const std::string &user, const std::string &request)
{
	auto req = std::unique_ptr<Request>(RequestFactory::Instance().CreateObject(request));
	req->SetUser(user);
	std::future<std::unique_ptr<Response>> result = std::async(
		std::launch::async, [](std::unique_ptr<Request> req, const std::string &user, const std::string &request) {
		                    auto res = req->DoRequest(user);
							res->SetRequest(std::move(req));
							res->Display();
							TheApp::Instance().SendAppMessage(request +"Done");
							return res;
	                  }, std::move(req), user, request);
	return result;
}

void Application::SendAppMessage(const std::string &request) {
	Lock lock;
	messages.push(request);
}

void Application::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch (action)
    {
        case GLFW_PRESS :
            switch (key) {
                case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, GLFW_TRUE); break;
                case GLFW_KEY_SPACE:
                {
                    worldScene->bDrawLookatPoint = !worldScene->bDrawLookatPoint;
                    printf("Target: %f, %f, %f\n", worldScene->camTarget.x, worldScene->camTarget.y, worldScene->camTarget.z);
                    printf("Position: %f, %f, %f\n", worldScene->sphereCamRelPos.x, worldScene->sphereCamRelPos.y, worldScene->sphereCamRelPos.z);

                    worldScene->iOffset += 1;
                    worldScene->iOffset = worldScene->iOffset % test::NUM_RELATIVES;
                    {
                        switch(worldScene->iOffset)
                        {
                            case test::MODEL_RELATIVE: printf("Model Relative\n"); break;
                            case test::WORLD_RELATIVE: printf("World Relative\n"); break;
                            case test::CAMERA_RELATIVE: printf("Camera Relative\n"); break;
                        }
                    }

                }
                break;
            default: /* Do nothing */ break;
        }
        break;
        default: /* Do nothing */ break;
    }

    // Continuous press no need to have key press each time
    switch (mods) {
        case GLFW_MOD_CONTROL: // Control key and char for orienttation ship YPR Yaw Pitch and Roll
            switch (key) {
                case GLFW_KEY_W: worldScene->OffsetOrientation(glm::vec3(1.0f, 0.0f, 0.0f), SMALL_ANGLE_INCREMENT);; break;
                case GLFW_KEY_S: worldScene->OffsetOrientation(glm::vec3(1.0f, 0.0f, 0.0f), -SMALL_ANGLE_INCREMENT); break;
                case GLFW_KEY_A: worldScene->OffsetOrientation(glm::vec3(0.0f, 0.0f, 1.0f), SMALL_ANGLE_INCREMENT); break;
                case GLFW_KEY_D: worldScene->OffsetOrientation(glm::vec3(0.0f, 0.0f, 1.0f), -SMALL_ANGLE_INCREMENT); break;
                case GLFW_KEY_Q: worldScene->OffsetOrientation(glm::vec3(0.0f, 1.0f, 0.0f), SMALL_ANGLE_INCREMENT); break;
                case GLFW_KEY_E: worldScene->OffsetOrientation(glm::vec3(0.0f, 1.0f, 0.0f), -SMALL_ANGLE_INCREMENT); break;
                default: /* Do nothing */ break;
            }
            break;
        case GLFW_MOD_SHIFT: // Uppercase Letter
            switch (key) {
//                case GLFW_KEY_W: worldScene->camTarget.z -= 0.4f; break;
//                case GLFW_KEY_S: worldScene->camTarget.z += 0.4f; break;
//                case GLFW_KEY_D: worldScene->camTarget.x += 0.4f; break;
//                case GLFW_KEY_A: worldScene->camTarget.x -= 0.4f; break;
//                case GLFW_KEY_E: worldScene->camTarget.y -= 0.4f; break;
//                case GLFW_KEY_Q: worldScene->camTarget.y += 0.4f; break;

                case GLFW_KEY_I: worldScene->sphereCamRelPos.y -= 1.125f; break;
                case GLFW_KEY_K: worldScene->sphereCamRelPos.y += 1.125f; break;
                case GLFW_KEY_J: worldScene->sphereCamRelPos.x -= 1.125f; break;
                case GLFW_KEY_L: worldScene->sphereCamRelPos.x += 1.125f; break;
                case GLFW_KEY_O: worldScene->sphereCamRelPos.z -= 0.5f; break;
                case GLFW_KEY_U: worldScene->sphereCamRelPos.z += 0.5f; break;
                default: /* Do nothing */ break;
            }
            break;
        default: // lowercase letter
            switch (key) {
//                case GLFW_KEY_W: worldScene->camTarget.z -= 4.0f; break;
//                case GLFW_KEY_S: worldScene->camTarget.z += 4.0f; break;
//                case GLFW_KEY_D: worldScene->camTarget.x += 4.0f; break;
//                case GLFW_KEY_A: worldScene->camTarget.x -= 4.0f; break;
//                case GLFW_KEY_E: worldScene->camTarget.y -= 4.0f; break;
//                case GLFW_KEY_Q: worldScene->camTarget.y += 4.0f; break;
                case GLFW_KEY_W: worldScene->OffsetOrientation(glm::vec3(1.0f, 0.0f, 0.0f), SMALL_ANGLE_INCREMENT);; break;
                case GLFW_KEY_S: worldScene->OffsetOrientation(glm::vec3(1.0f, 0.0f, 0.0f), -SMALL_ANGLE_INCREMENT); break;
                case GLFW_KEY_A: worldScene->OffsetOrientation(glm::vec3(0.0f, 0.0f, 1.0f), SMALL_ANGLE_INCREMENT); break;
                case GLFW_KEY_D: worldScene->OffsetOrientation(glm::vec3(0.0f, 0.0f, 1.0f), -SMALL_ANGLE_INCREMENT); break;
                case GLFW_KEY_Q: worldScene->OffsetOrientation(glm::vec3(0.0f, 1.0f, 0.0f), SMALL_ANGLE_INCREMENT); break;
                case GLFW_KEY_E: worldScene->OffsetOrientation(glm::vec3(0.0f, 1.0f, 0.0f), -SMALL_ANGLE_INCREMENT); break;

                case GLFW_KEY_I: worldScene->sphereCamRelPos.y -= 11.25f; break;
                case GLFW_KEY_K: worldScene->sphereCamRelPos.y += 11.25f; break;
                case GLFW_KEY_J: worldScene->sphereCamRelPos.x -= 11.25f; break;
                case GLFW_KEY_L: worldScene->sphereCamRelPos.x += 11.25f; break;
                case GLFW_KEY_O: worldScene->sphereCamRelPos.z -= 5.0f; break;
                case GLFW_KEY_U: worldScene->sphereCamRelPos.z += 5.0f; break;

                default: /* Do nothing */ break;
            }
            break;
    }

    worldScene->sphereCamRelPos.y = glm::clamp(worldScene->sphereCamRelPos.y, -78.75f, 10.0f);
    worldScene->camTarget.y = worldScene->camTarget.y > 0.0f ? worldScene->camTarget.y : 0.0f;
    worldScene->sphereCamRelPos.z = worldScene->sphereCamRelPos.z > 5.0f ? worldScene->sphereCamRelPos.z : 5.0f;
}
