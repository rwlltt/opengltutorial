#ifndef TUT_VERTEXBUFFER_
#define TUT_VERTEXBUFFER_

class VertexBuffer {
	unsigned int m_RendererID;

public:
	VertexBuffer(const void* data, unsigned int size, bool dynamic);
	~VertexBuffer();

	void Bind() const;
	void UnBind() const;
    void AdjustVertexData(const void * data, unsigned int size);
};


#endif