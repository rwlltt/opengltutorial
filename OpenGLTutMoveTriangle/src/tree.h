/*
 * tree.h
 */


#ifndef DATA_TREE_H_
#define DATA_TREE_H_

#include <vector>
#include <string>

namespace data
{

	class tree
	{
	public:
		tree() : root(nullptr),
			spacePrint(" "),
			linePrint("_"),
			leftBranch("/"),
			rightBranch("\\") {
		};
		virtual ~tree();

		void enterItem(const int item);
		void print();
		void printPostOrder();
		void printLevelOrder();
		void levelQueueNonRecOrder();
		void levelOrder();

	private:
		tree(const tree& oldtree);
		tree operator = (const tree& oldtree);

		class node {
		public:
			node() : data(0), right(nullptr), left(nullptr) {};
			int data;
		private:
			node(const node& oldnode);
			node operator = (const node& oldnode);
			node* right;
			node* left;
			friend class tree;
		};

		// Root of tree
		node* root;

		std::string spacePrint;
		std::string linePrint;
		std::string leftBranch;
		std::string rightBranch;

		// Recursive function
		void enterTreeItem(node *&treeNode, const int item);
		void printItem(node *top);
		void printPostOrderItem(node *top);
		//void deletePostOrderItem(node *top);
		void printNonRecspostorder(node *head);
		void deleteRecsPostOrderItem(node *top);

		void printGivenLevel(node* root, int level);
		void levelQueueOrder(node* n);
		int height(node* node);
		node* placeHolderNode();
		int nodeAmountByHeight(int treeHeight);
		void drawCompleteTreeByLevelOrder(int treeHeight, const std::vector<std::string>& drawitems);
		void drawRepeatString(int lengthSpace, const std::string& charPrint);
		void drawLavelSpaces(int nodeDone, int halfSpaceNode, int lengthSpace);
		void drawFullBranch(int lengthSpace);
		void drawRightBranch(int lengthSpace);
		void drawLeftBranch(int lengthSpace);
		void drawNoBranch(int lengthSpace);
		//void drawFourNode(int lengthSpace);
		void drawFourNode(const std::vector<std::string>& drawitems,
			int col, int lengthSpace);
		void drawItemInTree(const std::string& item, const int itemSpace,
			const int itemGaps);
		void drawLeftRightBranches(const std::vector<std::string>& drawitems,
			int col, int lengthSpace);
	};

} /* namespace data */

#endif /* DATA_TREE_H_ */
