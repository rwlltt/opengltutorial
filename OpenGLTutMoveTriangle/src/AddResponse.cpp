#include "AddResponse.h"
#include "Application.h"
#include "storage/Entity.h" 

using namespace io_utility;
//Loki::Functor<void, LOKI_TYPELIST_1(int)> cmd1{ fn };

namespace {
	Response* CreateAddResponse() {
		return new AddResponse();
	}

	const bool registered = ResponseFactory::Instance().Register("AddResponse", CreateAddResponse);
}


AddResponse::AddResponse()
{
}


AddResponse::~AddResponse()
{
//	std::cout << "Destruct AppResponse.\n";
}

void AddResponse::Display() const {
	print({ "Enter Movie ID: ", movieID });

}

std::tuple < std::string, std::set<char>, std::map<char, std::string>> AddResponse::Prompt() const {
	std::string prompt("Continue [AaQq]:");
	std::set<char> choices{ 'a', 'A', 'q', 'Q' };
	std::map<char, std::string> reqs{ {'a', "AddRequest"}, {'A', "AddRequest"}, {'q', "Quit"}, {'Q', "Quit"} };
	return std::make_tuple(prompt, choices, reqs);
}


