#ifndef TUT_INITAPPRESPONSE_
#define TUT_INITAPPRESPONSE_

#include "Response.h"
#include "storage/DBStorage.h" 
#include "Context.h"
#include "tests/TestClearColour.h"

class InitAppResponse : public Response
{
	test::TestClearColour test;

public:
	InitAppResponse();
	~InitAppResponse();
public:
	void Display() const;
	std::tuple < std::string, std::set<char>, std::map<char, std::string>> Prompt() const;

	void OnUpdate(float deltaTime)  override;
	void OnRender()  override;
	void OnImGuiRender() override;

};


#endif /* TUT_INITAPPRESPONSE_ */