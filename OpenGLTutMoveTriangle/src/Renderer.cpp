
#include "Renderer.h"
#include <iostream>

void  GLClearError() {
	while (glGetError() != GL_NO_ERROR);
}

bool GLLogCall(const char* function, const char* file, int line) {
	while (GLenum error = glGetError()) {
		std::cout << "[OpenGl Error](" << std::hex << error << std::dec << "): " <<
			function << " " << file << ":" << line << '\n';
		return false;
	}
	return true;
}

void Renderer::ViewPort(int width, int height) const
{
    float ratio = width / (float)height;
    int widthR, heightR;
    // Non stretch view of graphics render
    if (ratio > 1) {
        widthR = height;
        heightR = height;
    } else {
        widthR = width;
        heightR = width;
    }
    // Centre in the view screen width and height of window
    int centrePointX = width/2;
    int centrePointY = height/2;
    int vx,vy;
    vx = centrePointX - widthR/2;
    vy = centrePointY - heightR/2;
	GLCall(glViewport(vx, vy, (GLsizei)widthR, (GLsizei)heightR));
}

void Renderer::Clear() const
{
    GLCall(glClearColor(0.0f, 0.0f, 0.0f, 0.0f));
	GLCall(glClear(GL_COLOR_BUFFER_BIT));
}

void Renderer::Draw(const VertexArray & va, const IndexBuffer & ib, const Shader & shader) const
{
	shader.Bind();
	va.Bind();
	ib.Bind();
	GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));

}
