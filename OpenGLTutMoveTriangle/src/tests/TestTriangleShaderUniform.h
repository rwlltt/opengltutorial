#ifndef TUT_TEST_TRIANGLESHADERUNIFORM
#define TUT_TEST_TRIANGLESHADERUNIFORM

#include <Shader.h>
#include <VertexBuffer.h>
#include <VertexArray.h>
#include <VertexBufferLayout.h>
#include "Test.h"

namespace test {

	class TestTriangleShaderUniform : public Test {
	private:
        float vertexPositions[12];
        Shader shader;
        VertexBuffer vb;
        VertexArray va;
        VertexBufferLayout layout;
        //float fXOffset = 0.0f, fYOffset = 0.0f;
        float deltaTime = 0.0f;

    public:
        TestTriangleShaderUniform();
		~TestTriangleShaderUniform();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;

	private:
        void ComputePositionOffsets(float fElapsedTime);
        void AdjustVertexData();

    };


}

#endif /* TUT_TEST_TRIANGLESHADERUNIFORM*/

