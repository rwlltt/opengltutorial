#include "TestTriangleShaderUniform.h"

#include <Renderer.h>
#include "imgui/imgui.h"
#define ARRAY_COUNT( array ) (sizeof( array ) / (sizeof( array[0] ) * (sizeof( array ) != sizeof(void*) || sizeof( array[0] ) <= sizeof(void*))))

namespace test {


    TestTriangleShaderUniform::TestTriangleShaderUniform()
		: vertexPositions {   0.25f, 0.25f, 0.0f, 1.0f,
                              0.25f, -0.25f, 0.0f, 1.0f,
                             -0.25f, -0.25f, 0.0f, 1.0f},
                             shader {"res/shaders/TriangleUniform.shader"},
                             vb {vertexPositions, 12 * sizeof(vertexPositions[0]), true }
	{
        layout.Push<float>(4);
        va.AddBuffer(vb, layout);
        shader.Bind();
        shader.SetUniform1f("loopDuration", 5.0f );  // The shader will also calc the arc of circle
        shader.SetUniform1f("fragLoopDuration", 10.0f );  // The shader will also calc the arc of circle
    }

    TestTriangleShaderUniform::~TestTriangleShaderUniform()
	{
	}

	void TestTriangleShaderUniform::OnUpdate(float deltaTime)
	{
        // Store the delta
        this->deltaTime = deltaTime;
        //ComputePositionOffsets(deltaTime);
	}

	void TestTriangleShaderUniform::OnRender()
	{
        //AdjustVertexData(); Using a uniform to send the offset rather than all thew vertices

        shader.Bind();
        //shader.SetUniform2f("offset", fXOffset, fYOffset );
        //fprintf(stderr, "delta time %f \n",deltaTime );
        shader.SetUniform1f("time", deltaTime );

        va.Bind();
        GLCall(glDrawArrays(GL_TRIANGLES, 0, 3));
	}

	void TestTriangleShaderUniform::OnImGuiRender()
	{
		//ImGui::ColorEdit4("Clear Colour", m_clearColour);
	}

    void TestTriangleShaderUniform::ComputePositionOffsets(float fElapsedTime)
    {
        const float fLoopDuration = 5.0f;
        const float fScale = 3.14159f * 2.0f / fLoopDuration;

        // glutGet is replaced with deltatime calc with std chrono class
        //float fElapsedTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;

        float fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);

        //fXOffset = cosf(fCurrTimeThroughLoop * fScale) * 0.75f;
        //fYOffset = sinf(fCurrTimeThroughLoop * fScale) * 0.75f;
        //fprintf(stderr, "ComputePositionOffsets %f %f %f\n", fXOffset, fYOffset, fCurrTimeThroughLoop );
    }

    void TestTriangleShaderUniform::AdjustVertexData()
    {
        std::vector<float> fNewData(ARRAY_COUNT(vertexPositions));
        memcpy(&fNewData[0], vertexPositions, sizeof(vertexPositions));

        for(int iVertex = 0; iVertex < ARRAY_COUNT(vertexPositions); iVertex += 4)
        {
            //fNewData[iVertex] += fXOffset;
            //fNewData[iVertex + 1] += fYOffset;
        }

        vb.AdjustVertexData(&fNewData[0], sizeof(vertexPositions));
    }
}