#ifndef TUT_TESTS_
#define TUT_TESTS_

namespace test {

	class Test {
	
	public:
		Test() {}
		virtual ~Test() {}

		virtual void OnUpdate(float deltaTime) {}
		virtual void OnRender() {}
		virtual void OnImGuiRender() {}

	};

} // end namespace test
#endif /* TUT_TESTS_*/
