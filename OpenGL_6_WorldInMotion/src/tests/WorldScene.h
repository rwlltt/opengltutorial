#ifndef TUT_TEST_ARMATURE
#define TUT_TEST_ARMATURE

#include <Shader.h>
#include <Renderer.h>
#include <VertexBuffer.h>
#include <VertexArray.h>
#include <VertexBufferLayout.h>
#include <glutil/MatrixStack.h>
#include "Test.h"
#include "framework/Mesh.h"
#include "Geometry.h"

namespace test {
    struct TreeData
    {
        float fXPos;
        float fZPos;
        float fTrunkHeight;
        float fConeHeight;
    };

	class WorldScene : public Test {
	public:
        static bool bDrawLookatPoint;
        static glm::vec3 camTarget;
        //In spherical coordinates.
        static glm::vec3 sphereCamRelPos;

    private:
        const TreeData forest[98];
        Shader UniformColor;
        Shader ObjectColor;
        Shader UniformColorTint;

        Framework::Mesh ConeMesh;
        Framework::Mesh CylinderMesh;
        Framework::Mesh CubeTintMesh;
        Framework::Mesh CubeColorMesh;
        Framework::Mesh PlaneMesh;

        float deltaTime = 0.0f;
        glm::vec3 translationA;
        glm::vec3 translationB;
        GlobalUniformBuffer *ubo;
        Geometry geometry;

    public:
        WorldScene();
		~WorldScene();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;
        void SetGlobalUniform(int iGlobalMatricesBindingIndex, GlobalUniformBuffer* ubo);
        void NotifyRenderer(Renderer &renderer);

        void AdjBase(bool bIncrement);
        void AdjUpperArm(bool bIncrement);
        void AdjLowerArm(bool bIncrement);
        void AdjWristPitch(bool bIncrement);
        void AdjWristRoll(bool bIncrement);
        void AdjFingerOpen(bool bIncrement);
        void WritePose();

    private:
        glm::vec3 ResolveCamPosition();
        glm::mat4 CalcLookAtMatrix(const glm::vec3 &cameraPt, const glm::vec3 &lookPt, const glm::vec3 &upPt);
        void DrawForest(glutil::MatrixStack &modelMatrix, glutil::MatrixStack &camMatrix);
        //Trees are 3x3 in X/Z, and fTrunkHeight+fConeHeight in the Y.
        void DrawTree(glutil::MatrixStack &modelMatrix, float fTrunkHeight = 2.0f, float fConeHeight = 3.0f);
        void DrawParthenon(glutil::MatrixStack &modelMatrix);
        //Columns are 1x1 in the X/Z, and fHieght units in the Y.
        void DrawColumn(glutil::MatrixStack &modelMatrix, float fHeight = 5.0f);

    };


}

#endif /* TUT_TEST_ARMATURE*/

