#include "WorldScene.h"
#include <Renderer.h>
#include "imgui/imgui.h"
#include <iostream>

#define ARRAY_COUNT( array ) (sizeof( array ) / (sizeof( array[0] ) * (sizeof( array ) != sizeof(void*) || sizeof( array[0] ) <= sizeof(void*))))

namespace test {

    bool WorldScene::bDrawLookatPoint = false;
    glm::vec3 WorldScene::camTarget{0.0f, 0.4f, 0.0f};
    //In spherical coordinates.
    glm::vec3 WorldScene::sphereCamRelPos(67.5f, -46.0f, 150.0f);

    WorldScene::WorldScene():
    		forest {
                    {-45.0f, -40.0f, 2.0f, 3.0f},
                    {-42.0f, -35.0f, 2.0f, 3.0f},
                    {-39.0f, -29.0f, 2.0f, 4.0f},
                    {-44.0f, -26.0f, 3.0f, 3.0f},
                    {-40.0f, -22.0f, 2.0f, 4.0f},
                    {-36.0f, -15.0f, 3.0f, 3.0f},
                    {-41.0f, -11.0f, 2.0f, 3.0f},
                    {-37.0f, -6.0f, 3.0f, 3.0f},
                    {-45.0f, 0.0f, 2.0f, 3.0f},
                    {-39.0f, 4.0f, 3.0f, 4.0f},
                    {-36.0f, 8.0f, 2.0f, 3.0f},
                    {-44.0f, 13.0f, 3.0f, 3.0f},
                    {-42.0f, 17.0f, 2.0f, 3.0f},
                    {-38.0f, 23.0f, 3.0f, 4.0f},
                    {-41.0f, 27.0f, 2.0f, 3.0f},
                    {-39.0f, 32.0f, 3.0f, 3.0f},
                    {-44.0f, 37.0f, 3.0f, 4.0f},
                    {-36.0f, 42.0f, 2.0f, 3.0f},

                    {-32.0f, -45.0f, 2.0f, 3.0f},
                    {-30.0f, -42.0f, 2.0f, 4.0f},
                    {-34.0f, -38.0f, 3.0f, 5.0f},
                    {-33.0f, -35.0f, 3.0f, 4.0f},
                    {-29.0f, -28.0f, 2.0f, 3.0f},
                    {-26.0f, -25.0f, 3.0f, 5.0f},
                    {-35.0f, -21.0f, 3.0f, 4.0f},
                    {-31.0f, -17.0f, 3.0f, 3.0f},
                    {-28.0f, -12.0f, 2.0f, 4.0f},
                    {-29.0f, -7.0f, 3.0f, 3.0f},
                    {-26.0f, -1.0f, 2.0f, 4.0f},
                    {-32.0f, 6.0f, 2.0f, 3.0f},
                    {-30.0f, 10.0f, 3.0f, 5.0f},
                    {-33.0f, 14.0f, 2.0f, 4.0f},
                    {-35.0f, 19.0f, 3.0f, 4.0f},
                    {-28.0f, 22.0f, 2.0f, 3.0f},
                    {-33.0f, 26.0f, 3.0f, 3.0f},
                    {-29.0f, 31.0f, 3.0f, 4.0f},
                    {-32.0f, 38.0f, 2.0f, 3.0f},
                    {-27.0f, 41.0f, 3.0f, 4.0f},
                    {-31.0f, 45.0f, 2.0f, 4.0f},
                    {-28.0f, 48.0f, 3.0f, 5.0f},

                    {-25.0f, -48.0f, 2.0f, 3.0f},
                    {-20.0f, -42.0f, 3.0f, 4.0f},
                    {-22.0f, -39.0f, 2.0f, 3.0f},
                    {-19.0f, -34.0f, 2.0f, 3.0f},
                    {-23.0f, -30.0f, 3.0f, 4.0f},
                    {-24.0f, -24.0f, 2.0f, 3.0f},
                    {-16.0f, -21.0f, 2.0f, 3.0f},
                    {-17.0f, -17.0f, 3.0f, 3.0f},
                    {-25.0f, -13.0f, 2.0f, 4.0f},
                    {-23.0f, -8.0f, 2.0f, 3.0f},
                    {-17.0f, -2.0f, 3.0f, 3.0f},
                    {-16.0f, 1.0f, 2.0f, 3.0f},
                    {-19.0f, 4.0f, 3.0f, 3.0f},
                    {-22.0f, 8.0f, 2.0f, 4.0f},
                    {-21.0f, 14.0f, 2.0f, 3.0f},
                    {-16.0f, 19.0f, 2.0f, 3.0f},
                    {-23.0f, 24.0f, 3.0f, 3.0f},
                    {-18.0f, 28.0f, 2.0f, 4.0f},
                    {-24.0f, 31.0f, 2.0f, 3.0f},
                    {-20.0f, 36.0f, 2.0f, 3.0f},
                    {-22.0f, 41.0f, 3.0f, 3.0f},
                    {-21.0f, 45.0f, 2.0f, 3.0f},

                    {-12.0f, -40.0f, 2.0f, 4.0f},
                    {-11.0f, -35.0f, 3.0f, 3.0f},
                    {-10.0f, -29.0f, 1.0f, 3.0f},
                    {-9.0f, -26.0f, 2.0f, 2.0f},
                    {-6.0f, -22.0f, 2.0f, 3.0f},
                    {-15.0f, -15.0f, 1.0f, 3.0f},
                    {-8.0f, -11.0f, 2.0f, 3.0f},
                    {-14.0f, -6.0f, 2.0f, 4.0f},
                    {-12.0f, 0.0f, 2.0f, 3.0f},
                    {-7.0f, 4.0f, 2.0f, 2.0f},
                    {-13.0f, 8.0f, 2.0f, 2.0f},
                    {-9.0f, 13.0f, 1.0f, 3.0f},
                    {-13.0f, 17.0f, 3.0f, 4.0f},
                    {-6.0f, 23.0f, 2.0f, 3.0f},
                    {-12.0f, 27.0f, 1.0f, 2.0f},
                    {-8.0f, 32.0f, 2.0f, 3.0f},
                    {-10.0f, 37.0f, 3.0f, 3.0f},
                    {-11.0f, 42.0f, 2.0f, 2.0f},


                    {15.0f, 5.0f, 2.0f, 3.0f},
                    {15.0f, 10.0f, 2.0f, 3.0f},
                    {15.0f, 15.0f, 2.0f, 3.0f},
                    {15.0f, 20.0f, 2.0f, 3.0f},
                    {15.0f, 25.0f, 2.0f, 3.0f},
                    {15.0f, 30.0f, 2.0f, 3.0f},
                    {15.0f, 35.0f, 2.0f, 3.0f},
                    {15.0f, 40.0f, 2.0f, 3.0f},
                    {15.0f, 45.0f, 2.0f, 3.0f},

                    {25.0f, 5.0f, 2.0f, 3.0f},
                    {25.0f, 10.0f, 2.0f, 3.0f},
                    {25.0f, 15.0f, 2.0f, 3.0f},
                    {25.0f, 20.0f, 2.0f, 3.0f},
                    {25.0f, 25.0f, 2.0f, 3.0f},
                    {25.0f, 30.0f, 2.0f, 3.0f},
                    {25.0f, 35.0f, 2.0f, 3.0f},
                    {25.0f, 40.0f, 2.0f, 3.0f},
                    {25.0f, 45.0f, 2.0f, 3.0f},
            },
            UniformColor("res/shaders/ColorUniform.shader"),
            ObjectColor("res/shaders/ColorObject.shader"),
            UniformColorTint("res/shaders/ColorUniformTint.shader"),
            ConeMesh("UnitConeTint.xml"),
            CylinderMesh("UnitCylinderTint.xml"),
            CubeTintMesh("UnitCubeTint.xml"),
            CubeColorMesh("UnitCubeColor.xml"),
            PlaneMesh("UnitPlane.xml"),
            translationA {1.0, 1.0, -1.0},
            translationB {1.0, 1.0, -1.2},
            ubo(nullptr)
    {
	}

    WorldScene::~WorldScene()
	{
	}

    void WorldScene::NotifyRenderer(Renderer &renderer) {
        renderer.RegisterShader(&UniformColor);
        renderer.RegisterShader(&ObjectColor);
        renderer.RegisterShader(&UniformColorTint);
    }

    void WorldScene::SetGlobalUniform(int iGlobalMatricesBindingIndex, GlobalUniformBuffer* ubo) {
        UniformColor.SetGlobalUniform("GlobalMatrices", iGlobalMatricesBindingIndex);
        ObjectColor.SetGlobalUniform("GlobalMatrices", iGlobalMatricesBindingIndex);
        UniformColorTint.SetGlobalUniform("GlobalMatrices", iGlobalMatricesBindingIndex);
        this->ubo = ubo;
    }

	void WorldScene::OnUpdate(float deltaTime)
	{
        // Store the delta
        this->deltaTime = deltaTime;
	}

	void WorldScene::OnRender()
	{
        const glm::vec3 &camPos = ResolveCamPosition();

        glutil::MatrixStack camMatrix;
        camMatrix.SetMatrix(CalcLookAtMatrix(camPos, camTarget, glm::vec3(0.0f, 1.0f, 0.0f)));
//        glm::mat4 worldToCameraMatrix = camMatrix.Top();

//        if (ubo != nullptr) {
//            ubo->Bind();
//            ubo->SetGlobalUniformDataIndex1(worldToCameraMatrix);
//            ubo->UnBind();
//        }

        // Push worldToCameraMatrix into modeltoWorldMatrix
        glutil::MatrixStack modelMatrix;
        //Render the ground plane.
        {
            glutil::PushStack push(modelMatrix);
            modelMatrix = camMatrix;

            modelMatrix.Scale(glm::vec3(100.0f, 1.0f, 100.0f));
            glm::mat4 modelToWorldMatrix = modelMatrix.Top();

            UniformColor.Bind();
            UniformColor.SetUniformMat4f("modelToWorldMatrix", modelToWorldMatrix);
            UniformColor.SetUniform4f("baseColor", 0.302f, 0.416f, 0.0589f, 1.0f);
            PlaneMesh.Render();
            UniformColor.UnBind();
        }

        //Draw the trees
        DrawForest(modelMatrix, camMatrix);

        //Draw the building.
        {
            glutil::PushStack push(modelMatrix);
            modelMatrix = camMatrix;
            modelMatrix.Translate(glm::vec3(20.0f, 0.0f, -10.0f));

            DrawParthenon(modelMatrix);
        }


        if(bDrawLookatPoint)
        {
            GLCall(glDisable(GL_DEPTH_TEST));

            glutil::PushStack push(modelMatrix);
            modelMatrix = camMatrix;

            modelMatrix.Translate(camTarget);
            modelMatrix.Scale(1.0f, 1.0f, 1.0f);

            ObjectColor.Bind();
            ObjectColor.SetUniformMat4f("modelToWorldMatrix", modelMatrix.Top());
            CubeColorMesh.Render();
            ObjectColor.UnBind();
            GLCall(glEnable(GL_DEPTH_TEST));
        }

    }

	void WorldScene::OnImGuiRender()
	{
        ImGui::SliderFloat3("Offset 1", &translationA.x, -3.0f, 2.0f);
        ImGui::SliderFloat3("Offset 2", &translationB.x, -3.0f, 2.0f);
	}

    // Private drawing
    glm::vec3 WorldScene::ResolveCamPosition()
    {
        glutil::MatrixStack tempMat;

        float phi = geometry.DegToRad(sphereCamRelPos.x);
        float theta = geometry.DegToRad(sphereCamRelPos.y + 90.0f);

        float fSinTheta = sinf(theta);
        float fCosTheta = cosf(theta);
        float fCosPhi = cosf(phi);
        float fSinPhi = sinf(phi);

        glm::vec3 dirToCamera(fSinTheta * fCosPhi, fCosTheta, fSinTheta * fSinPhi);
        return (dirToCamera * sphereCamRelPos.z) + camTarget;
    }

    glm::mat4 WorldScene::CalcLookAtMatrix(const glm::vec3 &cameraPt, const glm::vec3 &lookPt, const glm::vec3 &upPt)
    {
        glm::vec3 lookDir = glm::normalize(lookPt - cameraPt);
        glm::vec3 upDir = glm::normalize(upPt);

        glm::vec3 rightDir = glm::normalize(glm::cross(lookDir, upDir));
        glm::vec3 perpUpDir = glm::cross(rightDir, lookDir);

        glm::mat4 rotMat(1.0f);
        rotMat[0] = glm::vec4(rightDir, 0.0f);
        rotMat[1] = glm::vec4(perpUpDir, 0.0f);
        rotMat[2] = glm::vec4(-lookDir, 0.0f);

        rotMat = glm::transpose(rotMat);

        glm::mat4 transMat(1.0f);
        transMat[3] = glm::vec4(-cameraPt, 1.0f);

        return rotMat * transMat;
    }

    void WorldScene::DrawForest(glutil::MatrixStack &modelMatrix, glutil::MatrixStack &camMatrix)
    {
        for(int iTree = 0; iTree < ARRAY_COUNT(forest); iTree++)
        {
            const TreeData &currTree = forest[iTree];

            glutil::PushStack push(modelMatrix);
            modelMatrix = camMatrix;

            modelMatrix.Translate(glm::vec3(currTree.fXPos, 0.0f, currTree.fZPos));
            DrawTree(modelMatrix, currTree.fTrunkHeight, currTree.fConeHeight);
        }
    }

    void WorldScene::DrawTree(glutil::MatrixStack &modelMatrix, float fTrunkHeight, float fConeHeight)
    {
        //Draw trunk.
        {
            glutil::PushStack push(modelMatrix);

            modelMatrix.Scale(glm::vec3(1.0f, fTrunkHeight, 1.0f));
            modelMatrix.Translate(glm::vec3(0.0f, 0.5f, 0.0f));

            glm::mat4 modelToWorldMatrix = modelMatrix.Top();
            UniformColorTint.Bind();
            UniformColorTint.SetUniformMat4f("modelToWorldMatrix", modelToWorldMatrix);
            UniformColorTint.SetUniform4f("baseColor", 0.694f, 0.4f, 0.106f, 1.0f);
            CylinderMesh.Render();
            UniformColorTint.UnBind();
        }

        //Draw the treetop
        {
            glutil::PushStack push(modelMatrix);

            modelMatrix.Translate(glm::vec3(0.0f, fTrunkHeight, 0.0f));
            modelMatrix.Scale(glm::vec3(3.0f, fConeHeight, 3.0f));

            glm::mat4 modelToWorldMatrix = modelMatrix.Top();
            UniformColorTint.Bind();
            UniformColorTint.SetUniformMat4f("modelToWorldMatrix", modelToWorldMatrix);
            UniformColorTint.SetUniform4f("baseColor", 0.0f, 1.0f, 0.0f, 1.0f);
            ConeMesh.Render();
            UniformColorTint.UnBind();
        }
    }

    void WorldScene::DrawParthenon(glutil::MatrixStack &modelMatrix)
    {
        const float g_fParthenonWidth = 14.0f;
        const float g_fParthenonLength = 20.0f;
        const float g_fParthenonColumnHeight = 5.0f;
        const float g_fParthenonBaseHeight = 1.0f;
        const float g_fParthenonTopHeight = 2.0f;
        //Draw base.
        {
            glutil::PushStack push(modelMatrix);

            modelMatrix.Scale(glm::vec3(g_fParthenonWidth, g_fParthenonBaseHeight, g_fParthenonLength));
            modelMatrix.Translate(glm::vec3(0.0f, 0.5f, 0.0f));


            glm::mat4 modelToWorldMatrix = modelMatrix.Top();
            UniformColorTint.Bind();
            UniformColorTint.SetUniformMat4f("modelToWorldMatrix", modelToWorldMatrix);
            UniformColorTint.SetUniform4f("baseColor", 0.9f, 0.9f, 0.9f, 0.9f);
            CubeTintMesh.Render();
            UniformColorTint.UnBind();
        }

        //Draw top.
        {
            glutil::PushStack push(modelMatrix);

            modelMatrix.Translate(glm::vec3(0.0f, g_fParthenonColumnHeight + g_fParthenonBaseHeight, 0.0f));
            modelMatrix.Scale(glm::vec3(g_fParthenonWidth, g_fParthenonTopHeight, g_fParthenonLength));
            modelMatrix.Translate(glm::vec3(0.0f, 0.5f, 0.0f));

            glm::mat4 modelToWorldMatrix = modelMatrix.Top();
            UniformColorTint.Bind();
            UniformColorTint.SetUniformMat4f("modelToWorldMatrix", modelToWorldMatrix);
            UniformColorTint.SetUniform4f("baseColor", 0.9f, 0.9f, 0.9f, 0.9f);
            CubeTintMesh.Render();
            UniformColorTint.UnBind();
        }

        //Draw columns.
        const float fFrontZVal = (g_fParthenonLength / 2.0f) - 1.0f;
        const float fRightXVal = (g_fParthenonWidth / 2.0f) - 1.0f;

        for(int iColumnNum = 0; iColumnNum < int(g_fParthenonWidth / 2.0f); iColumnNum++)
        {
            {
                glutil::PushStack push(modelMatrix);
                modelMatrix.Translate(glm::vec3((2.0f * iColumnNum) - (g_fParthenonWidth / 2.0f) + 1.0f,
                                                g_fParthenonBaseHeight, fFrontZVal));

                DrawColumn(modelMatrix, g_fParthenonColumnHeight);
            }
            {
                glutil::PushStack push(modelMatrix);
                modelMatrix.Translate(glm::vec3((2.0f * iColumnNum) - (g_fParthenonWidth / 2.0f) + 1.0f,
                                                g_fParthenonBaseHeight, -fFrontZVal));

                DrawColumn(modelMatrix, g_fParthenonColumnHeight);
            }
        }

        //Don't draw the first or last columns, since they've been drawn already.
        for(int iColumnNum = 1; iColumnNum < int((g_fParthenonLength - 2.0f) / 2.0f); iColumnNum++)
        {
            {
                glutil::PushStack push(modelMatrix);
                modelMatrix.Translate(glm::vec3(fRightXVal,
                                                g_fParthenonBaseHeight, (2.0f * iColumnNum) - (g_fParthenonLength / 2.0f) + 1.0f));

                DrawColumn(modelMatrix, g_fParthenonColumnHeight);
            }
            {
                glutil::PushStack push(modelMatrix);
                modelMatrix.Translate(glm::vec3(-fRightXVal,
                                                g_fParthenonBaseHeight, (2.0f * iColumnNum) - (g_fParthenonLength / 2.0f) + 1.0f));

                DrawColumn(modelMatrix, g_fParthenonColumnHeight);
            }
        }

        //Draw interior.
        {
            glutil::PushStack push(modelMatrix);

            modelMatrix.Translate(glm::vec3(0.0f, 1.0f, 0.0f));
            modelMatrix.Scale(glm::vec3(g_fParthenonWidth - 6.0f, g_fParthenonColumnHeight,
                                        g_fParthenonLength - 6.0f));
            modelMatrix.Translate(glm::vec3(0.0f, 0.5f, 0.0f));

            ObjectColor.Bind();
            ObjectColor.SetUniformMat4f("modelToWorldMatrix", modelMatrix.Top());
            CubeColorMesh.Render();
            ObjectColor.UnBind();
        }

        //Draw headpiece.
        {
            glutil::PushStack push(modelMatrix);

            modelMatrix.Translate(glm::vec3(
                    0.0f,
                    g_fParthenonColumnHeight + g_fParthenonBaseHeight + (g_fParthenonTopHeight / 2.0f),
                    g_fParthenonLength / 2.0f));
            modelMatrix.RotateX(-135.0f);
            modelMatrix.RotateY(45.0f);

            ObjectColor.Bind();
            ObjectColor.SetUniformMat4f("modelToWorldMatrix", modelMatrix.Top());
            CubeColorMesh.Render();
            ObjectColor.UnBind();
        }
    }

    //Columns are 1x1 in the X/Z, and fHieght units in the Y.
    void WorldScene::DrawColumn(glutil::MatrixStack &modelMatrix, float fHeight)
    {
        const float g_fColumnBaseHeight = 0.25f;
        //Draw the bottom of the column.
        {
            glutil::PushStack push(modelMatrix);

            modelMatrix.Scale(glm::vec3(1.0f, g_fColumnBaseHeight, 1.0f));
            modelMatrix.Translate(glm::vec3(0.0f, 0.5f, 0.0f));

            UniformColorTint.Bind();
            UniformColorTint.SetUniformMat4f("modelToWorldMatrix", modelMatrix.Top());
            UniformColorTint.SetUniform4f("baseColor", 1.0f, 1.0f, 1.0f, 1.0f);
            CubeTintMesh.Render();
            UniformColorTint.UnBind();
        }

        //Draw the top of the column.
        {
            glutil::PushStack push(modelMatrix);

            modelMatrix.Translate(glm::vec3(0.0f, fHeight - g_fColumnBaseHeight, 0.0f));
            modelMatrix.Scale(glm::vec3(1.0f, g_fColumnBaseHeight, 1.0f));
            modelMatrix.Translate(glm::vec3(0.0f, 0.5f, 0.0f));

            UniformColorTint.Bind();
            UniformColorTint.SetUniformMat4f("modelToWorldMatrix", modelMatrix.Top());
            UniformColorTint.SetUniform4f("baseColor", 0.9f, 0.9f, 0.9f, 0.9f);
            CubeTintMesh.Render();
            UniformColorTint.UnBind();
        }

        //Draw the main column.
        {
            glutil::PushStack push(modelMatrix);

            modelMatrix.Translate(glm::vec3(0.0f, g_fColumnBaseHeight, 0.0f));
            modelMatrix.Scale(glm::vec3(0.8f, fHeight - (g_fColumnBaseHeight * 2.0f), 0.8f));
            modelMatrix.Translate(glm::vec3(0.0f, 0.5f, 0.0f));

            UniformColorTint.Bind();
            UniformColorTint.SetUniformMat4f("modelToWorldMatrix", modelMatrix.Top());
            UniformColorTint.SetUniform4f("baseColor", 0.9f, 0.9f, 0.9f, 0.9f);
            CylinderMesh.Render();
            UniformColorTint.UnBind();
        }
    }

}