#include "AddRequest.h"
#include "Application.h"
#include "storage/DBStorage.h" 
#include "storage/Entity.h" 
#include "AddResponse.h"
#include "Context.h"

using namespace io_utility;

namespace {
	Request* CreateAddRequest() {
		std::string user_name;
		Environment env;
		return new AddRequest(user_name, env);
	}

	const bool registered = RequestFactory::Instance().Register("AddRequest", CreateAddRequest);
}


AddRequest::AddRequest(const std::string &user, const Environment &env) : Request(user, env)
{
	if (registered) {

	}
}


AddRequest::~AddRequest()
{
}


std::unique_ptr<Response> AddRequest::DoRequest(const std::string &user) {
	auto res = std::unique_ptr<AddResponse>(dynamic_cast<AddResponse*>(ResponseFactory::Instance().CreateObject("AddResponse")));
	res->movieID = "2";
	
	return res;

}
