
#include "Renderer.h"
#include <iostream>
#include <glm/gtc/type_ptr.hpp>
#include <glutil/glutil.h>

void  GLClearError() {
	while (glGetError() != GL_NO_ERROR);
}

bool GLLogCall(const char* function, const char* file, int line) {
	while (GLenum error = glGetError()) {
		std::cout << "[OpenGl Error](" << std::hex << error << std::dec << "): " <<
			function << " " << file << ":" << line << '\n';
		return false;
	}
	return true;
}

Renderer::Renderer(): fzNear{1.0f}, fzFar{100.0f},ubo(nullptr) {
}

Renderer::~Renderer() {
}

void Renderer::ViewPort(int width, int height)
{
    glutil::MatrixStack persMatrix;
    persMatrix.Perspective(45.0f, (width / (float)height), fzNear, fzFar);
    glm::mat4 cameraToClipMatrix = persMatrix.Top();

    // Set all registered shaders uniform camera to clip matrix.
//    for (auto shader1 : shaders) {
//        shader1->Bind();
//        shader1->SetUniformMat4f("cameraToClipMatrix", cameraToClipMatrix);
//        shader1->UnBind();
//    }

    // Global Uniforms
    if (ubo != nullptr) {
        ubo->Bind();
        ubo->SetGlobalUniformDataIndex0(cameraToClipMatrix);
        ubo->UnBind();
    }

    GLCall(glViewport(0, 0, (GLsizei)width, (GLsizei)height));
}

void Renderer::Clear() const
{
    GLCall(glClearColor(0.0f, 0.0f, 0.0f, 0.0f));
    GLCall(glClearDepth(1.0f));
	GLCall(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
}

void Renderer::Draw(const VertexArray & va, const IndexBuffer & ib, const Shader & shader) const
{
	shader.Bind();
	va.Bind();
	ib.Bind();
	GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));

}

void Renderer::RegisterShader(Shader* shader) {
    shaders.push_back(shader);
}

void Renderer::RegisterGlobalUniform(GlobalUniformBuffer *ubo) {
    this->ubo = ubo;
}

