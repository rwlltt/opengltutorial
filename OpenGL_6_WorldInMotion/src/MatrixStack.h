//
// Created by rodney on 1/06/2020.
//

#ifndef OPENGL_TUT_HIERARCHYMODEL_MATRIXSTACK_H
#define OPENGL_TUT_HIERARCHYMODEL_MATRIXSTACK_H

#include <glm/glm.hpp>
#include <stack>
#include "Geometry.h"

class MatrixStack
{

public:
    MatrixStack()
            : m_currMat(1.0f)
    {
    }

    const glm::mat4 &Top()
    {
        return m_currMat;
    }

    void RotateX(float fAngDeg)
    {
        m_currMat = m_currMat * glm::mat4(geometry.RotateX(fAngDeg));
    }

    void RotateY(float fAngDeg)
    {
        m_currMat = m_currMat * glm::mat4(geometry.RotateY(fAngDeg));
    }

    void RotateZ(float fAngDeg)
    {
        m_currMat = m_currMat * glm::mat4(geometry.RotateZ(fAngDeg));
    }

    void Scale(const glm::vec3 &scaleVec)
    {
        glm::mat4 scaleMat(1.0f);
        scaleMat[0].x = scaleVec.x;
        scaleMat[1].y = scaleVec.y;
        scaleMat[2].z = scaleVec.z;

        m_currMat = m_currMat * scaleMat;
    }

    void Translate(const glm::vec3 &offsetVec)
    {
        glm::mat4 translateMat(1.0f);
        translateMat[3] = glm::vec4(offsetVec, 1.0f);

        m_currMat = m_currMat * translateMat;
    }

    void Push()
    {
        m_matrices.push(m_currMat);
    }

    void Pop()
    {
        m_currMat = m_matrices.top();
        m_matrices.pop();
    }

private:
    glm::mat4 m_currMat;
    std::stack<glm::mat4> m_matrices;
    Geometry geometry;
};


#endif //OPENGL_TUT_HIERARCHYMODEL_MATRIXSTACK_H
