#shader vertex
#version 330 core

layout(location = 0) in vec4 position;
layout (location = 1) in vec4 color;
smooth out vec4 theColor;

void main()
{
    gl_Position = position;
    theColor = color;
}


#shader fragment
#version 330 core

smooth in vec4 theColor;
out vec4 outputColor;

void main()
{
    float lerpValue = gl_FragCoord.y / 500.0f;
    outputColor = mix(vec4(0.6f, 0.7f, 1.0f, 1.0f), vec4(0.3f, 0.3f, 0.2f, 1.0f), lerpValue);
    //outputColor = theColor;
}