#include "TestTriangleFragCoord.h"

#include <Renderer.h>
#include "imgui/imgui.h"

namespace test {

    TestTriangleFragCoord::TestTriangleFragCoord()
		: vertexPositions {   0.0f,  0.75f, 1.0f, 1.0f,
                              0.75f,  0.0f, 1.0f, 1.0f,
                             -0.75f,  0.0f, 1.0f, 1.0f},
                             shader {"res/shaders/TriangleFragCoord.shader"},
                             vb {vertexPositions, 12 * sizeof(vertexPositions[0]) }
	{
        layout.Push<float>(4);
        va.AddBuffer(vb, layout);
    }

    TestTriangleFragCoord::~TestTriangleFragCoord()
	{
	}

	void TestTriangleFragCoord::OnUpdate(float deltaTime)
	{
	}

	void TestTriangleFragCoord::OnRender()
	{
        shader.Bind();
        va.Bind();
        GLCall(glDrawArrays(GL_TRIANGLES, 0, 3));
	}

	void TestTriangleFragCoord::OnImGuiRender()
	{
		//ImGui::ColorEdit4("Clear Colour", m_clearColour);
	}


}