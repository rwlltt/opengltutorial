#ifndef TUT_TEST_TRIANGLEFRAGCOORD
#define TUT_TEST_TRIANGLEFRAGCOORD

#include <Shader.h>
#include <VertexBuffer.h>
#include <VertexArray.h>
#include <VertexBufferLayout.h>
#include "Test.h"

namespace test {

	class TestTriangleFragCoord : public Test {
	private:
        float vertexPositions[12];
        Shader shader;
        VertexBuffer vb;
        VertexArray va;
        VertexBufferLayout layout;

    public:
        TestTriangleFragCoord();
		~TestTriangleFragCoord();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;

	};


}

#endif /* TUT_TEST_TRIANGLEFRAGCOORD*/

