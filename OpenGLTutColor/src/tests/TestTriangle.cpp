#include "TestTriangle.h"

#include <Renderer.h>
#include "imgui/imgui.h"

namespace test {

    TestTriangle::TestTriangle()
		: vertexPositions {   0.0f,  0.75f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                              0.75f,  0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
                             -0.75f,  0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
                              0.75f,  0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
                              0.0f,  -0.75f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
                             -0.75f,  0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                             -0.75f,  0.20f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
                             -0.75f,  0.75f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
                              -0.20f,  0.75f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f},
                             shader {"res/shaders/Triangle1.shader"},
                             vb {vertexPositions, 72 * sizeof(vertexPositions[0]) }
	{
        layout.Push<float>(4);
        layout.Push<float>(4);
        va.AddBuffer(vb, layout);
    }

    TestTriangle::~TestTriangle()
	{
	}

	void TestTriangle::OnUpdate(float deltaTime)
	{
	}

	void TestTriangle::OnRender()
	{
        shader.Bind();
        va.Bind();
        GLCall(glDrawArrays(GL_TRIANGLES, 0, 9));
	}

	void TestTriangle::OnImGuiRender()
	{
		//ImGui::ColorEdit4("Clear Colour", m_clearColour);
	}


}