#ifndef TUT_CONSOLERESPONSE_
#define TUT_CONSOLERESPONSE_

#include "Response.h"
#include "storage/DBStorage.h" 
#include "Context.h"

class ConsoleResponse : public Response
{
	std::stringstream output_;

public:
	ConsoleResponse();
	virtual ~ConsoleResponse();

	ConsoleResponse& operator= (const ConsoleResponse &other) = delete;
	ConsoleResponse* operator&(ConsoleResponse object) = delete;


public:
	void Display() const;
	std::tuple < std::string, std::set<char>, std::map<char, std::string>> Prompt() const;

	std::string movieID{ "" };

};

#endif /* TUT_CONSOLERESPONSE_ */