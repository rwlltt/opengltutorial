#ifndef TUT_TEST_TRIANGLESHADERUNIFORM
#define TUT_TEST_TRIANGLESHADERUNIFORM

#include <Shader.h>
#include <VertexBuffer.h>
#include <VertexArray.h>
#include <VertexBufferLayout.h>
#include "Test.h"

namespace test {

	class OrthoCube : public Test {
	private:
        float vertexPositions[288];
        Shader shader;
        VertexBuffer vb;
        VertexArray va;
        VertexBufferLayout layout;
        //float fXOffset = 0.0f, fYOffset = 0.0f;
        float deltaTime = 0.0f;

    public:
        OrthoCube();
		~OrthoCube();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;
		Shader & UpdateShader() { return shader;};

	private:
        void ComputePositionOffsets(float fElapsedTime);
        void AdjustVertexData();

    };


}

#endif /* TUT_TEST_TRIANGLESHADERUNIFORM*/

