//
// Created by rodney on 4/05/2020.
//

#ifndef OPENGL_APP_TESTSQUARE_H
#define OPENGL_APP_TESTSQUARE_H

#include <Shader.h>
#include <VertexBuffer.h>
#include <VertexArray.h>
#include <VertexBufferLayout.h>
#include <Texture.h>
#include "Test.h"

namespace test {


    class TestSquare: public Test {
    private:
        float vertexPositions[12];
        const struct
        {
            float x, y;
            float tx, ty;
            float r, g, b;
        } vertices[4];
        unsigned int indices[6];
        Shader shader;
        Texture texture;

        VertexBuffer vb;
        VertexArray va;
        VertexBufferLayout layout;
        IndexBuffer ib;
        glm::mat4 proj;
        glm::mat4 view;
        glm::vec2 translationA;
        glm::vec1 translationB;
        glm::vec2 translationC;



    public:
        TestSquare();
        ~TestSquare();

        void OnUpdate(float deltaTime) override;
        void OnRender() override;
        void OnImGuiRender() override;

    };

}

#endif //OPENGL_APP_TESTSQUARE_H
