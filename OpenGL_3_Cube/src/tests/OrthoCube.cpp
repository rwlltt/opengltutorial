#include "OrthoCube.h"

#include <Renderer.h>
#include "imgui/imgui.h"
#include <glm/gtc/type_ptr.hpp>
#define ARRAY_COUNT( array ) (sizeof( array ) / (sizeof( array[0] ) * (sizeof( array ) != sizeof(void*) || sizeof( array[0] ) <= sizeof(void*))))

namespace test {


    OrthoCube::OrthoCube()
		: vertexPositions {
                    0.25f,  0.25f, -1.25f, 1.0f,
                    0.25f, -0.25f, -1.25f, 1.0f,
                    -0.25f,  0.25f, -1.25f, 1.0f,

                    0.25f, -0.25f, -1.25f, 1.0f,
                    -0.25f, -0.25f, -1.25f, 1.0f,
                    -0.25f,  0.25f, -1.25f, 1.0f,

                    0.25f,  0.25f, -2.75f, 1.0f,
                    -0.25f,  0.25f, -2.75f, 1.0f,
                    0.25f, -0.25f, -2.75f, 1.0f,

                    0.25f, -0.25f, -2.75f, 1.0f,
                    -0.25f,  0.25f, -2.75f, 1.0f,
                    -0.25f, -0.25f, -2.75f, 1.0f,

                    -0.25f,  0.25f, -1.25f, 1.0f,
                    -0.25f, -0.25f, -1.25f, 1.0f,
                    -0.25f, -0.25f, -2.75f, 1.0f,

                    -0.25f,  0.25f, -1.25f, 1.0f,
                    -0.25f, -0.25f, -2.75f, 1.0f,
                    -0.25f,  0.25f, -2.75f, 1.0f,

                    0.25f,  0.25f, -1.25f, 1.0f,
                    0.25f, -0.25f, -2.75f, 1.0f,
                    0.25f, -0.25f, -1.25f, 1.0f,

                    0.25f,  0.25f, -1.25f, 1.0f,
                    0.25f,  0.25f, -2.75f, 1.0f,
                    0.25f, -0.25f, -2.75f, 1.0f,

                    0.25f,  0.25f, -2.75f, 1.0f,
                    0.25f,  0.25f, -1.25f, 1.0f,
                    -0.25f,  0.25f, -1.25f, 1.0f,

                    0.25f,  0.25f, -2.75f, 1.0f,
                    -0.25f,  0.25f, -1.25f, 1.0f,
                    -0.25f,  0.25f, -2.75f, 1.0f,

                    0.25f, -0.25f, -2.75f, 1.0f,
                    -0.25f, -0.25f, -1.25f, 1.0f,
                    0.25f, -0.25f, -1.25f, 1.0f,

                    0.25f, -0.25f, -2.75f, 1.0f,
                    -0.25f, -0.25f, -2.75f, 1.0f,
                    -0.25f, -0.25f, -1.25f, 1.0f,




                    0.0f, 0.0f, 1.0f, 1.0f,
                    0.0f, 0.0f, 1.0f, 1.0f,
                    0.0f, 0.0f, 1.0f, 1.0f,

                    0.0f, 0.0f, 1.0f, 1.0f,
                    0.0f, 0.0f, 1.0f, 1.0f,
                    0.0f, 0.0f, 1.0f, 1.0f,

                    0.8f, 0.8f, 0.8f, 1.0f,
                    0.8f, 0.8f, 0.8f, 1.0f,
                    0.8f, 0.8f, 0.8f, 1.0f,

                    0.8f, 0.8f, 0.8f, 1.0f,
                    0.8f, 0.8f, 0.8f, 1.0f,
                    0.8f, 0.8f, 0.8f, 1.0f,

                    0.0f, 1.0f, 0.0f, 1.0f,
                    0.0f, 1.0f, 0.0f, 1.0f,
                    0.0f, 1.0f, 0.0f, 1.0f,

                    0.0f, 1.0f, 0.0f, 1.0f,
                    0.0f, 1.0f, 0.0f, 1.0f,
                    0.0f, 1.0f, 0.0f, 1.0f,

                    0.5f, 0.5f, 0.0f, 1.0f,
                    0.5f, 0.5f, 0.0f, 1.0f,
                    0.5f, 0.5f, 0.0f, 1.0f,

                    0.5f, 0.5f, 0.0f, 1.0f,
                    0.5f, 0.5f, 0.0f, 1.0f,
                    0.5f, 0.5f, 0.0f, 1.0f,

                    1.0f, 0.0f, 0.0f, 1.0f,
                    1.0f, 0.0f, 0.0f, 1.0f,
                    1.0f, 0.0f, 0.0f, 1.0f,

                    1.0f, 0.0f, 0.0f, 1.0f,
                    1.0f, 0.0f, 0.0f, 1.0f,
                    1.0f, 0.0f, 0.0f, 1.0f,

                    0.0f, 1.0f, 1.0f, 1.0f,
                    0.0f, 1.0f, 1.0f, 1.0f,
                    0.0f, 1.0f, 1.0f, 1.0f,

                    0.0f, 1.0f, 1.0f, 1.0f,
                    0.0f, 1.0f, 1.0f, 1.0f,
                    0.0f, 1.0f, 1.0f, 1.0f,

            },
                             //shader {"res/shaders/CubeUniform.shader"},
                             // shader {"res/shaders/CubeManualPerspective.shader"},
                             shader {"res/shaders/CubeMatrixPerspective.shader"},
                             vb {vertexPositions, 288 * sizeof(vertexPositions[0]), false }
	{
        //layout.Push<float>(4);
        //layout.Push<float>(4);
        //va.AddBuffer(vb, layout);
        //
        va.Bind();
        vb.Bind();
        GLCall(glEnableVertexAttribArray(0));
        GLCall(glEnableVertexAttribArray(1));
        size_t colorData = sizeof(vertexPositions) / 2;
        GLCall(glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0));
        GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)colorData));

        shader.Bind();
        //shader.SetUniform1f("frustumScale", 1.0f);
        //shader.SetUniform1f("zNear", 1.0f);
        //shader.SetUniform1f("zFar", 3.0f);
        //float fFrustumScale = 1.0f; float fzNear = 0.5f; float fzFar = 3.0f;
//        glm::mat4 gCWMatrix = glm::mat4();// = glm::make_mat4(theMatrix);
//        gCWMatrix[0][0] = fFrustumScale;
//        gCWMatrix[1][1] = fFrustumScale;
//        gCWMatrix[2][2] = (fzFar + fzNear) / (fzNear - fzFar);;
//        gCWMatrix[3][2] = (2 * fzFar * fzNear) / (fzNear - fzFar);
//        gCWMatrix[2][3] = -1.0f;
//        shader.SetUniformMat4f("perspectiveMatrix", gCWMatrix);
    }

    OrthoCube::~OrthoCube()
	{
	}

	void OrthoCube::OnUpdate(float deltaTime)
	{
        // Store the delta
        this->deltaTime = deltaTime;
        //ComputePositionOffsets(deltaTime);
	}

	void OrthoCube::OnRender()
	{
        //AdjustVertexData(); Using a uniform to send the offset rather than all thew vertices

        shader.Bind();
        shader.SetUniform2f("offset", 0.5f, 0.5f );

        va.Bind();
        GLCall(glDrawArrays(GL_TRIANGLES, 0, 36));
	}

	void OrthoCube::OnImGuiRender()
	{
		//ImGui::ColorEdit4("Clear Colour", m_clearColour);
	}

    void OrthoCube::ComputePositionOffsets(float fElapsedTime)
    {
        const float fLoopDuration = 5.0f;
        const float fScale = 3.14159f * 2.0f / fLoopDuration;

        // glutGet is replaced with deltatime calc with std chrono class
        //float fElapsedTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;

        float fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);

        //fXOffset = cosf(fCurrTimeThroughLoop * fScale) * 0.75f;
        //fYOffset = sinf(fCurrTimeThroughLoop * fScale) * 0.75f;
        //fprintf(stderr, "ComputePositionOffsets %f %f %f\n", fXOffset, fYOffset, fCurrTimeThroughLoop );
    }

    void OrthoCube::AdjustVertexData()
    {
        std::vector<float> fNewData(ARRAY_COUNT(vertexPositions));
        memcpy(&fNewData[0], vertexPositions, sizeof(vertexPositions));

        for(int iVertex = 0; iVertex < ARRAY_COUNT(vertexPositions); iVertex += 4)
        {
            //fNewData[iVertex] += fXOffset;
            //fNewData[iVertex + 1] += fYOffset;
        }

        vb.AdjustVertexData(&fNewData[0], sizeof(vertexPositions));
    }
}