
#include <imgui/imgui.h>
#include "TestSquare.h"
#include "application.h"
#include "glm/gtc/matrix_transform.hpp"

namespace test {

    TestSquare::TestSquare()
             : vertices{
                     -0.75f, -0.75f, 0.0f, 0.0f, 1.f, 0.f, 0.f ,
                      0.75f, -0.75f, 1.0f, 0.0f, 0.f, 1.f, 0.f ,
                      0.75f,  0.75f, 1.0f, 1.0f, 0.f, 0.f, 1.f ,
                     -0.75f,  0.75f, 0.0f, 1.0f, 0.f, 1.f, 1.f },
              indices {
                            2, 1, 0,
                            0, 3, 2
                    },
              shader {"res/shaders/Basic.shader"},
              texture {"res/textures/pal.png"},
              vb {vertices, 28 * sizeof(vertices[0]), false},
              ib {indices, 6},
              proj {glm::ortho(0.0f, 640.0f, 0.0f, 640.0f, -1.0f, 1.0f)},
              view {glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0.2))},
              translationA {-0.51, 0.42}, // Focus
              translationB {1.0f},         // Zoom
              translationC {-0.184f, 0.182f} // iResolution
    {
        // Three locations in vertex shader
        layout.Push<float>(2);
        layout.Push<float>(2);
        layout.Push<float>(3);
        va.AddBuffer(vb, layout);
    }

    TestSquare::~TestSquare()
    {
    }

    void TestSquare::OnUpdate(float deltaTime)
    {
    }

    void TestSquare::OnRender()
    {
        texture.Bind();

        shader.Bind();
        va.Bind();
        {
            //glm::mat4 model = glm::translate(glm::mat4(1.0f), translationA);
            //glm::mat4 s_mvp = proj * view * model;
            shader.Bind();
            shader.SetUniform2f("Focus", translationA.x, translationA.y);
            shader.SetUniform1f("Zoom", translationB.x);
            shader.SetUniform2f("iResolution", translationC.x, translationC.y);
            va.Bind();
            ib.Bind();
            GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));

        }
    }

    void TestSquare::OnImGuiRender()
    {
        //ImGui::ColorEdit4("Clear Colour", m_clearColour);
        {
            ImGui::SliderFloat2("Focus", &translationA.x, -4.0f, 4.0f);
            ImGui::SliderFloat("Zoom", &translationB.x, -1.0f, 20.0f);
            ImGui::SliderFloat2("Resolution", &translationC.x, -1.0f, 1.0f);

            ImGuiIO& io = ImGui::GetIO();
            io.KeyShift = io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
            if (io.KeyShift) {
                if (ImGui::IsKeyPressed('F')) {
                    translationA.x -= 0.005f;
                } else if (ImGui::IsKeyPressed('G'))
                        translationA.y -= 0.005f;
            } else {
                if (ImGui::IsKeyPressed('F')) {
                    translationA.x += 0.005f;
                } else if (ImGui::IsKeyPressed('G'))
                    translationA.y += 0.005f;

            }
        }

    }

}