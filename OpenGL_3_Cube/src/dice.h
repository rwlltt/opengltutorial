
#ifndef DICE_H_
#define DICE_H_

#include <random>

class dice
{
	static std::random_device m_rd;
	static std::mt19937 m_rng;
	std::uniform_int_distribution<std::mt19937::result_type> m_dist6{ 1, 100 }; // distribution range
public:
	dice();
	~dice();

	int roll();
};


#endif /* DICE_H_ */