﻿
//#ifdef _WIN32
//#define APIENTRY __stdcall
//#endif

/* Modern OpenGL with GLAD */
//#include <GLAD/glad.h> /* GLAD before glfw3 for GL.h */
//#include <GLFW/glfw3.h>

#include "Application.h"

//#include "linmath.h"
//#include "Renderer.h"


int main(int argc, char** argv)
{
	try {
		TheApp::Instance().Init();
		TheApp::Instance().Run();
	}
	catch (std::exception &e) {
		std::cout << e.what() << "\n";
	}
	return EXIT_SUCCESS;
}


