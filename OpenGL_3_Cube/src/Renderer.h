#ifndef TUT_RENDERER_
#define TUT_RENDERER_

#include <GLAD/glad.h> 
#include "VertexArray.h"
#include "IndexBuffer.h"
#include "Shader.h"

#define ASSERT(x) if (!(x)) __debugbreak();
#define GLCall(x) GLClearError();\
	x;\
	ASSERT(GLLogCall(#x, __FILE__, __LINE__))


void  GLClearError();

bool GLLogCall(const char* function, const char* file, int line);

class Renderer {
private:
    const float fFrustumScale = 1.0f;
    float fzNear = 0.5f;
    float fzFar = 3.0f;
    float theMatrix[16];

public:
    Renderer();
    ~Renderer();
	void ViewPort(int width, int height);
    void ShaderPerspective(Shader &shader);
	void Clear() const;
	void Draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader) const;
};


#endif
