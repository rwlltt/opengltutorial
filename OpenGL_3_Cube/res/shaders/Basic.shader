#shader vertex
#version 330 core

//layout(location = 0) in vec4 vPos;
layout(location = 0) in vec2 vPos;
layout(location = 1) in vec2 texCoord;
layout(location = 2) in vec3 vCol;

out vec4 v_color;
out vec2 v_TexCoord;

uniform mat4 MVP;
uniform mat4 u_MVP;

void main()
{
	//gl_Position = u_MVP * vPos;
	//gl_Position = MVP * vec4(vPos, 0.0, 1.0);
	//gl_Position = vec4(vPos, 0.0, 1.0);
	//v_color = vCol;
	//vec4 thePos = u_MVP * vPos;
	//vec2 aPos = vec2(thePos.x, thePos.y);
	//mainImage(v_color, aPos);
	v_TexCoord = vPos;
	gl_Position = vec4(vPos, 0.0, 1.0);
};


#shader fragment
#version 330 core

layout(location = 0) out vec4 color;

in vec4 v_color;
in vec2 v_TexCoord;

uniform sampler2D u_Texture;
const int MaxIterations = 1400;
//const vec2 Focus = vec2(-0.51, 0.54);
//const float Zoom = 1.0;
//const vec2 iResolution = vec2(-0.5, 0.5);
uniform vec2 Focus;
uniform float Zoom;
uniform vec2 iResolution;
const vec3 outerColor1 = vec3(1.0); //uniform
const vec3 outerColor2 = vec3(0.0, 0.0, 1.0); //uniform

vec3 colorIt(int iteration, float sqLengthZ) {
    // If the point is within the mandlebrot set
    // just color it black
    if(iteration == MaxIterations)
        return vec3(0.0);

    // Else we give it a smoothed color
   	float ratio = (float(iteration) - log2(log2(sqLengthZ))) / float(MaxIterations);

    // Procedurally generated colors
    return mix(vec3(1.0, 1.0, 1.0), vec3(1.0, 1.0, 0.0), sqrt(ratio));
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    // C is the aspect-ratio corrected UV coordinate.
    vec2 c = (-1.0 + 2.0 * fragCoord / iResolution.xy) * vec2(iResolution.x / iResolution.y, 1.0);

    // Apply scaling, then offset to get a zoom effect
    c = (c * exp(-Zoom)) + Focus;
	vec2 z = c;

    int iteration = 0;

    while(iteration < MaxIterations) {
        // Precompute for efficiency
   	    float zr2 = z.x * z.x;
        float zi2 = z.y * z.y;

        // The larger the square length of Z,
        // the smoother the shading
        if(zr2 + zi2 > 32.0) break;

        // Complex multiplication, then addition
    	z = vec2(zr2 - zi2, 2.0 * z.x * z.y) + c;
        ++iteration;
    }

    // Generate the colors
    fragColor = vec4(colorIt(iteration, dot(z,z)), 1.0);
    fragColor = texture(u_Texture, vec2((iteration == MaxIterations ? 0.0 : float(iteration)) / 100.0, 0.0));

    // Apply gamma correction
    //fragColor.rgb = pow(fragColor.rgb, vec3(0.5));
}

void main()
{
	//vec4 texColor = texture(u_Texture, v_TexCoord);
	//color = texColor;
	//vec4 v_color;
	mainImage(color, v_TexCoord);
	//color = v_color;//mainImage(texColor);
};

