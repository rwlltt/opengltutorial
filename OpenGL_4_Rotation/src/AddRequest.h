#ifndef TUT_ADDREQUEST_
#define TUT_ADDREQUEST_

#include "Request.h"

class AddRequest: public Request
{
public:
	AddRequest(const std::string &user, const Environment &env);
	virtual ~AddRequest();

	//	InitAppRequest(const InitAppRequest& other) = delete;
	AddRequest& operator= (const AddRequest &other) = delete;
	AddRequest* operator&(AddRequest object) = delete;

	virtual std::unique_ptr<Response> DoRequest(const std::string &user);

};

#endif /* TUT_ADDREQUEST_*/