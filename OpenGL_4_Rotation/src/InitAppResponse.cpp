#include "InitAppResponse.h"
#include "Application.h"


using namespace io_utility;

namespace {
	Response* CreateInitAppResponse() {
		return new InitAppResponse();
	}

	const bool registered = ResponseFactory::Instance().Register("InitAppResponse", CreateInitAppResponse);
}


InitAppResponse::InitAppResponse() : test()
{
}


InitAppResponse::~InitAppResponse()
{
}

void InitAppResponse::Display() const {
	//test.OnUpdate(0.0f);
	//test.OnRender();
	//print({ "App Started\n" });
}

std::tuple<std::string, std::set<char>, std::map<char, std::string>> InitAppResponse::Prompt() const {
	std::string prompt("Continue [AaQqCc]:");
	std::set<char> choices{ 'a', 'A', 'q', 'Q' };
	std::map<char, std::string> reqs{ {'a', "AddRequest"}, {'A', "AddRequest"}, {'q', "Quit"}, {'Q', "Quit"} };
	return std::make_tuple(prompt, choices, reqs);
}

void InitAppResponse::OnUpdate(float deltaTime) 
{
	test.OnUpdate(deltaTime);
}

void InitAppResponse::OnRender() 
{
	test.OnRender();
}

void InitAppResponse::OnImGuiRender() 
{
	test.OnImGuiRender();
}



