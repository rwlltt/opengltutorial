//
// Created by rodney on 10/05/2020.
//

#ifndef OPENGL_TUT_MODELTRANSLATION_STATIONARYWEDGE_H
#define OPENGL_TUT_MODELTRANSLATION_STATIONARYWEDGE_H


#include "WedgeInstance.h"

class StationaryWedge: public WedgeInstance {
public:
    glm::vec3 OffsetXYZ(float fElapsedTime) override;
};


#endif //OPENGL_TUT_MODELTRANSLATION_STATIONARYWEDGE_H
