#include "TestTriangle.h"

#include <Renderer.h>
#include "imgui/imgui.h"
#define ARRAY_COUNT( array ) (sizeof( array ) / (sizeof( array[0] ) * (sizeof( array ) != sizeof(void*) || sizeof( array[0] ) <= sizeof(void*))))

namespace test {


    TestTriangle::TestTriangle()
		: vertexPositions {   0.25f, 0.25f, 0.0f, 1.0f,
                              0.25f, -0.25f, 0.0f, 1.0f,
                             -0.25f, -0.25f, 0.0f, 1.0f},
                             shader {"res/shaders/Triangle1.shader"},
                             vb {vertexPositions, 12 * sizeof(vertexPositions[0]), true }
	{
        layout.Push<float>(4);
        va.AddBuffer(vb, layout);
        // The above does this for location 0 in vertex shader
        //GLCall(glEnableVertexAttribArray(0));
        //GLCall(glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0));
    }

    TestTriangle::~TestTriangle()
	{
	}

	void TestTriangle::OnUpdate(float deltaTime)
	{
        ComputePositionOffsets(deltaTime);
	}

	void TestTriangle::OnRender()
	{
        AdjustVertexData();

        shader.Bind();
        va.Bind();
        GLCall(glDrawArrays(GL_TRIANGLES, 0, 9));
	}

	void TestTriangle::OnImGuiRender()
	{
		//ImGui::ColorEdit4("Clear Colour", m_clearColour);
	}

    void TestTriangle::ComputePositionOffsets(float fElapsedTime)
    {
        const float fLoopDuration = 5.0f;
        const float fScale = 3.14159f * 2.0f / fLoopDuration;

        // glutGet is replaced with deltatime calc with std chrono class
        //float fElapsedTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;

        float fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);

        fXOffset = cosf(fCurrTimeThroughLoop * fScale) * 0.5f;
        fYOffset = sinf(fCurrTimeThroughLoop * fScale) * 0.5f;
        //fprintf(stderr, "ComputePositionOffsets %f %f %f\n", fXOffset, fYOffset, fCurrTimeThroughLoop );
    }

    void TestTriangle::AdjustVertexData()
    {
        std::vector<float> fNewData(ARRAY_COUNT(vertexPositions));
        memcpy(&fNewData[0], vertexPositions, sizeof(vertexPositions));

        for(int iVertex = 0; iVertex < ARRAY_COUNT(vertexPositions); iVertex += 4)
        {
            fNewData[iVertex] += fXOffset;
            fNewData[iVertex + 1] += fYOffset;
        }

        vb.AdjustVertexData(&fNewData[0], sizeof(vertexPositions));
    }
}