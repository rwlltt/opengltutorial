#ifndef TUT_TEST_WEDGEDEPTHBASEINDEX
#define TUT_TEST_WEDGEDEPTHBASEINDEX

#include <Shader.h>
#include <VertexBuffer.h>
#include <VertexArray.h>
#include <VertexBufferLayout.h>
#include "Test.h"
#include "WedgeInstance.h"

namespace test {

	class WedgeBaseIndex : public Test {
	private:
        float vertexData[56];
        GLushort indexData[24];
        Shader shader;
        VertexBuffer vb;
        VertexArray va;
        //VertexArray va2;
        VertexBufferLayout layout;
        IndexBuffer ib;
        int numberOfVertices;
        float deltaTime = 0.0f;
        std::unique_ptr<WedgeInstance> wedgeInstances[5];
        glm::vec3 translationA;
        glm::vec3 translationB;

    public:
        WedgeBaseIndex();
		~WedgeBaseIndex();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;
		Shader & UpdateShader() { return shader;};

	private:
        void ComputePositionOffsets(float fElapsedTime);
        void AdjustVertexData();

    };


}

#endif /* TUT_TEST_WEDGEDEPTHBASEINDEX*/

