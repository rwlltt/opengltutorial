#include "Wedge.h"

#include <Renderer.h>
#include "imgui/imgui.h"
#include <glm/gtc/type_ptr.hpp>
#define ARRAY_COUNT( array ) (sizeof( array ) / (sizeof( array[0] ) * (sizeof( array ) != sizeof(void*) || sizeof( array[0] ) <= sizeof(void*))))
#define RIGHT_EXTENT 0.8f
#define LEFT_EXTENT -RIGHT_EXTENT
#define TOP_EXTENT 0.20f
#define MIDDLE_EXTENT 0.0f
#define BOTTOM_EXTENT -TOP_EXTENT
#define FRONT_EXTENT -1.25f
#define REAR_EXTENT -1.75f

#define GREEN_COLOR 0.75f, 0.75f, 1.0f, 1.0f
#define BLUE_COLOR 	0.0f, 0.5f, 0.0f, 1.0f
#define RED_COLOR 1.0f, 0.0f, 0.0f, 1.0f
#define GREY_COLOR 0.8f, 0.8f, 0.8f, 1.0f
#define BROWN_COLOR 0.5f, 0.5f, 0.0f, 1.0f

namespace test {


    Wedge::Wedge()
		: vertexPositions {
                    //Object 1 positions
                    LEFT_EXTENT,	TOP_EXTENT,		REAR_EXTENT,
                    LEFT_EXTENT,	MIDDLE_EXTENT,	FRONT_EXTENT,
                    RIGHT_EXTENT,	MIDDLE_EXTENT,	FRONT_EXTENT,
                    RIGHT_EXTENT,	TOP_EXTENT,		REAR_EXTENT,

                    LEFT_EXTENT,	BOTTOM_EXTENT,	REAR_EXTENT,
                    LEFT_EXTENT,	MIDDLE_EXTENT,	FRONT_EXTENT,
                    RIGHT_EXTENT,	MIDDLE_EXTENT,	FRONT_EXTENT,
                    RIGHT_EXTENT,	BOTTOM_EXTENT,	REAR_EXTENT,

                    LEFT_EXTENT,	TOP_EXTENT,		REAR_EXTENT,
                    LEFT_EXTENT,	MIDDLE_EXTENT,	FRONT_EXTENT,
                    LEFT_EXTENT,	BOTTOM_EXTENT,	REAR_EXTENT,

                    RIGHT_EXTENT,	TOP_EXTENT,		REAR_EXTENT,
                    RIGHT_EXTENT,	MIDDLE_EXTENT,	FRONT_EXTENT,
                    RIGHT_EXTENT,	BOTTOM_EXTENT,	REAR_EXTENT,

                    LEFT_EXTENT,	BOTTOM_EXTENT,	REAR_EXTENT,
                    LEFT_EXTENT,	TOP_EXTENT,		REAR_EXTENT,
                    RIGHT_EXTENT,	TOP_EXTENT,		REAR_EXTENT,
                    RIGHT_EXTENT,	BOTTOM_EXTENT,	REAR_EXTENT,

                    //Object 2 positions
                    TOP_EXTENT,		RIGHT_EXTENT,	REAR_EXTENT,
                    MIDDLE_EXTENT,	RIGHT_EXTENT,	FRONT_EXTENT,
                    MIDDLE_EXTENT,	LEFT_EXTENT,	FRONT_EXTENT,
                    TOP_EXTENT,		LEFT_EXTENT,	REAR_EXTENT,

                    BOTTOM_EXTENT,	RIGHT_EXTENT,	REAR_EXTENT,
                    MIDDLE_EXTENT,	RIGHT_EXTENT,	FRONT_EXTENT,
                    MIDDLE_EXTENT,	LEFT_EXTENT,	FRONT_EXTENT,
                    BOTTOM_EXTENT,	LEFT_EXTENT,	REAR_EXTENT,

                    TOP_EXTENT,		RIGHT_EXTENT,	REAR_EXTENT,
                    MIDDLE_EXTENT,	RIGHT_EXTENT,	FRONT_EXTENT,
                    BOTTOM_EXTENT,	RIGHT_EXTENT,	REAR_EXTENT,

                    TOP_EXTENT,		LEFT_EXTENT,	REAR_EXTENT,
                    MIDDLE_EXTENT,	LEFT_EXTENT,	FRONT_EXTENT,
                    BOTTOM_EXTENT,	LEFT_EXTENT,	REAR_EXTENT,

                    BOTTOM_EXTENT,	RIGHT_EXTENT,	REAR_EXTENT,
                    TOP_EXTENT,		RIGHT_EXTENT,	REAR_EXTENT,
                    TOP_EXTENT,		LEFT_EXTENT,	REAR_EXTENT,
                    BOTTOM_EXTENT,	LEFT_EXTENT,	REAR_EXTENT,

                    //Object 1 colors
                    GREEN_COLOR,
                    GREEN_COLOR,
                    GREEN_COLOR,
                    GREEN_COLOR,

                    BLUE_COLOR,
                    BLUE_COLOR,
                    BLUE_COLOR,
                    BLUE_COLOR,

                    RED_COLOR,
                    RED_COLOR,
                    RED_COLOR,

                    GREY_COLOR,
                    GREY_COLOR,
                    GREY_COLOR,

                    BROWN_COLOR,
                    BROWN_COLOR,
                    BROWN_COLOR,
                    BROWN_COLOR,

                    //Object 2 colors
                    RED_COLOR,
                    RED_COLOR,
                    RED_COLOR,
                    RED_COLOR,

                    BROWN_COLOR,
                    BROWN_COLOR,
                    BROWN_COLOR,
                    BROWN_COLOR,

                    BLUE_COLOR,
                    BLUE_COLOR,
                    BLUE_COLOR,

                    GREEN_COLOR,
                    GREEN_COLOR,
                    GREEN_COLOR,

                    GREY_COLOR,
                    GREY_COLOR,
                    GREY_COLOR,
                    GREY_COLOR,
            }, indexData {
                        0, 2, 1,
                        3, 2, 0,

                        4, 5, 6,
                        6, 7, 4,

                        8, 9, 10,
                        11, 13, 12,

                        14, 16, 15,
                        17, 16, 14,
                },
                shader {"res/shaders/MatrixPerspective.shader"},
                vb {vertexPositions, 252 * sizeof(vertexPositions[0]), false },
                ib {indexData, 24}
	{
        //layout.Push<float>(4);
        //layout.Push<float>(4);
        //va.AddBuffer(vb, layout);
        //
        int numberOfVertices = 36;
        size_t colorDataOffset = sizeof(float) * 3 * numberOfVertices;
        vb.Bind(); // set the GL_ARRAY_BUFFER binding once

        va.Bind(); // element buffer binding is part of the VAO state,
        GLCall(glEnableVertexAttribArray(0));
        GLCall(glEnableVertexAttribArray(1));
        GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0));
        GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)colorDataOffset));
        ib.Bind();  //  GL_ELEMENT_ARRAY_BUFFER is set for each VAO
        va.UnBind();

        va2.Bind(); // element buffer binding is part of the VAO state,
        size_t posDataOffset = sizeof(float) * 3 * (numberOfVertices/2);
        colorDataOffset += sizeof(float) * 4 * (numberOfVertices/2);
        GLCall(glEnableVertexAttribArray(0));
        GLCall(glEnableVertexAttribArray(1));
        GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)posDataOffset));
        GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)colorDataOffset));
        ib.Bind(); //  GL_ELEMENT_ARRAY_BUFFER is set for each VAO
        va2.UnBind();

        //shader.Bind();
    }

    Wedge::~Wedge()
	{
	}

	void Wedge::OnUpdate(float deltaTime)
	{
        // Store the delta
        this->deltaTime = deltaTime;
        //ComputePositionOffsets(deltaTime);
	}

	void Wedge::OnRender()
	{
        //AdjustVertexData(); Using a uniform to send the offset rather than all thew vertices

        shader.Bind();

        va.Bind();
        shader.SetUniform3f("offset", 0.0f, 0.0f, 0.0f );
        GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, 0));
        va.UnBind();
        va2.Bind();
        shader.SetUniform3f("offset", 0.0f, 0.0f, -1.0f );
        GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, 0));
        va2.UnBind();

        //GLCall(glDrawArrays(GL_TRIANGLES, 0, 36));
	}

	void Wedge::OnImGuiRender()
	{
		//ImGui::ColorEdit4("Clear Colour", m_clearColour);
	}

    void Wedge::ComputePositionOffsets(float fElapsedTime)
    {
        const float fLoopDuration = 5.0f;
        const float fScale = 3.14159f * 2.0f / fLoopDuration;

        // glutGet is replaced with deltatime calc with std chrono class
        //float fElapsedTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;

        float fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);

        //fXOffset = cosf(fCurrTimeThroughLoop * fScale) * 0.75f;
        //fYOffset = sinf(fCurrTimeThroughLoop * fScale) * 0.75f;
        //fprintf(stderr, "ComputePositionOffsets %f %f %f\n", fXOffset, fYOffset, fCurrTimeThroughLoop );
    }

    void Wedge::AdjustVertexData()
    {
        std::vector<float> fNewData(ARRAY_COUNT(vertexPositions));
        memcpy(&fNewData[0], vertexPositions, sizeof(vertexPositions));

        for(int iVertex = 0; iVertex < ARRAY_COUNT(vertexPositions); iVertex += 4)
        {
            //fNewData[iVertex] += fXOffset;
            //fNewData[iVertex + 1] += fYOffset;
        }

        vb.AdjustVertexData(&fNewData[0], sizeof(vertexPositions));
    }
}