//
// Created by rodney on 20/05/2020.
//

#ifndef OPENGL_TUT_ROTATION_SAMPLEWEDGE_H
#define OPENGL_TUT_ROTATION_SAMPLEWEDGE_H


#include "WedgeInstance.h"

class SampleWedge: public WedgeInstance {
private:
    glm::vec3 offset;
    short scale_type;
public:
    SampleWedge(glm::vec3 offset, short scale_Type );
    glm::vec3 CalcScale(float fElapsedTime) override;
    glm::mat3 CalcRotation(float fElapsedTime) override;
    glm::vec3 getOffset() override;
private:
    float CalcLerpFactor(float fElapsedTime, float fLoopDuration);
    float ComputeAngleRad(float fElapsedTime, float fLoopDuration);
};


#endif //OPENGL_TUT_ROTATION_SAMPLEWEDGE_H
