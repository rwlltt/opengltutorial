
#include "Renderer.h"
#include <iostream>
#include <glm/gtc/type_ptr.hpp>


void  GLClearError() {
	while (glGetError() != GL_NO_ERROR);
}

bool GLLogCall(const char* function, const char* file, int line) {
	while (GLenum error = glGetError()) {
		std::cout << "[OpenGl Error](" << std::hex << error << std::dec << "): " <<
			function << " " << file << ":" << line << '\n';
		return false;
	}
	return true;
}

Renderer::Renderer(): fFrustumScale{CalcFrustumScale(45.0f)}, cameraToClipMatrix{0.0f} {
    float fzNear = 1.0f; float fzFar = 61.0f;
    cameraToClipMatrix[0].x = fFrustumScale;
    cameraToClipMatrix[1].y = fFrustumScale;
    cameraToClipMatrix[2].z = (fzFar + fzNear) / (fzNear - fzFar);
    cameraToClipMatrix[2].w = -1.0f;
    cameraToClipMatrix[3].z = (2 * fzFar * fzNear) / (fzNear - fzFar);
}

Renderer::~Renderer() {

}

void Renderer::ViewPort(int width, int height)
{
    float ratio = width / (float)height;
    int widthR, heightR;
    // Non stretch view of graphics render
    if (ratio > 1) {
        widthR = height;
        heightR = height;
    } else {
        widthR = width;
        heightR = width;
    }
    // Centre in the view screen width and height of window
    int centrePointX = width/2;
    int centrePointY = height/2;
    int vx,vy;
    vx = centrePointX - widthR/2;
    vy = centrePointY - heightR/2;
	//GLCall(glViewport(vx, vy, (GLsizei)widthR, (GLsizei)heightR));

	// Change to the view port with perspective aspect in shader perperpective
    //std::cout << "fFrustumScale " << fFrustumScale << "\n";
    cameraToClipMatrix[0].x = fFrustumScale * ((heightR-vy) / (float)(widthR-vx));
    cameraToClipMatrix[1].y = fFrustumScale;
    GLCall(glViewport(vx, vy, (GLsizei)widthR, (GLsizei)heightR));

//    theMatrix[0] = fFrustumScale / (width / (float)height);
//    theMatrix[5] = fFrustumScale;
//    GLCall(glViewport(0, 0, (GLsizei)width, (GLsizei)height));
}

void Renderer::ShaderPerspective(Shader &shader) {
    shader.Bind();
    shader.SetUniformMat4f("cameraToClipMatrix", cameraToClipMatrix);
    shader.UnBind();
}

void Renderer::Clear() const
{
    GLCall(glClearColor(0.0f, 0.0f, 0.0f, 0.0f));
    GLCall(glClearDepth(1.0f));
	GLCall(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
}

void Renderer::Draw(const VertexArray & va, const IndexBuffer & ib, const Shader & shader) const
{
	shader.Bind();
	va.Bind();
	ib.Bind();
	GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));

}

float Renderer::CalcFrustumScale(float fFovDeg)
{
    const float degToRad = 3.14159f * 2.0f / 360.0f;
    float fFovRad = fFovDeg * degToRad;
    return 1.0f / tan(fFovRad / 2.0f);
}

