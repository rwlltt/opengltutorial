#include "Request.h"
#include "Application.h"

using namespace io_utility;

/** Enironment */
Environment::Environment() : folder_(".")
{
}


Environment::~Environment()
{
}

/** User */
User::User(const std::string &name): name_(name)
{
}

User::~User()
{
}

void User::OnUpdate(float deltaTime)
{
	if (shall_prompt_)
		res_->OnUpdate(0.0f);
}

void User::OnRender()
{
	if (shall_prompt_)
		res_->OnRender();
}

void User::OnImGuiRender()
{
	Lock lock(this);
	//this->AtomicAssignn()
	if (shall_prompt_)
		res_->OnImGuiRender();
	1;
}




/** Request base */
Request::Request(const std::string &user_name, const Environment &env): user_name_(user_name), env_(env)
{
}


Request::~Request()
{
}
