﻿/*
 * PersonalModel.cpp
 *
 *  Created on: 20/07/2017
 *      Author: rodney
 */
#ifdef NDEBUG 
#define D(x)
#else 
#define D(x) x
#endif

#include "Model.h"
#include "ModuleException.h"
#include "storage/DBStorage.h"
#include "storage/driver/DBDriver.h"
#include "storage/driver/DBResult.h"
#include "storage/ParseDatabaseConnection.h"
#include "io_utility.h"
#include <sstream>

namespace Module {

	Model::Model() : m_dbStorage(new Storage::DBStorage(m_dbVersion, m_dbConfigXml.c_str())){}
	
	Model::~Model() {
		if (m_dbStorage != 0) {
			D(std::cerr << "About to release dbStorage\n";)
			Storage::DBStorage* p = m_dbStorage.release();
			delete p;
			D(std::cerr << "Released dbStorage\n";)
		}

	}

//
///////////////////////////////////////////////////////////////////////////////
// Public method
///////////////////////////////////////////////////////////////////////////////
//
// std::tuple<bool, std::string> Model::loginUser(const std::string username, const std::string password,
		// const Module::UserRole &roletype) {
	// if (m_hasLoggeduser) {
		// throw(ModuleException("PersonalModule loginUser Has user logged in, use logout to login new user"));
	// }
	// bool logged { false };
	// std::stringstream message;
	// std::string role;
	// switch (roletype) {
	// case Module::UserRole::ADMIN:
		// role = "RBD$ADMIN";
		// break;
	// default:
		// role = "GUEST";
		// break;
	// }
	// auto result = m_dbStorage.loginUser(username, password, role);
	// if (m_dbStorage.isLoggedUser()) {
		// m_UserName = username;
		// m_UserPassword = password;
		// m_UserRole = role;
		// m_hasLoggeduser = true;
		// logged = true;
	// } else {
		// Storage::DBCallError err = std::get<1>(result);
		// message << " " << err.error;
	// }
	// return std::tuple<bool, std::string>(logged, message.str());

// }

	// if (!loggedUser()) {
		// throw(ModuleException("PersonalModule addNewPerson no logged in user."));
	// }

std::unique_ptr<Storage::Driver::DBDriver> Model::create_dr() {
	using namespace Storage;
    auto connectType = ConnectionType::LIVE; // default to live db connect
	if (io_utility::to_uppercase(m_UserName.c_str()).compare("TEST") == 0) {
		connectType = ConnectionType::TEST;
	}
	ParseDatabaseConnection parseDBConnection(m_dbConfigXml.c_str());
	parseDBConnection.ReadConnection(connectType);
	if (parseDBConnection.getXmlErrors().length() > 0) {
		throw(ModuleException("Model create_tables - error in module configuration xml."));
	}
	auto dbconnect = parseDBConnection.getDatabaseConnection(); // will be set if no error
	return Storage::Driver::DBDriver::CreateDBDriver(dbconnect);
}
	
std::string Model::db_version(Storage::Driver::DBDriver* dr) {
	using namespace std;
	using namespace Storage;
	unique_ptr<Driver::DBStatement> pSt;
    unique_ptr<Driver::DBResult> pRt;
	std::string version{""};
	// Try a select of version
	try {
		pSt = dr->CreateStatement("SELECT a.ID, a.DESCRIPTION, a.TOTALTABLES FROM VERSION a");
		bool done;
		pRt = pSt->Execute();
		Storage::RowFields fields;
		unsigned short ish = 1;
		fields.insert(std::make_pair(ish, Storage::FieldDetail{"ID", 0}));
		fields.insert(std::make_pair(++ish, Storage::FieldDetail{"DESCRIPTION", ""}));
		fields.insert(std::make_pair(++ish, Storage::FieldDetail{"TOTALTABLES", 0}));
		Storage::RowItem row{fields};
 
		while (true) {
			done = pRt->Fetch(row);
			if (!done)
				break;
			version = row.ItemDetail();
		}
	} catch (exception& e) {
		version = e.what();
	}
	dr->Commit();
	return version;

}	
	
void Model::create_tables() {
	Lock lock(*this);
	using namespace std;
	using namespace Storage;
	
	string sql_drop{"DROP TABLE if exists Movies; "};
	string sql_drop_version{"DROP TABLE if exists Version; "};
	string sql_create{
	   "CREATE TABLE if not exists Movies("
	   " M_ID INT PRIMARY KEY      NOT NULL,"
	   " Movie           VARCHAR(50) NOT NULL"
	   " );"};
	string sql_create_version{
	   "CREATE TABLE if not exists Version("
	   " ID INT PRIMARY KEY    NOT NULL,"
	   " Description           VARCHAR(50) NOT NULL,"
	   " TotalTables INT       NOT NULL"
	   " );"};
	
	auto dr = create_dr();
    try {
		dr->Connect(m_UserName, m_UserPassword, m_Role);
		if (true == dr->isConnected()) {
			unique_ptr<Driver::DBStatement> pSt;
			unique_ptr<Driver::DBResult> pRt;

			dr->drop_table(dr.get(), "Movies", sql_drop);
			dr->drop_table(dr.get(), "Version", sql_drop_version);

			pSt = dr->CreateStatement(sql_create);
			pRt = pSt->Execute();
			pSt = dr->CreateStatement(sql_create_version);
			pRt = pSt->Execute();
			pSt = dr->CreateStatement("INSERT INTO Version(ID, Description, TotalTables) VALUES (1, '" + std::string(dbVersion) + "', 1) ");
			pSt->Execute();
			dr->Commit();

			auto movies = io_utility::read_file("res/movies.txt", true);
			int m_id = 0;
			pSt = dr->CreateStatement("INSERT INTO Movies(M_ID, Movie) VALUES (" + std::to_string(m_id++) + ", '[ðŸ˜‡] [\xF0\x9F\x98\x87] [\xF0\x9F\x98\x86] [\xC4\x80\xC4\x81\xC3\x86\xC3\xA6\xC3\x98\xC3\xB8]')");
			pSt->Execute();
			for (string m: movies) {
				// escape string for quotes
				auto new_str = io_utility::sql_escape_single_quotes(m);
				pSt = dr->CreateStatement("INSERT INTO Movies(M_ID, Movie) VALUES (" + std::to_string(m_id++) + ", '" + new_str + "') ");
				pSt->Execute();
			}
			dr->Commit();
			
		}
	} catch (exception& e) {
		stringstream message;
		message << "Model create_tables - " << e.what();
		throw(ModuleException(message.str()));
	} 
	if (dr.get()) {
		D(std::cerr << "About to release create dr\n";)
		Storage::Driver::DBDriver* p = dr.release();
		delete p;
		D(std::cerr << "Released create dr\n";)
	}

}


Storage::ResultRetrieval Model::movie_list() {
	try {
		auto log_result = m_dbStorage->loginUser(m_UserName, m_UserPassword);
		if (std::get<0>(log_result) == false) {
			std::cout << "Log result " << std::get<1>(log_result).error << '\n';
		}
		Storage::DBRetrieveMap where;
		Storage::DBRetrieveMap params;
		Storage::ResultRetrieval result = m_dbStorage->retrieve("Movies", where, params);
		return result; 
	}
	catch (std::exception &e) {
		std::cout << "Model::movie_list " << e.what() << std::endl;
	}
	//D(std::cout << "Number found " << dest.size() << '\n';)
	throw(ModuleException("No login"));
}

} /* namespace Module */
