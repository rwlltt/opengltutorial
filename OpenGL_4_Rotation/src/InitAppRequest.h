#ifndef TUT_INITAPPREQUEST_
#define TUT_INITAPPREQUEST_

#include "Request.h"

class InitAppRequest: public Request
{
public:
	InitAppRequest(const std::string &user, const Environment &env);
	virtual ~InitAppRequest();

	//	InitAppRequest(const InitAppRequest& other) = delete;
	InitAppRequest& operator= (const InitAppRequest &other) = delete;
	InitAppRequest* operator&(InitAppRequest object) = delete;

	virtual std::unique_ptr<Response> DoRequest(const std::string &user);

};

#endif /* TUT_INITAPPREQUEST_*/