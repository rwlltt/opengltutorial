
#include "VertexBuffer.h"
#include "Renderer.h"

VertexBuffer::VertexBuffer(const void * data, unsigned int size, bool dynamic)
{
    unsigned int type;
	GLCall(glGenBuffers(1, &m_RendererID));
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_RendererID));
	if (dynamic) {
	    type = GL_STREAM_DRAW;
	} else {
        type = GL_STATIC_DRAW;
    }
    GLCall(glBufferData(GL_ARRAY_BUFFER, size, data, type));

}

VertexBuffer::~VertexBuffer()
{
	GLCall(glDeleteBuffers(1, &m_RendererID));
}

void VertexBuffer::Bind() const
{
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_RendererID));
}

void VertexBuffer::UnBind() const
{
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0));
}

void VertexBuffer::AdjustVertexData(const void * data, unsigned int size) {
    GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_RendererID));
    GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, size, data));
    GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0));

}