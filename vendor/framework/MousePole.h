/** Copyright (C) 2010-2012 by Jason L. McKesson **/
/** This file is licensed under the MIT License. **/


#ifndef FRAMEWORK_MOUSE_POLE_H
#define FRAMEWORK_MOUSE_POLE_H

#include <glm/glm.hpp>
//#include "GLAD/glad.h"
#include <GLFW/glfw3.h>
//#include <GL/freeglut.h>
#include <glutil/MousePoles.h>

namespace Framework
{
	inline int calc_glfw_modifiers(int modifiers)
	{
		int ret = 0;
//		int modifiers = glutGetModifiers();
		if(modifiers & GLFW_MOD_SHIFT)
			ret |= glutil::MM_KEY_SHIFT;
		if(modifiers & GLFW_MOD_CONTROL)
			ret |= glutil::MM_KEY_CTRL;
		if(modifiers & GLFW_MOD_ALT)
			ret |= glutil::MM_KEY_ALT;

		return ret;
	}

	template<typename Pole>
	inline void ForwardMouseMotion(Pole &forward, int x, int y)
	{
		forward.MouseMove(glm::ivec2(x, y));
	}

	template<typename Pole>
	inline void ForwardMouseButton(Pole &forward, int button, int state, int x, int y, int mods)
	{
		//int modifiers = calc_glut_modifiers();
        int modifiers = calc_glfw_modifiers(mods);

		glm::ivec2 mouseLoc = glm::ivec2(x, y);

		glutil::MouseButtons eButton;

		switch(button)
		{
		    case GLFW_MOUSE_BUTTON_1: //GLUT_LEFT_BUTTON:
			eButton = glutil::MB_LEFT_BTN;
			break;
		    case GLFW_MOUSE_BUTTON_3://GLUT_MIDDLE_BUTTON:
			eButton = glutil::MB_MIDDLE_BTN;
			break;
		    case GLFW_MOUSE_BUTTON_2://GLUT_RIGHT_BUTTON:
			eButton = glutil::MB_RIGHT_BTN;
			break;
#ifdef LOAD_X11
			//Linux Mouse wheel support
		case 3:
			{
				forward.MouseWheel(1, modifiers, mouseLoc);
				return;
			}
		case 4:
			{
				forward.MouseWheel(-1, modifiers, mouseLoc);
				return;
			}
#endif
		default:
			return;
		}

		forward.MouseClick(eButton, state == /*GLUT_DOWN*/GLFW_PRESS, modifiers, glm::ivec2(x, y));
	}

	template<typename Pole>
//	inline void ForwardMouseWheel(Pole &forward, int wheel, int direction, int x, int y)
    inline void ForwardMouseWheel(Pole &forward, int direction)
	{
		forward.MouseWheel(direction, glutil::MM_KEY_SHIFT, glm::ivec2(0, 0));
	}

}

#endif //FRAMEWORK_MOUSE_POLE_H
