/*
 * DatabaseStatement.h
 */

#ifndef D_DATABASESTATEMENT_H_
#define D_DATABASESTATEMENT_H_

#include "RowItem.h"
#include <unordered_map>
#include <string>
#include <vector>

/// Validate namespace
/**
 * Namespace for validator for presenter
 */
namespace Storage {

    /// Rule type

    /**
     * Specify a StatementType in method to get SQL for it.
     */
    enum class StatementType : char {
        SELECT = 's', ///< Select
        INSERT = 'i', ///< Insert
        UPDATE = 'u', ///< Update sql
        ST_DELETE = 'd'  ///< Delete
    };

    /**
     * Specify a DBOType in method to get SQL for it.
     */
    enum class DBOType : char {
        TABLE = 't', ///< Table
        PROCEDURE = 'p', ///< Procedure
    };

    /// Hash for class enum

    struct EnumClassHash {
        /// operator() for hash in map

        template <typename T>
        std::size_t operator()(T t) const {
            auto val = static_cast<std::size_t> (t);
            return val;
        }
    };

    ///Procedure parameters by index so to parameter name and type
    typedef std::unordered_map<unsigned short, FieldDetail> ProcParams;
    ///Procedure parameters value type for iterating
    typedef std::unordered_map<unsigned short, FieldDetail>::value_type ProcParams_value_type;

    /// Map of field details to field name by user
    typedef std::vector<FieldDetail> DBRetrieveMap;

    /// DatabaseStatement

    /**
     * Database statement for sql statements in a database query.
     * The fields are mapped with an index number. 
     * The numbered fields are used to make a sql statement.
     * The field name is used in the sql statement and retrived rows are indexed by a number.
     * It is requirement for the index to start at 1 and increment by one for each field.
     * Therefore only the ParseDatabaseStatement functions can create the indexed FieldDetail
     * to ensure the 1 to N order.
     *
     * \sa getField.
     *
     */
    class DatabaseStatement {
        std::string m_tableName;
        std::string m_tableID;
        std::string m_entityName;
        RowFields m_fields;
        DBOType m_dboType{DBOType::TABLE};
        ProcParams m_params;

    public:
        /// Constructor
        /**
         * \param tableName const reference of string
         * \param tableID const reference of string
         * \param entityName const reference of string (optional)
         * \param dboType type of dbo (default is TABLE)
         */
        DatabaseStatement(const std::string &tableName, const std::string &tableID,
        		          const std::string &entityName = "", DBOType dboType = DBOType::TABLE);

        /// Virtual deconstructor
        virtual ~DatabaseStatement();

        /// DBO Type
         /**
         * \return std::string
         */
        DBOType getDBOType() {
            return m_dboType;
        };
        /// Table Name
         /**
         * \return std::string
         */
        std::string getTableName() {
            return m_tableName;
        };
        /// Table ID

        /**
         * \return std::string
         */
        std::string getTableID() {
            return m_tableID;
        };
        /// Object entity Name
         /**
         * \return std::string
         */
        std::string getEntityName() {
            return m_entityName;
        };

        /// Create a row with this statements MapFields in an initial state
        /**
         * The RowItem is in an initialized state with no values set.
         * Just a RowItem with number of 1 to N fields and their data type ready
         * to be set a value.
         * Not all fields need to be set, and leaving them unset keeps them seen
         * as empty with no value set.
         * \return RowItem value type
         */
        RowItem createRowItem() {
            RowItem row(m_fields);
            return row;
        };
        /// SQL Value
        /**
         * \param stype enum of statement type
         * \param item row item for field values for sql
         * \return string of the SQL for the statement
         */
        std::string getSQL(StatementType stype, const RowItem& item);
        /// SQL Value
        /**
         * \param stype enum of statement type
         * \param item row item for field values for sql
         * \return string of the SQL for the statement
         */
        std::string getSQLCount(StatementType stype, const RowItem& item);
        /// SQL Where Value
        /**
         * \param stype enum of statement type
         * \param item row item for field values for where sql
         * \return string of the Where SQL for the statement
         */
        std::string getWhereSQL(StatementType stype, const RowItem& item);

        /// get statement full detail list
        /**
         * \return string
         * */
        std::string StatementDetail();

        /// set Parameter values for DBO Procedure
        /**
         * \param params DBRetrieveMap
         * \return string
         * */
        void setParameterValues(const Storage::DBRetrieveMap &params);

    private:
        /// get statement Fields Detail list
        /**
         * \param stype enum of statement type
         * \param item row item for field values for where sql
         * \return string
         * */
        std::string statementFieldDetail(StatementType stype, const RowItem& item);
        /// get statement Fields Detail list
        /**
         * \param item row item for field values for where sql
         * \return string
         * */
        std::string statementFieldValueDetail(const RowItem& item);
        /// get statement Fields Returning Detail list
        /**
         * \param stype enum of statement type
         * \param item row item for field values for where sql
         * \return string
         * */
        std::string statementReturningFieldDetail(StatementType stype, const RowItem& item);
        /// get statement Where Fields Detail list
        /**
         * \param item item for field value details
         * \return string
         * */
        std::string statementWhereFieldDetail(const RowItem& item);
        /// get parameter values for select procedure statement
        /**
         * \return string
         * */
        std::string parameterValues();
        /// add FieldName and index
        /**
         * New fields added will be initialized with initialized state.
         * No values are set and flagged is_set is false.
         * The field is added to MapFields.
         * This is used when a new RowItem is made with function createRowItem()
         * \param index order found and its index value for field name
         * \param field const reference to string
         * \param field_type const reference to string
         * */
        void setField(unsigned short index, const std::string &field,
                Storage::FieldType field_type);

        /// add ParameterName and index
        /**
         * New parameters added will be initialized with initialized state.
         * No values are set and flagged is_set is false.
         * The parameter is added to MapParameters.
         * This is used only for Procedure database objects.
         * \param index order found and its index value for parameter
         * \param name const reference to string
         * \param field_type const reference to string
         * */
        void setParameter(unsigned short index, const std::string &name,
                Storage::FieldType field_type);

        friend class ParseDatabaseStatement; // able to setField

    public:
        /// Equality operator
        /**
         * \param lhs left hand side
         * \param rhs right hand side
         * \return true if equal
         */
        friend bool operator==(const DatabaseStatement &lhs, const DatabaseStatement &rhs);
        /// Inequality operator
        /**
         * \param lhs left hand side
         * \param rhs right hand side
         * \return true if not equal
         */
        friend bool operator!=(const DatabaseStatement &lhs, const DatabaseStatement &rhs);

    };

    /// Equality operator

    /**
     * \param lhs left hand side
     * \param rhs right hand side
     * \return true if equal
     */
    inline bool operator==(const DatabaseStatement &lhs, const DatabaseStatement &rhs) {
        bool result = false;
        if (lhs.m_fields.size() == rhs.m_fields.size()) {
            for (unsigned short index = 1; index <= lhs.m_fields.size(); ++index) {
                FieldDetail lhs_fd = lhs.m_fields.at(index);
                FieldDetail rhs_fd = rhs.m_fields.at(index);
                if (!(lhs_fd == rhs_fd)) {
                    return false;
                };
            }
            result = true;
        } else {
            result = false;
        }

        return (lhs.m_tableID == rhs.m_tableID &&
                lhs.m_tableName == rhs.m_tableName &&
                lhs.m_entityName == rhs.m_entityName &&
                result);
    }
    /// Equality operator

    /**
     * \param lhs left hand side
     * \param rhs right hand side
     * \return true if not equal
     */
    inline bool operator!=(const DatabaseStatement &lhs, const DatabaseStatement &rhs) {
        return !(lhs == rhs);
    }

    /// MapStatement map of datatypes StatementType and statements
    typedef std::unordered_map<StatementType, DatabaseStatement, EnumClassHash> MapStatement;
    /// MapStatement map of datatypes StatementType and statements value type
    typedef std::unordered_map<StatementType, DatabaseStatement, EnumClassHash>::value_type MapStatement_value_type;


} /* namespace Storage */

#endif /* D_DATABASESTATEMENT_H_ */
