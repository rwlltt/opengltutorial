/*
 * ParseDatabaseConnection.h
 */

#ifndef D_PARSEDATABASECONNECTION_H_
#define D_PARSEDATABASECONNECTION_H_

#include <string>
#include <sstream>
#include <unordered_map>
#include <vector>
#include "DatabaseConnection.h"

namespace Storage
{

/// Parse Database Connection`
/**
 */
class ParseDatabaseConnection
{
    DatabaseConnection m_database_connection{"", "", "", ""};
    const char *m_xml{};
    std::stringstream m_xmlerror{}; ///< what was xml parser problem
    Storage::ConnectionType db_connect_type{Storage::ConnectionType::LIVE}; ///< statement data type (set in constructor and required)
// <file role=\"live\" driver=\"Sqlite\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
    std::string m_a_role{"role"}, m_a_driver{"driver"}, m_a_name{"name"};
    std::string m_a_server{"server"}, m_a_charset{"charset"};

public:
    /// Constructor
    /**
     * \param xml const char xml
     */
    ParseDatabaseConnection ( const char *xml );
    /// Virtual deconstructor
    ~ParseDatabaseConnection();

    /// Public Read Statements method
    /**
     * \sa ParseDatabaseConnection()
     * \param connectType what type of connection to database: production or test
     */
    void ReadConnection (const Storage::ConnectionType &connectType);
    /// Get xml read errors if any
    /**
     * Empty if no error
     * \return string of xml errors
     */
    std::string getXmlErrors ();
    /// Get read DatabaseConnection
    /**
     * \return DatabaseConnection
     */
    DatabaseConnection getDatabaseConnection();
    
private:

   /// readAtribute
    bool readAttribute ( const char *name, const char **atts, std::string &value );
     // trim white spaces
    inline static std::string trimWhitespaces ( std::string value );
    // to Uppercase a name
    inline static std::string toUppercase ( const char *name );
  	bool find_attributes(map_attrs name_value);

};

}

#endif /* D_PARSEDATABASECONNECTION_H_*/
