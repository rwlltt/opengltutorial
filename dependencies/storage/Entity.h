/*
 * Hero.h
 */

#ifndef ENTITY_H_
#define ENTITY_H_

#include "EntityMap.h"
#include "DBStorage.h"
#include "DataDefine.h"
#include "ParseDatabaseStatement.h"
#include "Hero.h"
#include <string>


namespace Storage {

	//extern const char *dbVersion;
	//extern const char *dbConfigXml; ///< Module db config xml
	//extern const int dbNumberOfTables;
	//extern const char* dbTableNames[];


	/// Item
	class Entity
	{

		std::string m_table_name{ "" };
		DataItem m_item{};
		DataOne2OneNodes m_one2one;
		DataOne2ManyNodes m_one2many;

	public:
        static EntityTraits<Storage::Entity> m_entityTraits;
        
        /// Constructor
		Entity(const std::string &table_name);
		/// Deconstructor
		virtual ~Entity() {};

		//Rule of five, but a user-defined dtor is not necessary due to unique_ptr
		auto clone() const { return std::unique_ptr<Entity>(clone_impl()); }

		Entity(Entity const& other) {
			for (auto &key : other.m_one2one) {
				m_one2one[key.first] = key.second->clone();
			}
			for (auto &key : other.m_one2many) {
				for (auto &item : key.second) {
					m_one2many[key.first].push_back(item->clone());
				}
			}
			//m_table_name = other.m_table_name;
			m_item = other.m_item;
		}
		Entity(Entity && other) = default;
		Entity& operator=(Entity const& other) {
			for (auto &key : other.m_one2one) {
				m_one2one[key.first] = key.second->clone();
			}
			for (auto &key : other.m_one2many) {
				for (auto &item : key.second) {
					m_one2many[key.first].push_back(item->clone());
				}
			}
			//m_table_name = other.m_table_name;
			m_item = other.m_item;
			return *this;
		}
		Entity& operator=(Entity && other) = default;


		void setField(const std::string &field_name, const std::string &value);
		std::string getField(const std::string &field_name) const;

        void bindItem2(const Storage::RowItem &item, EntityMapBase* entityMap) {
            unsigned short total = item.TotalFields();
            for (unsigned short index = 1; index <= total; ++index) {
                FieldDetail fd = item.getFieldDetail(index);
                int intValue;
                std::string strValue;
                bool isSet;
                switch (fd.field_type) {
                case Storage::FieldType::INTEGER:
                case Storage::FieldType::AUTOGENINT:
                    item.getField(index, intValue, isSet);
                    if (isSet == true) {
                        entityMap->setField(fd.field_name, intValue);
                    }
                    break;
                case Storage::FieldType::STRING:
                    item.getField(index, strValue, isSet);
                    if (isSet == true) {
                        entityMap->setField(fd.field_name, strValue);
                    }
                    break;
                }
            }
        }


		const DataItem &getDataItem() const;
        DataItem &getXDataItem();

		void setOne2One(const std::string &field_name, std::unique_ptr<Entity> value);
		void setOne2Many(const std::string &field_name, std::unique_ptr<Entity> value);

	protected:
		virtual Entity* clone_impl() const {
			return new Entity(*this);
		};
	};



	/// EntityMap Item
	/**
	 EntityTriats<Module::Item> is a wrapper object to know how to store data from the dbstorage.lib
	 Using EntityTraits it can store a table row by tablename, and the fields in the table.
	 Each instance of Module::Item holds one row from any table.

	*/
	//STORENTMAP_DECLARE_BEGIN(entname, cxxtype)                                          \

	typedef std::unordered_map<std::string, std::string> EntityTableMap;
	typedef std::unordered_map<std::string, std::vector<std::pair<std::string, FieldType>>> EntityTableFieldsMap;
	typedef std::unordered_map<std::string, std::string> EntityRelationMap;
	typedef std::vector<std::pair<std::string, RelationType>> EntityRelationDetailList;

	template<>
	struct EntityTraits<Storage::Entity> {
		static const char *entitytype;
        static const char *entityname;
        static EntityTableMap            map_tables;
		static EntityTableFieldsMap      map_table_fields;
		static EntityRelationMap         map_relations;
		static EntityRelationDetailList  list_relation_details;
		typedef Storage::Entity          component_type;
		typedef EntityTableMap           component_map_type;
		typedef EntityTableFieldsMap     component_list_detail_type;
		typedef EntityRelationMap        component_relmap_type;
		typedef EntityRelationDetailList component_rellist_detail_type;

		static component_map_type init_table_map() {
			component_map_type table_map;
			table_map.insert(std::make_pair("ROO", "ROO"));
			return table_map;
		}
		/// Relationships to other entity 
		static component_relmap_type init_relation_map() {
			component_relmap_type method_map;
    		method_map.insert(std::make_pair("ROO", "ROO"));
			return method_map;
		}

		static component_list_detail_type init_table_fields_map() {
            component_list_detail_type method_list;
            auto list_field = method_list[entityname];
            list_field.push_back(std::make_pair("NAMEFLD", FieldType::INTEGER));
            method_list[entityname] = list_field;
            return method_list;
        }

		static void getFieldValue(const component_type &item, FieldDetail &fd) {
			int number;
			std::string str_value(item.getField(fd.field_name));
			try {
				switch (fd.field_type) {
				case Storage::FieldType::AUTOGENINT:
				case Storage::FieldType::INTEGER:
					number = std::stoi(str_value);
					// chk = std::to_string(number);
					// if (chk.length() != str_value.length()) {}
					fd.int_value = number;
					break;
				case Storage::FieldType::STRING:
					fd.str_value = str_value;
					break;
				}
			}
			catch (const std::exception /*&e*/) {
			}
		}

		static void setFieldValue(component_type &item, FieldDetail &fd) {
			switch (fd.field_type) {
			case Storage::FieldType::AUTOGENINT:
			case Storage::FieldType::INTEGER:
				item.setField(fd.field_name, std::to_string(fd.int_value));
				break;
			case Storage::FieldType::STRING:
				item.setField(fd.field_name, fd.str_value);
				break;
			}
		}


	//	//STOREENTMAP_RELATIONS_BEGIN
		static component_rellist_detail_type init_relation_detail_list() {
			component_rellist_detail_type method_list;
			//STORERELMAP_RELATIONS_DECLARE(fld, ftype)           
			//STORERELMAP_RELATIONS_DECLARE(Chest, RelationType::ONETOONE)
			//method_list.push_back(std::make_pair("Chest", RelationType::ONETOONE));
			//STORERELMAP_RELATIONS_DECLARE(ChestItems, RelationType::ONETOMANY)
			//method_list.push_back(std::make_pair("ChestItems", RelationType::ONETOMANY));
			return method_list;
		}

	//	//STOREENTMAP_RELATIONS_DETAIL_BEGIN
		static void getRelationDetail(RelationDetail &rd) {
			//RELATION_ONE2MANY(fld, rtype, pk, rfktype, fk, fpk, ffk)  
			//RELATION_ONE2MANY(ChestItems, FieldType::INTEGER, ChestID, FieldType::INTEGER, ChestID, ChestItemID, ChestID)

   //         if (rd.field_name.compare("ChestItems") == 0) {
			//	rd.field_type_pk = FieldType::INTEGER;
			//	rd.field_pk = "ChestID";
			//	rd.field_type_fk = FieldType::INTEGER;
			//	rd.field_fk = "ChestID";
			//	rd.foreign_table = "ChestItems";
			//	rd.foreign_pk = "ChestItemID";
			//	rd.foreign_fk = "ChestID";
			//}

            //RELATION_ONE2ONE(fld, rtype, pk, rfktype, fk, fpk, ffk)  
			//RELATION_ONE2ONE(Chest, FieldType::INTEGER, PersonID, FieldType::INTEGER, ChestID, ChestID, PersonID)
			// same as above
			//RELATION_ONE2ONE(Chest, FieldType::INTEGER, PersonID, FieldType::INTEGER, ChestID, ChestID, PersonID)

   //         if (rd.field_name.compare("Chest") == 0) {
			//	rd.field_type_pk = FieldType::INTEGER;
			//	rd.field_pk = "PersonID";
			//	rd.field_type_fk = FieldType::INTEGER;
			//	rd.field_fk = "ChestID";
			//	rd.foreign_table = "Chest";
			//	rd.foreign_pk = "ChestID";
			//	rd.foreign_fk = "ChestID";
			//}
		}

		static void getRelationEntity(component_type &, RelationDetail &) {
		}

	//	//STOREENTMAP_RELATIONS_SET_BEGIN
		static void setRelationEntity(component_type &item, RelationDetail &rd) {
	//		//SET_ONE2ONE_ACCESSOR(Chest, Module::Chest, setChest)
	//		//SET_ONE2ONE_ACCESSOR(fld, etype, setfunc)                           
	//		if (rd.field_name.compare("Chest") == 0) {
	//			EntityMap<Entity> emap{ "Chest" };
	//			if (rd.relation_type == Storage::RelationType::ONETOONE) {
	//				DBRetrieveMap where;
	//				if (rd.field_type_pk == Storage::FieldType::INTEGER) {
	//					Storage::FieldDetail fd;
	//					fd.field_name = rd.field_pk;
	//					getFieldValue(item, fd);
	//					where = { {rd.foreign_fk, fd.int_value} };
	//				}
	//				auto result = rd.db->retrieve(rd.foreign_table, where, {});
	//				if (!result.empty()) {
	//					auto item_fr = result.next<Entity>();
	//					if (rd.field_type_fk == Storage::FieldType::INTEGER) {
	//						int int_value;
	//						emap.getField(item_fr, rd.foreign_pk, int_value);
	//						Storage::FieldDetail fd;
	//						fd.field_name = rd.field_fk;
	//						fd.int_value = int_value;
	//						setFieldValue(item, fd);
	//					}
	//					item.setOne2One("Chest", item_fr.clone());
	//					//std::unique_ptr<DBResult> result = std::make_unique<DBSqliteResult>(rc, stmt);
	//				}
	//			}
			}

	//		//SET_ONE2MANY_ACCESSOR(ChestItems, Module::ChestItems, addChestItem)
	//		//SET_ONE2MANY_ACCESSOR(fld, etype, setfunc)                          
	//		if (rd.field_name.compare("ChestItems") == 0) {
	//			EntityMap<Entity> emap{ "ChestItems" };
	//			if (rd.relation_type == Storage::RelationType::ONETOMANY) {
	//				DBRetrieveMap where;
	//				if (rd.field_type_pk == Storage::FieldType::INTEGER) {
	//					Storage::FieldDetail fd;
	//					fd.field_name = rd.field_pk;
	//					getFieldValue(item, fd);
	//					where = { {rd.foreign_fk, fd.int_value} };
	//				}
	//				auto result = rd.db->retrieve("C", where, {});
	//				while (!result.empty()) {
	//					auto item_rd = result.next<Entity>();
	//					//item.addChestItem(item_rd);
	//					std::cout << "Relation: " << item_rd.getField("Movie");
	//				}
	//			}
	//		}
	//	}

	};


/// EntityMap ChestItems
STORENTMAP_DECLARE_BEGIN(ChestItems, Module::ChestItems)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(ChestItemID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(InventoryID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(ChestID, FieldType::INTEGER)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STORERELMAP_RELATIONS_DECLARE(Inventory, RelationType::ONETOONE)
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(ChestItemID, getChestItemID)
GET_INT_ACCESSOR(InventoryID, getInventoryID)
GET_INT_ACCESSOR(ChestID, getChestID)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(ChestItemID, setChestItemID)
SET_INT_ACCESSOR(InventoryID, setInventoryID)
SET_INT_ACCESSOR(ChestID, setChestID)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_BEGIN
RELATION_ONE2ONE(Inventory, FieldType::INTEGER, InventoryID, FieldType::INTEGER, InventoryID, InventoryID, InventoryID)
STOREENTMAP_RELATIONS_DETAIL_END
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_BEGIN
SET_ONE2ONE_ACCESSOR(Inventory, Module::Inventory, setInventory)
STOREENTMAP_RELATIONS_SET_END
STORENTMAP_DECLARE_END

/// EntityMap ChestItems2
STORENTMAP_DECLARE_BEGIN(ChestItems2, Module::ChestItems2)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(ChestItemID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(InventoryID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(ChestID, FieldType::INTEGER)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STORERELMAP_RELATIONS_DECLARE(Inventory2, RelationType::ONETOONE)
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(ChestItemID, getChestItemID)
GET_INT_ACCESSOR(InventoryID, getInventoryID)
GET_INT_ACCESSOR(ChestID, getChestID)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(ChestItemID, setChestItemID)
SET_INT_ACCESSOR(InventoryID, setInventoryID)
SET_INT_ACCESSOR(ChestID, setChestID)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_BEGIN
RELATION_ONE2ONE(Inventory2, FieldType::INTEGER, InventoryID, FieldType::INTEGER, InventoryID, InventoryID, InventoryID)
STOREENTMAP_RELATIONS_DETAIL_END
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_BEGIN
SET_ONE2ONE_ACCESSOR(Inventory2, Module::Inventory2, setInventory)
STOREENTMAP_RELATIONS_SET_END
STORENTMAP_DECLARE_END

/// EntityMap Inventory
STORENTMAP_DECLARE_BEGIN(Inventory, Module::Inventory)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(InventoryID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Quota, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Name, FieldType::STRING)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(InventoryID, getInventoryID)
GET_INT_ACCESSOR(Quota, getQuota)
GET_STRING_ACCESSOR(Name, getName)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(InventoryID, setInventoryID)
SET_INT_ACCESSOR(Quota, setQuota)
SET_STRING_ACCESSOR(Name, setName)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_EMPTY
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_EMPTY
STORENTMAP_DECLARE_END

/// EntityMap InventoryCount
STORENTMAP_DECLARE_BEGIN(InventoryCount, Module::InventoryCount)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(InventoryID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Quota, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Name, FieldType::STRING)
STOREENTMAP_FIELDS_DECLARE(CountItem, FieldType::INTEGER)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(InventoryID, getInventoryID)
GET_INT_ACCESSOR(Quota, getQuota)
GET_STRING_ACCESSOR(Name, getName)
GET_INT_ACCESSOR(CountItem, getCountItem)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(InventoryID, setInventoryID)
SET_INT_ACCESSOR(Quota, setQuota)
SET_STRING_ACCESSOR(Name, setName)
SET_INT_ACCESSOR(CountItem, setCountItem)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_EMPTY
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_EMPTY
STORENTMAP_DECLARE_END

/// EntityMap Inventory2
STORENTMAP_DECLARE_BEGIN(Inventory2, Module::Inventory2)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(InventoryID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Quota, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Name, FieldType::STRING)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(InventoryID, getInventoryID)
GET_INT_ACCESSOR(Quota, getQuota)
GET_STRING_ACCESSOR(Name, getName)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(InventoryID, setInventoryID)
SET_INT_ACCESSOR(Quota, setQuota)
SET_STRING_ACCESSOR(Name, setName)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_EMPTY
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_EMPTY
STORENTMAP_DECLARE_END

/// EntityMap Hero
STORENTMAP_DECLARE_BEGIN(Hero, Module::Hero)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(HeroID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(PersonID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(StatuteID, FieldType::INTEGER)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(HeroID, getHeroID)
GET_INT_ACCESSOR(PersonID, getPersonID)
GET_INT_ACCESSOR(StatuteID, getStatuteID)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(HeroID, setHeroID)
SET_INT_ACCESSOR(PersonID, setPersonID)
SET_INT_ACCESSOR(StatuteID, setStatuteID)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_EMPTY
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_EMPTY
STORENTMAP_DECLARE_END


/// EntityMap Chest
STORENTMAP_DECLARE_BEGIN(Chest, Module::Chest)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(ChestID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(PersonID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Name, FieldType::STRING)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STORERELMAP_RELATIONS_DECLARE(ChestItems, RelationType::ONETOMANY)
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(ChestID, getChestID)
GET_INT_ACCESSOR(PersonID, getPersonID)
GET_STRING_ACCESSOR(ID, getName)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(ChestID, setChestID)
SET_INT_ACCESSOR(PersonID, setPersonID)
SET_STRING_ACCESSOR(Name, setName)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_BEGIN
RELATION_ONE2MANY(ChestItems, FieldType::INTEGER, ChestID, FieldType::INTEGER, ChestID, ChestItemID, ChestID)
STOREENTMAP_RELATIONS_DETAIL_END
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_BEGIN
SET_ONE2MANY_ACCESSOR(ChestItems, Module::ChestItems, addChestItem)
STOREENTMAP_RELATIONS_SET_END
STORENTMAP_DECLARE_END

/// EntityMap Chest2
STORENTMAP_DECLARE_BEGIN(Chest2, Module::Chest2)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(ChestID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(PersonID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Name, FieldType::STRING)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STORERELMAP_RELATIONS_DECLARE(ChestItems2, RelationType::ONETOMANY)
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(ChestID, getChestID)
GET_INT_ACCESSOR(PersonID, getPersonID)
GET_STRING_ACCESSOR(ID, getName)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(ChestID, setChestID)
SET_INT_ACCESSOR(PersonID, setPersonID)
SET_STRING_ACCESSOR(Name, setName)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_BEGIN
RELATION_ONE2MANY(ChestItems2, FieldType::INTEGER, ChestID, FieldType::INTEGER, ChestID, ChestItemID, ChestID)
STOREENTMAP_RELATIONS_DETAIL_END
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_BEGIN
//SET_ONE2MANY_ACCESSOR(ChestItems2, Module::ChestItems2, addChestItem)
if (rd.field_name.compare("ChestItems2") == 0) {
	EntityMap<Module::ChestItems2, EntityTraits<Module::ChestItems2>> emap{ "ChestItems2" };
	if (rd.relation_type == Storage::RelationType::ONETOMANY) {
		DBRetrieveMap where;
		if (rd.field_type_pk == Storage::FieldType::INTEGER) {
			Storage::FieldDetail fd;
			fd.field_name = rd.field_pk;
			getFieldValue(item, fd);
			where = { {rd.foreign_fk, fd.int_value} };
		}
		//auto dest = rd.db->doRead<Module::ChestItems2>("C", where, {});
		//if (!dest.empty()) {
		//	for (auto di: dest) {
		//		item.addChestItem(di);
		//	}
		//}
	}
}

STOREENTMAP_RELATIONS_SET_END
STORENTMAP_DECLARE_END

/// EntityMap Person
STORENTMAP_DECLARE_BEGIN(Person, Module::Person)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(PersonID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Firstname, FieldType::STRING)
STOREENTMAP_FIELDS_DECLARE(Surname, FieldType::STRING)
STOREENTMAP_FIELDS_DECLARE(ChestID, FieldType::INTEGER)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STORERELMAP_RELATIONS_DECLARE(Chest, RelationType::ONETOONE)
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(ID, getPersonID)
GET_STRING_ACCESSOR(Firstname, getFirstname)
GET_STRING_ACCESSOR(Surname, getSurname)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(ID, setPersonID)
SET_STRING_ACCESSOR(Firstname, setFirstname)
SET_STRING_ACCESSOR(Surname, setSurname)
SET_INT_ACCESSOR(ChestID, setChestID)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_BEGIN
RELATION_ONE2ONE(Chest, FieldType::INTEGER, PersonID, FieldType::INTEGER, ChestID, ChestID, PersonID)
STOREENTMAP_RELATIONS_DETAIL_END
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_BEGIN
SET_ONE2ONE_ACCESSOR(Chest, Module::Chest, setChest)
STOREENTMAP_RELATIONS_SET_END
STORENTMAP_DECLARE_END


/// EntityMap Statue
STORENTMAP_DECLARE_BEGIN(Statute, Module::Statute)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(StatuteID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Title, FieldType::STRING)
STOREENTMAP_FIELDS_DECLARE(Quota, FieldType::INTEGER)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(StatuteID, getStatuteID)
GET_STRING_ACCESSOR(Title, getTitle)
GET_INT_ACCESSOR(Quota, getQuota)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(StatuteID, setStatuteID)
SET_STRING_ACCESSOR(Title, setTitle)
SET_INT_ACCESSOR(Quota, setQuota)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_EMPTY
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_EMPTY
STORENTMAP_DECLARE_END


} /* namespace Storage */


#endif /* ENTITY_H_ */
