/*
 * DBResultInterface.h
 *
 *  Created on: 30/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DRIVER_DBRESULT_H_
#define SRC_STORAGE_DRIVER_DBRESULT_H_

#include "../RowItem.h"

namespace Storage {
namespace Driver {

class DBDriver;

/// DBResult
class DBResult {

public:
	/// Constructor
	DBResult() {}
	/// Deconstructor
	virtual ~DBResult() {}
	/// Fetch
	/**
	 * \param row Storage::RowItem
	 * \return if more to fetch
	 */
	virtual bool Fetch(Storage::RowItem &row) = 0;

};

} /* namespace Driver */
} /* namespace Storage */

#endif /* SRC_STORAGE_DRIVER_DBRESULT_H_ */
