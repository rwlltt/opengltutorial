/*
 * DBFirebirdStatement.h
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DBSQLITESTATEMENT_H_
#define SRC_STORAGE_DBSQLITESTATEMENT_H_

#include "DBDriver.h"
#include "storage/sqlite/sqlite3.h"

namespace Storage {
namespace Driver {

/// DBSqliteStatement
class DBSqliteStatement: public DBStatement {

	sqlite3 *m_st_db{0};
	//sqlite3_stmt *m_st_fetch{0};
	std::string m_sql{};
	Storage::RowItem m_placeHolderRow{};

	void splitReturningStatement(std::string &sql, std::string &table);

public:
	/// Constructor
	/**
	 * \param st IBPP Statement
	 * \param sql string
	 * \param placeHolderRow (default empty)
	 */
	DBSqliteStatement(sqlite3* db, const std::string &sql,
			const Storage::RowItem &placeHolderRow = {}):
		 m_st_db(db), m_sql(sql), m_placeHolderRow{placeHolderRow}
	{
	};
	/// Deconstructor
	virtual ~DBSqliteStatement(){
	};

	/// Execute
	/**
	 * \return DBResult
	 */
	virtual std::unique_ptr<DBResult> Execute() override;
	/// Execute
	/**
	 * \param name string name
	 * \return DBResult
	 */
	virtual std::unique_ptr<DBResult> ExecuteCursor(const std::string &name) override;
	/// Execute
	/**
	 * \return RowItem returning
	 */
	virtual Storage::RowItem ExecuteReturn() override;
};

} /* namespace Driver */
} /* namespace Storage */

#endif /* SRC_STORAGE_DBSQLITESTATEMENT_H_ */
