/*
 * DBStatement.h
 *
 *  Created on: 1/07/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DRIVER_DBSTATEMENT_H_
#define SRC_STORAGE_DRIVER_DBSTATEMENT_H_

#include "DBResult.h"
#include "../RowItem.h"
#include <memory>

namespace Storage {
namespace Driver {

class DBDriver;

/// DBStatement
class DBStatement {

public:
	/// Constructor
	DBStatement() {}
	/// Deconstructor
	virtual ~DBStatement() {}
	/// Execute
	/**
	 * \return int could be count of rows
	 */
	virtual std::unique_ptr<DBResult> Execute() = 0;
	/// Execute
	/**
	 * \param name string name
	 * \return DBResult
	 */
	virtual std::unique_ptr<DBResult> ExecuteCursor(const std::string &name) = 0;
	/// Execute
	/**
	 * \return RowItem returning
	 */
	virtual Storage::RowItem ExecuteReturn() = 0;


};

} /* namespace Driver */
} /* namespace Storage */




#endif /* SRC_STORAGE_DRIVER_DBSTATEMENT_H_ */
