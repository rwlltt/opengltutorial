/*
 * DBFirebirdDriver.h
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DBSQLITEDRIVER_H_
#define SRC_STORAGE_DBSQLITEDRIVER_H_

#ifdef NDEBUG 
#define D(x)
#else 
#define D(x) x
#endif

#include "DBDriver.h"
#include "DBStatement.h"
#include "storage/sqlite/sqlite3.h"
#include <fstream>

namespace Storage {
namespace Driver {

/// DBSqliteDriver
class DBSqliteDriver: public DBDriver {

   sqlite3 *m_db{0};

public:
	/// Constructor
	/**
	 * \param dbconnect
	 */
	DBSqliteDriver(const DatabaseConnection &dbconnect):
		DBDriver(dbconnect)
	{
	};
	/// Deconstructor
	virtual ~DBSqliteDriver(){
		
		disconnect();
		D(std::cerr << "Sqlite closed\n";)
	};

	/// Connect to DB
	/**
	 * \param user
	 * \param password
	 * \param role
	 */
	virtual void Connect(const std::string &user, const std::string &password, const std::string &role) override;
	/// Connection to DB made
	/**
	 * \return bool
	 */
	virtual bool isConnected() {
		return m_isConnected;
	}
	/// Prepare with rowitem placeholders data
	/**
	 * \param sql
	 * \param placeHolderRow
	 * \return DBStatement unique ptr
	 */
	virtual std::unique_ptr<DBStatement> CreateStatement(const std::string &sql, const Storage::RowItem &placeHolderRow = {}) override;
	/// has a transaction started
	/**
	 * \return bool
	 */
	virtual bool hasTransactionStarted() {
		return m_hasTransactionStarted;
	}
	/// Commit
	virtual void Commit() override;

	/// Create Sqlite database
	bool createDatabase(std::string file) ;
private:
	void disconnect();

};

} /* namespace Driver */
} /* namespace Storage */

#endif /* SRC_STORAGE_DBSQLITEDRIVER_H_ */
