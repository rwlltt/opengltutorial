/*
 * DBDriver.h
 *
 *  Created on: 29/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DRIVER_DBDRIVER_H_
#define SRC_STORAGE_DRIVER_DBDRIVER_H_

#include "DBResult.h"
#include "DBStatement.h"
#include "../DatabaseConnection.h"
#include "../RowItem.h"
#include <memory>
#include <string>
#include <vector>

namespace Storage {
namespace Driver {

/// Map of field details to field name by user
typedef std::vector<Storage::FieldDetail> DBParamList;

/// DBDriver interface
class DBDriver {

protected:
	DatabaseConnection m_dbconnect;      ///< DatabaseConnection
	bool m_isConnected{false};           ///< is it connected?
	bool m_hasTransactionStarted{false}; ///< has transaction started?

public:
	/// Constructor
	/**
	 * \param dbconnect database name
	 */
	DBDriver(const DatabaseConnection &dbconnect):
		m_dbconnect{dbconnect}
	{};
	/// Virtual deconstructor
	virtual ~DBDriver(){};

	/// Create DBDriver
	/**
	 * \param dbconnect database name
	 * \return shared_ptr<DBDriver>
	 */
	static std::unique_ptr<DBDriver> CreateDBDriver(const DatabaseConnection &dbconnect);
	/// Connect to DB
	/**
	 * \param user
	 * \param password
	 * \param role
	 */
	virtual void Connect(const std::string &user, const std::string &password, const std::string &role) = 0;
	/// Connection to DB made
	/**
	 * \return bool
	 */
	virtual bool isConnected() = 0;

	/// Prepare with rowitem placeholders data
	/**
	 * \param sql
	 * \param placeHolderRow
	 * \return DBStatement unique ptr
	 */
	virtual std::unique_ptr<DBStatement> CreateStatement(const std::string &sql, const Storage::RowItem &placeHolderRow = {}) = 0;
	/// has a transaction started
	/**
	 * \return bool
	 */
	virtual bool hasTransactionStarted() = 0;

	/// Commit
	virtual void Commit() = 0;
	
	void drop_table(DBDriver* dr, std::string table_name, std::string sql_drop);
	

};

} /* namespace Driver */
} /* namespace Storage */


#endif /* SRC_STORAGE_DRIVER_DBDRIVER_H_ */
