/*
 * DBSqliteResult.h
 *
 *  Created on: 30/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_DRIVER_DBSQLITERESULT_H_
#define SRC_STORAGE_DRIVER_DBSQLITERESULT_H_

#include "DBResult.h"
#include "storage/sqlite/sqlite3.h"

namespace Storage {
namespace Driver {

/// DBSqliteResult
class DBSqliteResult: public DBResult {
	int m_rowDone{0};
	int m_rc;
	sqlite3_stmt *m_st_fetch{0};

public:
	/// Constructor
	/**
	 * \param rc
	 * \param st
	 */
	DBSqliteResult(int rc, sqlite3_stmt * st):
	    m_rc(rc), m_st_fetch{st}
	{

	}
	virtual ~DBSqliteResult() {
		if (m_st_fetch) {;
			sqlite3_finalize(m_st_fetch);
		}
	}
	/// Fetch
	/**
	 * \param row Storage::RowItem
	 * \return bool of more to fetch
	 */
	virtual bool Fetch(Storage::RowItem &row) override;

};

} /* namespace Driver */
} /* namespace Storage */

#endif /* SRC_STORAGE_DRIVER_DBSQLITERESULT_H_ */
