/*
 * RowRecord.h
 */
#ifndef D_ROWRECORD_H
#define D_ROWRECORD_H

#include "FileRecord.h"

namespace Storage
{
/// Knows how to read and write a single data row to file
class RowRecord: public FileRecord
{
public:
    /// Create an empty row 
    RowRecord ();
    /// Create minimum record to write to file
    /**
     * \param recordID int of a record id
     * \param field1 string of field one
     */
    RowRecord ( int recordID, const char *field1 );
    /// Virtual deconstructor
    virtual ~RowRecord();
    /// Write to a file stream this row
    /**
     * \param ofs c++ output file stream
     */
    void StreamWrite ( std::ofstream &ofs );
    /// Read from a file stream this row
    /**
     * \param ifs c++ input file stream
     */
    void StreamRead ( std::ifstream &ifs );
    /// Set the record id
    /**
     * \param id int of record id
     */
    void setRecordID (int id);
    /// Set field one
    /**
     * \param value string of field
     */
    void setField1 ( std::string value );
    /// Set field two
    /**
     * \param value string of field
     */
    void setField2 ( std::string value );
    /// Set field three
    /**
     * \param value string of field
     */
    void setField3 ( std::string value );
    /// Get the record id
    /**
     * \return int of record id
     */
    int getRecordID ();
    /// Get field one
    /**
     * \return value string of field
     */
    std::string getField1 () const;
    /// Get field two
    /**
     * \return value string of field
     */
    std::string getField2 () const;
    /// Get field three
    /**
     * \return value string of field
     */
    std::string getField3 () const;

private:

    int m_recordID;
    char m_Field1[41]; // 40 charactores allowed plus null
    char m_Field2[41]; // 40 charactores allowed plus null
    char m_Field3[41]; // 40 charactores allowed plus null

};


}
#endif  /* D_ROWRECORD_H */
