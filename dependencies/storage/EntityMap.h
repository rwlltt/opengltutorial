/*
 * EntityMap.h
 *
 *  Created on: 5/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_ENTITYMAP_H_
#define SRC_STORAGE_ENTITYMAP_H_

#include "RowItem.h"
#include <map>
#include <unordered_map>
#include <algorithm>
#include <string>
#include <queue>
#include <sstream>
#include <string>
#include <memory>
#include <exception>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <ctime>

namespace Storage
{

class DBStorage;
template<class T>
class Bind;

/// For Storage Data mapping to named types and module methods
/**
 * \defgroup EntityDataMap Entity Map Traits
 * @{
 */
/// Standard exception override
class MapException: public std::exception
{
public:
    /// Constructor
	MapException(std::string what) :reason(what) {
		std::ofstream outfile;
		outfile.open("/var/log/storage/storage.log", std::ios_base::app);
		if (outfile.good()) {
		    outfile << "Error: " << what << "\n";
		}
		outfile.close();
    }
    /// Virtual Deconstructor
    virtual ~MapException() {
    }
    /// What went wrong
    const char *what() const noexcept {
        return ( reason.c_str() );
    }

private:
    std::string reason;

};





/// EntityMapBase
class EntityMapBase {

public:
	EntityMapBase() {};
	virtual ~EntityMapBase() {};

    virtual void setField(const std::string &field, const std::string &value) {};
    virtual void setField(const std::string &field, const int &value) {};

};

/// EntityMapBase
class EntityBase {

public:
	EntityBase() {};
	virtual ~EntityBase() {};

};

/// RelationMapBase
class RelationMapBase {

public:
	RelationMapBase() {};
	virtual ~RelationMapBase() {};

};

/// RelationBase
class RelationBase {

public:
	RelationBase() {};
	virtual ~RelationBase() {};

};


/// Template for Entity Trait data types
/**
 * \tparam T data type used for module data traits
 *
 */
template <class T>
struct EntityTraits {
    static const char *entitytype;  ///< entitytype type name
    static const char *entityname;  ///< name of table
    typedef T component_type;       ///< T component typedef

    /// getFieldValue
    /**
     * \param item const reference of item
     * \param fd reference FieldDetail
     */
    static void getFieldValue ( const component_type &item, FieldDetail &fd ) {}
    /// setFieldValue
    /**
     * \param item reference of item
     * \param fd reference FieldDetail
     */
    static void setFieldValue ( component_type &item, FieldDetail &fd ) {}

    /// getRelationDetail
    /**
     * \param fd reference FieldDetail
     */
    static void getRelationDetail ( RelationDetail &fd ) {}
    /// setFieldValue
    /**
     * \param item reference of item
     * \param rd reference FieldDetail
     */
    static void getRelationEntity ( component_type &item, RelationDetail &rd ) {}
    /// setFieldValue
    /**
     * \param item reference of item
     * \param rd reference FieldDetail
     */
    static void setRelationEntity ( component_type &item, RelationDetail &rd ) {}


};
// template <class T>
// const char *EntityTraits<T>::name = "Entity"; ///< initialize static string to name identify the T
// template <class T>
// const char *EntityTraits<T>::idname = "id"; ///< initialize static string to id identify the T
//template <class T>
//const char *EntityTraits<T>::entitytype = "Storage::Entity"; ///< initialize static string to name identify the T



/// For finding fieldnames to the EntityTraits map to getMethods, setMethods and fields queueing
/**
 * \tparam T the type name identified by static name
 * \tparam U the type name identifing the ModuleDataTraits of T
 *
 * \sa EntityTraits::name
 *
 */
template <typename T, typename U = Storage::EntityTraits<T>>
class EntityMap: public EntityMapBase
{
	std::string m_table_name{""};
    /**
     * \sa getMapField, setMapField, field_get_avaliable, field_set_avaliable, do_front_get and do_front_set
     */
    std::queue<FieldDetail> field_queue;
    /**
     * \sa getMapField, setMapField, field_get_avaliable, field_set_avaliable, do_front_get and do_front_set
     */
    std::queue<RelationDetail> relation_queue;

    std::shared_ptr<Storage::DBStorage> sp_db;

public:
    /// Map type definition of queue field value methods
    typedef typename std::unordered_map<std::string, std::string >::value_type map_tables_value_type;
    /// Map type definition of FieldDetails to field name
    typedef typename std::vector<std::pair<std::string, FieldType> >::value_type list_field_detail_value_type;
    /// Map type definition of queue relation value methods
    typedef typename std::unordered_map<std::string, std::string >::value_type map_relations_value_type;
    /// Map type definition of Relationtype to field name
    typedef typename std::vector<std::pair<std::string, RelationType> >::value_type list_relation_detail_value_type;

public:
    EntityMap(const std::string table_name): EntityMapBase{}, m_table_name{table_name} {
        //std::cout << table_name << "\n";
    };
    virtual ~EntityMap() {};

    /// create instance of T
    /**
     *\return T instance
     *
     */
    T createEntity() {
    	return T(m_table_name);
    }

    /// get field value of the item
    /**
     * \param[in] item const refernece to item of type T
     * \param[in] field const reference to the field name
     * \param[out] value reference of the value retrieved
     *
     */
    void getField ( const T &item, const std::string &field, std::string &value ) {
		auto table_map = U::map_table_fields[m_table_name];
        auto itrt = std::find_if ( table_map.begin(), table_map.end(), [&field] ( list_field_detail_value_type& p ) -> bool {
            return ( p.first == field );
        } );
        if ( itrt != table_map.end() ) {
            FieldType ft = itrt->second;
            FieldDetail fd;
            fd.field_name = field;
            fd.field_type = ft;
            if ( ft == FieldType::STRING ) {
                U::getFieldValue ( item, fd );
                value = fd.str_value;
            //    std::cout << "EntityMap type found GET STRING " << value << std::endl;
            } else {
                value = std::string ( "EntityMap: Field is not STRING type:- \"" + field + "\"." );
            }
        } else {
            value = std::string ( "EntityMap: Unknown field type found:- \"" + field + "\"." );
        }
    }
    /// get field value of the item
    /**
     * override for int
     * \param[in] item const refernece to item of type T
     * \param[in] field const reference to the field name
     * \param[out] value reference of the value retrieved
     *
     */
    void getField ( const T &item, const std::string &field, int &value ) {
		auto table_map = U::map_table_fields[m_table_name];
        auto itrt = std::find_if ( table_map.begin(), table_map.end(), [&field] ( list_field_detail_value_type& p ) -> bool {
            return ( p.first == field );
        } );
        if ( itrt != table_map.end() ) {
            FieldType ft = itrt->second;
            FieldDetail fd;
            fd.field_name = field;
            fd.field_type = ft;
            if ( ft == FieldType::INTEGER ) {
                U::getFieldValue ( item, fd );
                value = fd.int_value;
          //      std::cout << "EntityMap type found GET INTEGER " << value << std::endl;
            } else {
                value = -1;//std::string ( "EntityMap: Field is not INTEGER type:- \"" + field + "\"." );
            }
        } else {
            value = -1;//std::string ( "EntityMap: Unknown field type found:- \"" + field + "\"." );
        }
    }
    /// set field value of the item
    /**
     * \param[in,out] item refernece to item of type T
     * \param[in] field const reference to the field name
     * \param[in] value reference of the value retrieved
     *
     */
    void setField ( T &item, const std::string &field, const std::string &value )  {
 		auto table_map = U::map_table_fields[m_table_name];
        auto itrt = std::find_if (table_map.begin(), table_map.end(), [&field] ( list_field_detail_value_type& p ) -> bool {
            return ( p.first == field );
        } );
        if ( itrt != table_map.end() ) {
            FieldType ft = itrt->second;
            FieldDetail fd;
            fd.field_name = field;
            fd.field_type = ft;
            if ( ft == FieldType::STRING ) {
                fd.str_value = value;
                U::setFieldValue ( item, fd );
        //        std::cout << "EntityMap type found SET STRING " << value << std::endl;
            } else {
            	std::stringstream message;
            	message << "EntityMap setField (string): Field " << field << " not a string type for entity " << U::entitytype;
            	throw (MapException (message.str()));
            }
        } else {
        	std::stringstream message;
        	message << "EntityMap setField (string): Field " << field << " is mismatched for entity " << U::entitytype;
        	throw (MapException (message.str()));
        }
    }
    /// set field value of the item
    /**
     * override for int
     * \param[in,out] item refernece to item of type T
     * \param[in] field const reference to the field name
     * \param[in] value reference of the value retrieved
     *
     */
    void setField ( T &item, const std::string &field, const int &value ) {
 		auto table_map = U::map_table_fields[m_table_name];
        auto itrt = std::find_if ( table_map.begin(), table_map.end(), [&field] ( list_field_detail_value_type& p ) -> bool {
            return ( p.first == field );
        } );
        if ( itrt != table_map.end() ) {
            FieldType ft = itrt->second;
            FieldDetail fd;
            fd.field_name = field;
            fd.field_type = ft;
            if ( ft == FieldType::INTEGER ) {
                fd.int_value = value;
                U::setFieldValue ( item, fd );
           //     std::cout << "EntityMap type found SET INTEGER " << value << std::endl;
            } else {
            	std::stringstream message;
            	message << "EntityMap setField (int): Field " << field << " not an integer type for entity " << U::entitytype;
            	throw (MapException (message.str()));
            }
        } else {
        	std::stringstream message;
        	message << "EntityMap setField (int): Field " << field << " is mismatched for entity " << U::entitytype;
        	throw (MapException (message.str()));
        }

    }

    /// get mapped fieldnames as a queue so no need to use field names on a whole object
    /**
     * Useful for doing generic operation on a type of T
     * Search for field and if a found field type then queue field name for using the
     * field name queue.
     * \param[in] field const reference to field name
     * \sa field_avaliable and do_front
     */
    void getMapField ( const std::string &field ) {
 		auto tablename_map = U::map_tables;
 		auto table_map = U::map_table_fields[m_table_name];
        auto fld_itr = std::find_if ( tablename_map.begin(), tablename_map.end(), [&field] ( map_tables_value_type& p ) -> bool {
            return ( p.first == field );
        } );

        auto itrt = std::find_if ( table_map.begin(), table_map.end(), [&field] ( list_field_detail_value_type& p ) -> bool {
            return ( p.first == field );
        } );

        field_queue = std::queue<FieldDetail>();// new queue;

        if ( itrt != table_map.end() ) {
            FieldType ft = itrt->second;
            FieldDetail fd;
            fd.field_name = field;
            fd.field_type = ft;
            field_queue.push ( fd );
        } else if ( fld_itr != tablename_map.end() ) {
        	// For the entry found map all map fields
            for (auto pair: table_map) {
                Storage::FieldDetail fd;
                fd.field_name = pair.first;
                fd.field_type = pair.second;
                field_queue.push ( fd );
            }
        }
    }
    /// field available for get/set methods
    /**
     * It is required that the queue be used for either setfieldvalues only or getfieldvalues only
     * \return true if a field name is available
     */
    bool field_avaliable() {
        return ( !field_queue.empty() );
    }
    /// retrieve the next field name for a get/set method
    /**
     * It is required that the queue be used for either setfieldvalues only or getfieldvalues only
     * \return field string value of the field name
     */
    FieldDetail do_front() {
    	if (field_queue.empty()) {
    		FieldDetail fd;
    		fd.field_name = "field unavailable";
    		return fd;
    	}
        FieldDetail first = field_queue.front();
        field_queue.pop();
        return ( first );
    }
    /// set a relation value of the item
    /**
     * \param[in,out] item refernece to item of type T
     * \param[in] field const reference to the field name
     * \param[in] value pointer of DBStorage
     *
     */
    void setRelation ( T &item, const std::string &field, Storage::DBStorage* value )  {
        auto itrt = std::find_if ( U::list_relation_details.begin(), U::list_relation_details.end(), [&field] ( list_relation_detail_value_type& p ) -> bool {
            return ( p.first == field );
        } );
        if ( itrt != U::list_relation_details.end() ) {
			Storage::RelationDetail rd;
			rd.field_name = field;
			rd.relation_type = itrt->second;
			U::getRelationDetail ( rd );// updates rd
			//rd.rel_fields = value;
			rd.db = value;
			U::setRelationEntity ( item, rd );
        } else {
        	std::stringstream message;
        	message << "EntityMap setRelation: Field " << field << " is mismatched for entity " << U::entitytype;
        	throw (MapException (message.str()));
        }
    }

    /// get mapped relation names as a queue on a whole object
    /**
     * Useful for doing generic operation on a type of T
     * Search for relation field and if a found then queue for using the
     * relation name queue.
     * \sa relation_avaliable and do_relation_front
     */
    void getMapRelations () {
        relation_queue = std::queue<RelationDetail>();// new queue;
        //U entityMap;
		for (auto pair: U::list_relation_details) {
			Storage::RelationDetail rd;
			rd.field_name = pair.first;
			rd.relation_type = pair.second;
			U::getRelationDetail ( rd );// updates rd
			relation_queue.push ( rd );
		}
    }
    /// field available for get/set methods
    /**
     * It is required that the queue be used for either setfieldvalues only or getfieldvalues only
     * \return true if a field name is available
     */
    bool relation_avaliable() {
        return ( !relation_queue.empty() );
    }
    /// retrieve the next field name for a get/set method
    /**
     * It is required that the queue be used for either setfieldvalues only or getfieldvalues only
     * \return field string value of the field name
     */
    RelationDetail do_relation_front() {
    	if (relation_queue.empty()) {
    		RelationDetail fd;
    		fd.field_name = "field unavailable";
    		return fd;
    	}
    	RelationDetail first = relation_queue.front();
        relation_queue.pop();
        return ( first );
    }

};



/// STORENTMAP_INIT EntityMap Initializer
/**
 * Macro for CPP file to initialize the EntityMap static maps
 * \param entname name of the entity table name
 * \param entidname name of the entity unique/primary field name
 * \param cxxtype name of the entity type
// const char *EntityTraits<cxxtype>::name = #entname;             \
// const char *EntityTraits<cxxtype>::idname = #entidname;         \
 */
#define STORENTMAP_INIT(entname, entidname, cxxtype)   namespace Storage { \
const char *EntityTraits<cxxtype>::entitytype = #cxxtype;       \
const char *EntityTraits<cxxtype>::entityname = #entname;       \
entname ## TableMap EntityTraits<cxxtype>::map_tables =  EntityTraits<cxxtype>::init_table_map();                                  \
entname ## TableFieldsMap EntityTraits<cxxtype>::map_table_fields =  EntityTraits<cxxtype>::init_table_fields_map();           \
entname ## RelationMap EntityTraits<cxxtype>::map_relations =  EntityTraits<cxxtype>::init_relation_map();                         \
entname ## RelationDetailList EntityTraits<cxxtype>::list_relation_details =  EntityTraits<cxxtype>::init_relation_detail_list();  \
}

/// STORENTMAP_DECLARE_BEGIN Begin the EntityMap table declaration
/**
 *  Macro for H file to declare the EntityMap static maps
 *  Use same parameters as STOREENTMAP_INIT
 * \param entname name of the entity table name
 * \param cxxtype name of thne entity type
 * \sa STORENTMAP_INIT and STORENTMAP_DECLARE_END
    static const char *name;                                                \
    static const char *idname;                                          \
 */
#define STORENTMAP_DECLARE_BEGIN(entname, cxxtype)                                          \
typedef std::unordered_map<std::string, std::string> entname ## TableMap;                   \
typedef std::unordered_map<std::string, std::vector<std::pair<std::string, FieldType>>> entname ## TableFieldsMap;          \
typedef std::unordered_map<std::string, std::string> entname ## RelationMap;                \
typedef std::vector<std::pair<std::string, RelationType>> entname ## RelationDetailList;    \
template<>                                                                  \
struct EntityTraits<cxxtype> {                                              \
    static const char *entitytype;                                          \
    static const char *entityname;                                          \
    static entname ## TableMap         map_tables;                          \
    static entname ## TableFieldsMap  map_table_fields;                  \
    static entname ## RelationMap         map_relations;                    \
    static entname ## RelationDetailList  list_relation_details;            \
    typedef cxxtype                    component_type;                      \
    typedef entname ## TableMap        component_map_type;                  \
    typedef entname ## TableFieldsMap component_list_detail_type;          \
    typedef entname ## RelationMap        component_relmap_type;            \
    typedef entname ## RelationDetailList component_rellist_detail_type;    \
	static component_map_type init_table_map () {                           \
		component_map_type method_map;                                      \
		method_map.insert ( std::make_pair ( #cxxtype, #cxxtype));          \
		return method_map;                                                  \
	}                                                                       \
    static component_relmap_type init_relation_map () {                     \
	    component_relmap_type method_map;                                   \
	    method_map.insert ( std::make_pair ( #cxxtype, #cxxtype));          \
	    return method_map;                                                  \
    }

/// STOREENTMAP_FIELDS_BEGIN Begin the EntityMap fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_END
 */
#define STOREENTMAP_FIELDS_BEGIN                                            \
	static component_list_detail_type init_table_fields_map () {            \
        component_list_detail_type method_list;                             \
        auto list_field = method_list[entityname];

/// STOREENTMAP_FIELDS_DECLARE EntityMap fields declaration
/**
 * \param fld Field name as used in table
 * \param ftype Type of the field using Storage::FieldType enum
 */
#define STOREENTMAP_FIELDS_DECLARE(fld, ftype)                                  \
        list_field.push_back ( std::make_pair ( #fld, ftype ) );

/// STOREENTMAP_FIELDS_END End the EntityMap fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 */
#define STOREENTMAP_FIELDS_END                                                  \
        method_list[entityname]=list_field;                                     \
        return method_list;                                                     \
    }

/// STOREENTMAP_FIELDS_GET_BEGIN Begin the EntityMap get fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_GET_END
 */
#define STOREENTMAP_FIELDS_GET_BEGIN                                             \
    static void getFieldValue ( const component_type &item, FieldDetail &fd ) {

/// GET_STRING_ACCESSOR Get string fields declaration
/**
 *  Macro for H file to declare the EntityMap field with Storage::FieldType String
 *  \param fld Field name
 *  \param getfunc get method
 * \sa Storage::FieldType::STRING
 */
#define GET_STRING_ACCESSOR(fld, getfunc)                                        \
        if ( fd.field_name.compare(#fld) == 0 ) { fd.str_value = item.getfunc(); }

/// GET_INT_ACCESSOR Get int fields declaration
/**
 *  Macro for H file to declare the EntityMap field with Storage::FieldType Integer
 *  \param fld Field name
 *  \param getfunc get method
 * \sa Storage::FieldType::INTEGER
 */
#define GET_INT_ACCESSOR(fld, getfunc)                                           \
        if ( fd.field_name.compare(#fld) == 0 ) { fd.int_value = item.getfunc(); }

/// GET_STRING_ITEM Get fields declaration
/**
 *  Macro for H file to declare the EntityMap type with Storage::FieldType String
 *  \param fld Field name
 * \sa Storage::FieldType::STRING and STORENTMAP_DECLARE_BEGIN cxxtype
 */
#define GET_STRING_ITEM(fld)                                                     \
        if ( fd.field_name.compare(#fld) == 0 ) { fd.str_value = item; }

/// GET_INT_ITEM Get fields declaration
/**
 *  Macro for H file to declare the EntityMap type with Storage::FieldType Integer
 *  \param fld Field name
 * \sa Storage::FieldType::STRING and STORENTMAP_DECLARE_BEGIN cxxtype
 */
#define GET_INT_ITEM(fld)                                                        \
        if ( fd.field_name.compare(#fld) == 0 ) { fd.int_value = item; }


/// STOREENTMAP_FIELDS_GET_END End the EntityMap get fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 */
#define STOREENTMAP_FIELDS_GET_END                                               \
    }

/// STOREENTMAP_FIELDS_SET_BEGIN Begin the EntityMap set fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_GET_END
 */
#define STOREENTMAP_FIELDS_SET_BEGIN                                             \
    static void setFieldValue ( component_type &item, FieldDetail &fd ) {

/// SET_STRING_ACCESSOR Set string fields declaration
/**
 *  Macro for H file to declare the EntityMap field with Storage::FieldType String
 *  \param fld Field name
 *  \param setfunc set method
 * \sa Storage::FieldType::STRING
 */
#define SET_STRING_ACCESSOR(fld, setfunc)                                        \
        if ( fd.field_name.compare(#fld) == 0 ) { item.setfunc(fd.str_value); }

/// SET_INT_ACCESSOR Set int fields declaration
/**
 *  Macro for H file to declare the EntityMap field with Storage::FieldType Integer
 *  \param fld Field name
 *  \param setfunc set method
 * \sa Storage::FieldType::INTEGER
 */
#define SET_INT_ACCESSOR(fld, setfunc)                                           \
        if ( fd.field_name.compare(#fld) == 0 ) { item.setfunc(fd.int_value); }

/// SET_STRING_ITEM Set fields declaration
/**
 *  Macro for H file to declare the EntityMap type with Storage::FieldType String
 *  \param fld Field name
 * \sa Storage::FieldType::STRING and STORENTMAP_DECLARE_BEGIN cxxtype
 */
#define SET_STRING_ITEM(fld)                                                     \
        if ( fd.field_name.compare(#fld) == 0 ) { item = fd.str_value; }

/// SET_INT_ITEM Set fields declaration
/**
 *  Macro for H file to declare the EntityMap type with Storage::FieldType Integer
 *  \param fld Field name
 * \sa Storage::FieldType::STRING and STORENTMAP_DECLARE_BEGIN cxxtype
 */
#define SET_INT_ITEM(fld)                                                        \
        if ( fd.field_name.compare(#fld) == 0 ) { item = fd.int_value; }

/// STOREENTMAP_FIELDS_SET_END End the EntityMap set fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 */
#define STOREENTMAP_FIELDS_SET_END                                               \
    }

/// STORENTMAP_DECLARE_END End the EntityMap table declaration
/**
 *  Macro for H file to declare the EntityMap fields
 */
#define STORENTMAP_DECLARE_END                                                 \
};

/// ENTITY_INT_ATTRIBUTE declaration
/**
 *  \param fld Field name
 * \sa Storage::FieldType::STRING and STORENTMAP_DECLARE_BEGIN cxxtype
 */
#define ENTITY_INT_ATTRIBUTE(fld)                                              \
private:                                                                       \
	int m_##fld {0};                                                           \
public:                                                                        \
	int get##fld() const { return m_##fld; }                                   \
	void set##fld(const int &value){ m_##fld = value; }

/// ENTITY_STRING_ATTRIBUTE declaration
/**
 *  \param fld Field name
 * \sa Storage::FieldType::STRING and STORENTMAP_DECLARE_BEGIN cxxtype
 */
#define ENTITY_STRING_ATTRIBUTE(fld)                                           \
private:                                                                       \
	std::string m_##fld {};                                                    \
public:                                                                        \
	const std::string get##fld() const { return m_##fld; }                     \
	void set##fld(const std::string &value){ m_##fld = value; }


/// STOREENTMAP_RELATIONS_BEGIN Begin the EntityMap relations declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_RELATIONS_END
 */
#define STOREENTMAP_RELATIONS_BEGIN                                               \
	static component_rellist_detail_type init_relation_detail_list () {           \
        component_rellist_detail_type method_list;

/// STOREENTMAP_RELATIONS_DECLARE EntityMap relations declaration
/**
 * \param fld Field name as used in table
 * \param ftype Type of the field using Storage::FieldType enum
 */
#define STORERELMAP_RELATIONS_DECLARE(fld, ftype)                                 \
        method_list.push_back ( std::make_pair ( #fld, ftype ) );

/// STOREENTMAP_RELATIONS_END End the EntityMap fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 */
#define STOREENTMAP_RELATIONS_END                                                 \
        return method_list;                                                       \
    }

/// STOREENTMAP_RELATIONS_DETAIL_GET_BEGIN Begin the EntityMap get fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_GET_END
 */
#define STOREENTMAP_RELATIONS_DETAIL_BEGIN                       \
    static void getRelationDetail ( RelationDetail &rd ) {

/// GET_ONE2ONE_ACCESSOR Get one to one relation declaration
/**
 *  Macro for H file to declare the EntityMap type with Storage::FieldType Integer
 *  \param fld Field name
 *  \param rtype
 *  \param pk  primary key of this entity
 *  \param rfktype
 *  \param fk  foriegn key in this entity to other table
 *  \param fpk foreign table primary key
 *  \param ffk foriegn table foriegn key to this entity
 */
#define RELATION_ONE2ONE(fld, rtype, pk, rfktype, fk, fpk, ffk)  \
        if ( rd.field_name.compare(#fld) == 0 ) {       \
            rd.field_type_pk = rtype;                   \
			rd.field_pk = #pk;                          \
            rd.field_type_fk = rfktype;                 \
			rd.field_fk = #fk;                          \
			rd.foreign_table = #fld;                    \
			rd.foreign_pk = #fpk;                       \
			rd.foreign_fk = #ffk;                       \
         }

/// GET_ONE2MANY_ACCESSOR Get one to one relation declaration
/**
 *  Macro for H file to declare the EntityMap type with Storage::FieldType Integer
 *  \param fld Field name
 *  \param rtype
 *  \param pk  primary key of this entity
 *  \param rfktype
 *  \param fk  foriegn key in this entity to other table
 *  \param fpk foreign table primary key
 *  \param ffk foriegn table foriegn key to this entity
 */
#define RELATION_ONE2MANY(fld, rtype, pk, rfktype, fk, fpk, ffk)  \
        if ( rd.field_name.compare(#fld) == 0 ) {       \
            rd.field_type_pk = rtype;                   \
			rd.field_pk = #pk;                          \
            rd.field_type_fk = rfktype;                 \
			rd.field_fk = #fk;                          \
			rd.foreign_table = #fld;                    \
			rd.foreign_pk = #fpk;                       \
			rd.foreign_fk = #ffk;                       \
         }

/// STOREENTMAP_RELATIONS_DETAIL_GET_BEGIN Begin the EntityMap get fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_GET_END
 */
#define STOREENTMAP_RELATIONS_DETAIL_EMPTY                       \
    static void getRelationDetail ( RelationDetail & ) {         \
    }
/// STOREENTMAP_RELATIONS_DETAIL_GET_BEGIN Begin the EntityMap get fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_GET_END
 */
#define STOREENTMAP_RELATIONS_DETAIL_END                       \
      }

/// STOREENTMAP_RELATIONS_GET_BEGIN Begin the EntityMap get fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_GET_END
 */
#define STOREENTMAP_RELATIONS_GET_BEGIN                                             \
    static void getRelationEntity ( component_type &item, RelationDetail &rd ) {


/// STOREENTMAP_RELATIONS_GET_BEGIN Begin the EntityMap get fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_GET_END
 */
#define STOREENTMAP_RELATIONS_GET_EMPTY                                             \
    static void getRelationEntity ( component_type &, RelationDetail & ) {          \
    }

/// STOREENTMAP_RELATIONS_GET_END End the EntityMap get fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 */
#define STOREENTMAP_RELATIONS_GET_END                                               \
    }


/// STOREENTMAP_FIELDS_SET_BEGIN Begin the EntityMap set fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_GET_END
 */
#define STOREENTMAP_RELATIONS_SET_BEGIN                                             \
    static void setRelationEntity ( component_type &item, RelationDetail &rd ) {


/// STOREENTMAP_FIELDS_SET_BEGIN Begin the EntityMap set fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_GET_END
 */
//if (rd.db->unableRead()) {
//	std::stringstream message;
//	message << "EntityMap setRelation: Field " << #fld << " is unable read db for entity " << name;
//	throw (MapException (message.str()));
//}
#define SET_ONE2ONE_ACCESSOR(fld, etype, setfunc)                           \
	if ( rd.field_name.compare(#fld) == 0 ) {                               \
		EntityMap<etype, EntityTraits<etype>> emap{#fld};                                              \
		if (rd.relation_type == Storage::RelationType::ONETOONE) {          \
			DBRetrieveMap where;                                            \
			if (rd.field_type_pk == Storage::FieldType::INTEGER) {          \
				Storage::FieldDetail fd;                                    \
				fd.field_name = rd.field_pk;                                \
				getFieldValue(item, fd);                                    \
				where = {{rd.foreign_fk, fd.int_value}};                    \
			}                                                               \
			auto result = rd.db->retrieve("B", where, {});                  \
			if (!result.empty()) {                                          \
				auto item_fr = result.next<etype, EntityMap<etype, EntityTraits<etype>> >();                           \
				if (rd.field_type_fk == Storage::FieldType::INTEGER) {			\
						int int_value;                                          \
						emap.getField(item_fr, rd.foreign_pk, int_value);    \
						Storage::FieldDetail fd;                                \
						fd.field_name = rd.field_fk;                            \
						fd.int_value = int_value;                               \
						setFieldValue(item, fd);                                \
				}                                                           \
				item.setfunc(item_fr);                                   \
			}                                                               \
		}                                                                   \
	}

/// STOREENTMAP_FIELDS_SET_BEGIN Begin the EntityMap set fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_GET_END
 */
//if (rd.db->unableRead()) {
//	std::stringstream message;
//	message << "EntityMap setRelation: Field " << #fld << " is unable read db for entity " << name;
//	throw (MapException (message.str()));
//}
#define SET_ONE2MANY_ACCESSOR(fld, etype, setfunc)                          
	//if ( rd.field_name.compare(#fld) == 0 ) {                               \
	//	EntityMap<etype> emap{#fld};                                        \
	//	if (rd.relation_type == Storage::RelationType::ONETOMANY) {         \
	//		DBRetrieveMap where;                                            \
	//		if (rd.field_type_pk == Storage::FieldType::INTEGER) {          \
	//			Storage::FieldDetail fd;                                    \
	//			fd.field_name = rd.field_pk;                                \
	//			getFieldValue(item, fd);                                    \
	//			where = {{rd.foreign_fk, fd.int_value}};                    \
	//		}                                                               \
	//		auto result = rd.db->retrieve("C", where, {});                  \
	//		while (!result.empty()) {                                       \   
	//		}                                                               \
	//	}                                                                   \
	//}

	//auto item_rd = result.next<etype>();                        \
				//item.setfunc(item_rd);                                      \

/// STOREENTMAP_FIELDS_SET_BEGIN Begin the EntityMap set fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_GET_END
 */
#define STOREENTMAP_RELATIONS_SET_EMPTY                                             \
    static void setRelationEntity ( component_type &, RelationDetail & ) {          \
    }

/// STOREENTMAP_FIELDS_SET_BEGIN Begin the EntityMap set fields declaration
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STOREENTMAP_FIELDS_GET_END
 */
#define STOREENTMAP_RELATIONS_SET_END                                             \
    }




/// STORE ENTITY DECLARE Storage::Entity base class EntityMap table declaration
/**
 *\sa STORENTMAP_INIT and STORENTMAP_DECLARE_END
 */
// STORE_ENTITY_FIELDS_BEGIN Storage::Entity base class start of fields declare
/**
 *  Macro for H file to declare the EntityMap fields
 * \sa STORE_ENTITY_FIELDS_BEGIN
 */
#define STORE_ENTITY_FIELDS_BEGIN                                                  \
   using namespace Storage;                                                        \
   EntityTableFieldsMap method_list = EntityTraits<Entity>::map_table_fields;      \
   auto list_field = method_list[table_name];

#define STORE_ENTITY_FIELDS_DECLARE(fld, ftype)                                    \
   list_field.push_back ( std::make_pair ( #fld, ftype ) );

#define STORE_ENTITY_FIELDS_END                                                    \
    method_list[table_name] = list_field;                                          \
    EntityTraits<Entity>::map_table_fields = method_list; 


/// ENTITY_INT_DATA_ATTRIBUTE declaration
/**
 *  \param fname Method name
 *  \param tfld Field name
 * \sa Storage::FieldType::STRING and STORENTMAP_DECLARE_BEGIN cxxtype
 */
#define ENTITY_INT_DATA_ATTRIBUTE(fname, tfld)                                      \
const int get##fname() const {                                                      \
    using namespace Storage;                                                        \
    FieldDetail fd{};                                                               \
    fd.field_name = #tfld;                                                          \
    fd.field_type = FieldType::INTEGER;                                             \
    EntityTraits<Entity>::getFieldValue((Entity)*this, fd);                         \
    return fd.int_value;                                                            \
}                                                                                   \
void set##fname(const int &value) {                                                 \
    using namespace Storage;                                                        \
    FieldDetail fd{};                                                               \
    fd.field_name = #tfld;                                                          \
    fd.field_type = FieldType::INTEGER;                                             \
    fd.int_value = value;                                                           \
    EntityTraits<Entity>::setFieldValue(*this, fd);                                 \
}

 /// ENTITY_STRING_DATA_ATTRIBUTE declaration
 /**
  *  \param tfld Method name
  *  \param tfld Field name
  * \sa Storage::FieldType::STRING and STORENTMAP_DECLARE_BEGIN cxxtype
  */
#define ENTITY_STRING_DATA_ATTRIBUTE(fname, tfld)                                   \
const std::string get##fname() const {                                              \
    using namespace Storage;                                                        \
    FieldDetail fd{};                                                               \
    fd.field_name = #tfld;                                                          \
    fd.field_type = FieldType::STRING;                                              \
    EntityTraits<Entity>::getFieldValue((Entity)*this, fd);                         \
    return fd.str_value;                                                            \
}                                                                                   \
void set##fname(const std::string &value) {                                         \
    using namespace Storage;                                                        \
    FieldDetail fd{};                                                               \
    fd.field_name = #tfld;                                                          \
    fd.field_type = FieldType::STRING;                                              \
    fd.str_value = value;                                                           \
    EntityTraits<Entity>::setFieldValue(*this, fd);                                 \
}



/** @} */ /* group EntityDataMap */


} // end Storage namespace



#endif /* SRC_STORAGE_ENTITYMAP_H_ */
