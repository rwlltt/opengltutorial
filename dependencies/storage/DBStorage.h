#ifndef STORAGEDBSTORAGE_H_
#define STORAGEDBSTORAGE_H_

#include "EntityMap.h"
#include "StorageException.h"
#include "DatabaseStatement.h"
#include "DatabaseConnection.h"
#include "driver/DBDriver.h"
#include <vector>
#include <unordered_map>
#include <string>
#include <memory>
#include <mutex>
#include <tuple>

namespace Storage {

/// Tables stored uses a destination bind source and a Bind for binary file storage
/**
 * \sa Storage::DBStorage,  Control::Bind, Control::BindSourceBase and Storage::RowRecord
 */
struct DBStoreTable {
	MapStatement statements;                 ///< database statements

};

struct DBCallError {
	std::string configError;
	bool isConfig{false};
	bool islogged{false};
	std::string error;
};
/// Tuple to return results of success
/**
 * return true empty vector for success
 */
typedef std::tuple<bool, DBCallError> DBStorageCallReturn;
/// Table map for StoreTable information
typedef std::unordered_map<std::string, DBStoreTable> DBTableMap;
/// Iterator for map of table names
typedef std::unordered_map<std::string, DBStoreTable>::iterator DBTableMap_Iter;
/// Map value type for iter
typedef std::unordered_map<std::string, DBStoreTable>::value_type DBTableMap_Value_type;


//
// Public function
// bindItem - when an item is add or set with data - flow to destination source
template<class T, typename A = Storage::EntityMap<T,T>, typename M = Storage::EntityTraits<T>>
void bindItem(const Storage::RowItem &item, T & entity, A & entityMap) {
	unsigned short total = item.TotalFields();
	for (unsigned short index = 1; index <= total; ++index) {
		FieldDetail fd = item.getFieldDetail(index);
		int intValue;
		std::string strValue;
		bool isSet;
		switch (fd.field_type) {
		case Storage::FieldType::INTEGER:
		case Storage::FieldType::AUTOGENINT:
			item.getField(index, intValue, isSet);
			if (isSet == true) {
				entityMap.setField(entity, fd.field_name, intValue);
			}
			break;
		case Storage::FieldType::STRING:
			item.getField(index, strValue, isSet);
			if (isSet == true) {
				entityMap.setField(entity, fd.field_name, strValue);
			}
			break;
		}
	}
}

//template<class T, class U, typename A = Storage::EntityMap<T, U>, typename M = Storage::EntityTraits<U>>
//void bindItem2(const Storage::RowItem &item, T & entity, EntityMapBase* entityMap) {
//    unsigned short total = item.TotalFields();
//    for (unsigned short index = 1; index <= total; ++index) {
//        FieldDetail fd = item.getFieldDetail(index);
//        int intValue;
//        std::string strValue;
//        bool isSet;
//        switch (fd.field_type) {
//        case Storage::FieldType::INTEGER:
//        case Storage::FieldType::AUTOGENINT:
//            item.getField(index, intValue, isSet);
//            if (isSet == true) {
//                entityMap.setField(entity, fd.field_name, intValue);
//            }
//            break;
//        case Storage::FieldType::STRING:
//            item.getField(index, strValue, isSet);
//            if (isSet == true) {
//                entityMap.setField(entity, fd.field_name, strValue);
//            }
//            break;
//        }
//    }
//}

/**
*/
template<class T, typename A>// = Storage::EntityMap<T,T>>//, typename M = Storage::EntityTraits<T>>
void doUpdate(Storage::RowItem & item, T & entity, A & entityMap) {
	unsigned short total = item.TotalFields();
	for (unsigned short findex = 1; findex <= total; ++findex) {
		FieldDetail fd = item.getFieldDetail(findex);
		int intValue;
		std::string strValue;
		switch (fd.field_type) {
		case Storage::FieldType::AUTOGENINT:
		case Storage::FieldType::INTEGER:
			entityMap.getField(entity, fd.field_name, intValue);
			item.setField(findex, intValue);
			break;
		case Storage::FieldType::STRING:
			entityMap.getField(entity, fd.field_name, strValue);
			item.setField(findex, strValue);
			break;
		}
	}
}

/// Storage read collection used to read the database for at table
/**
 * \sa Storage::DBStorage	
*/
class ResultRetrieval {
	std::string m_table_name;
	int m_row_count{ 0 };
	std::unique_ptr<Storage::Driver::DBResult> m_dbselect_result;
	Storage::RowItem m_row;

public:
	ResultRetrieval();
	ResultRetrieval(const std::string &tablename, int count, std::unique_ptr<Storage::Driver::DBResult> result, Storage::RowItem row);

	int row_count() { return m_row_count; };
	bool empty();

	///////////////////////////////////////////////////////////////////////////////
	// public function
	///////////////////////////////////////////////////////////////////////////////
	template<class T, typename A = Storage::EntityMap<T>, typename M = Storage::EntityTraits<T>>
	T next() {
		if (m_row_count > 0 && m_dbselect_result->Fetch(m_row)) {
			m_row_count--;
			try {
				A entityMap(m_table_name);
				T entity = entityMap.createEntity();
				bindItem(m_row, entity, entityMap);
				entityMap.getMapRelations();
				//while (entityMap.relation_avaliable()) {
				//	auto rd = entityMap.do_relation_front();
				//	entityMap.setRelation(entity, rd.field_name, 0);
				//}
				return entity;
			}
			catch (std::exception &e) {
				std::string message("ResultRetrieval next: " + std::string(e.what()));
				throw StorageException(message);
			}
		}
		throw StorageException("ResultRetrieval next: End of list");
	}

    //template<class T, typename A = Storage::EntityMap<T>, typename M = Storage::EntityTraits<T>>
    //T next2() {
    //    if (m_row_count > 0 && m_dbselect_result->Fetch(m_row)) {
    //        m_row_count--;
    //        try {
    //            A entityMap(m_table_name);
    //            T entity = entityMap.createEntity();
    //            std::string str;
    //            entityMap.getField(entity, "MM", str);
    //                //bindItem2(m_row, entity, entityMap);
    //            entity.bindItem2(m_row, &entityMap);
    //            //entityMap.getMapRelations();
    //            //while (entityMap.relation_avaliable()) {
    //            //	auto rd = entityMap.do_relation_front();
    //            //	entityMap.setRelation(entity, rd.field_name, 0);
    //            //}
    //            return entity;
    //        }
    //        catch (std::exception &e) {
    //            std::string message("ResultRetrieval next: " + std::string(e.what()));
    //            throw StorageException(message);
    //        }
    //    }
    //    throw StorageException("ResultRetrieval next: End of list");
    //}


};

typedef std::tuple<ResultRetrieval, DBCallError> DBStorageResultRetrieval;

/// Knows how to read and write a database for a set of tables
class DBStorage {
	DatabaseConnection m_dbconnect { "", "", "", "" };
	std::string m_UserName { "" };
	std::string m_UserPassword { "" };
	std::string m_UserRole { "" };
	bool m_userLogged { false };
	std::string m_version;
	std::string m_xml;

	DBTableMap mapTableSource;

	mutable std::mutex m_Mutex{};
	mutable std::mutex m_DBCallMutex{};
    //bool m_shall_stop{false};
 /*   bool m_has_stopped{true};
	double m_fraction_done { 0.0 };*/
	std::string m_message { "" };
	std::unique_ptr<Storage::Driver::DBDriver> m_dbdr;

public:
	/// Create with a storage version so we can use the same tables in the version
	/**
	 * \param version description of the database storage and storage version
	 * \param xml char* xml for configuration of database store
	 */
	DBStorage(std::string version, const char *xml);
	/// Virtual deconstructor
	virtual ~DBStorage();

	/// User login to database
	/**
	 * \param user const reference to string
	 * \param password const reference to string
	 * \param role const reference to string (default is Guest role)
	 */
	DBStorageCallReturn loginUser(const std::string user, const std::string password,
			std::string role = "GUEST");
	/// Is a logged user
	/**
	 * \return true if user is logged
	 */
	bool isLoggedUser() const {
		return m_userLogged;
	}

	/// Read the db storage
	//template<class T, typename A = Storage::EntityMap<T>,	typename M = Storage::EntityTraits<T>>

	//std::vector<T> doRead(const std::string &tableName, DBRetrieveMap where, DBRetrieveMap parameters) {
	//	std::vector<T> dest;
	//	A tent(tableName);
	//	std::unique_ptr<Storage::RowBind<T>> bind(new Storage::RowBind<T>(tableName, &dest));
	//	DBStorageCallReturn result;
	//	result = doReadStore(tableName, bind.get(), where, parameters);
	//	if (std::get<0>(result) == true) {
	//		for (T &item : dest) {
	//			tent.getMapRelations();
	//			while (tent.relation_avaliable()) {
	//				auto rd = tent.do_relation_front();
	//				tent.setRelation(item, rd.field_name, this);
	//			}
	//		}
	//		return dest;
	//	} else {
	//		if (std::get<1>(result).isConfig == true) {
	//			throw StorageException(std::get<1>(result).configError);
	//		} else {
	//			throw StorageException(std::get<1>(result).error);
	//		}
	//	}
	//}
	/// Read the db storage
	/**
	* Using a template method to retrieve DataType entities in a DBResult
	* \tparam T entity type mainly
	* \tparam A entity map
	* \tparam M entity traits
	* \param where DBRetrievemap
	* \param parameters DBRetrieveMep
	* \return Control::List<T>
	*/
    ResultRetrieval retrieve(const std::string &tableName, DBRetrieveMap where, DBRetrieveMap parameters) {
		return retrieve_setup(tableName, where, parameters);
	}
	/// Add a new item in the db storage
	/**
	 * \tparam T entity type mainly
	 * \tparam A entity map
	 * \tparam M entity traits
	 * \param new_item reference to T()
	 * \return T
	 */
	template<class T, typename A = Storage::EntityMap<T>, typename M = Storage::EntityTraits<T>>
	T doAddItem(const std::string &tableName, const T &new_item) {
		A entityMap(tableName);
		auto db_stmt = db_statement(tableName); // can throw exception
		auto item = db_stmt.createRowItem();
		doUpdate(item, new_item, entityMap);
		DBStorageCallReturn result = doAddItem(tableName, item);
		T entity = new_item;
		bindItem(item, entity, entityMap);
		if (std::get<0>(result) == true) {
			T& item = entity;
			A tent(tableName);
			tent.getMapRelations();
			while (tent.relation_avaliable()) {
				auto rd = tent.do_relation_front();
				tent.setRelation(item, rd.field_name, this);
			}
			return item;
		} else {
			if (std::get<1>(result).isConfig == true) {
				throw StorageException(std::get<1>(result).configError);
			} else {
				throw StorageException(std::get<1>(result).error);
			}
		}

	}

	/// Add a new item in the db storage
	/**
	 * \tparam T entity type mainly
	 * \tparam A entity map
	 * \tparam M entity traits
	 * \param remove_item reference to T()
	 */
	template<class T, typename A = Storage::EntityMap<T>, typename M = Storage::EntityTraits<T>>
	void doRemoveItem(const std::string &tableName, const T &remove_item) {
		A entityMap(tableName);
		auto db_stmt = db_statement(tableName); // can throw exception
		auto item = db_stmt.createRowItem();
		doUpdate(item, remove_item, entityMap);
		DBStorageCallReturn result = doRemoveItem(tableName, item);
		if (std::get<0>(result) == false) {
			if (std::get<1>(result).isConfig == true) {
				throw StorageException(std::get<1>(result).configError);
			}
			else {
				throw StorageException(std::get<1>(result).error);
			}
		}
	}

	/// Add a new item in the db storage
	/**
	 * \tparam T entity type mainly
	 * \tparam A entity map
	 * \tparam M entity traits
	 * \param original_item reference to T()
	 * \param update_item reference to T()
	 * \return T
	 */
	template<class T, typename A = Storage::EntityMap<T>, typename M = Storage::EntityTraits<T>>
	T doUpdateItem(const std::string &tableName, const T &original_item, const T &update_item) {
		A entityMap(tableName);
		auto db_stmt = db_statement(tableName); // can throw exception
		auto orig_item = db_stmt.createRowItem();
		auto updt_item = db_stmt.createRowItem();
		doUpdate(orig_item, original_item, entityMap);
		doUpdate(updt_item, update_item, entityMap);
		DBStorageCallReturn result = doUpdateItem(tableName, orig_item, updt_item);
		if (std::get<0>(result) == true) {
			T& item = update_item;
			A tent(tableName);
			tent.getMapRelations();
			while (tent.relation_avaliable()) {
				auto rd = tent.do_relation_front();
				tent.setRelation(item, rd.field_name, this);
			}
			return item; // return updated
		} else {
			if (std::get<1>(result).isConfig == true) {
				throw StorageException(std::get<1>(result).configError);
			} else {
				throw StorageException(std::get<1>(result).error);
			}
		}
	}

private:
	inline static std::string toUppercase(const char *name);
	std::string db_version();
	DatabaseStatement &db_statement(const std::string tableName);

	/// Check able to read DB storage
	/**
	 * \param tablename
	 * \return bool true if able to read check passed
	 */
	bool checkTable(const std::string &tableName, std::string &errors);
	/// where criteria
	RowItem whereCriteria(DBRetrieveMap &where);
	/// Read the db storage
	/**
	 * Using the Tables bind object to update its the bind destination with doBind
	 * \param tableName string
	 * \param bind the bind to doBind and doUpdate to the BindSource
	 */
	//DBStorageCallReturn doReadStore(const std::string& tableName,
	//		Bind<Storage::RowItem> *bind, DBRetrieveMap where,
	//		DBRetrieveMap parameters);
	/// Read the db storage
	/**
	* Using the Tables bind object to update its the bind destination with doBind
	* \param tableName string
	* \param bind the bind to doBind and doUpdate to the BindSource
	*/
	ResultRetrieval retrieve_setup(const std::string& tableName, DBRetrieveMap where, DBRetrieveMap parameters);

	/// Add item to the db storage
	/**
	 * Using the Tables bind object to update its the bind destination
	 * with doUpdate to a row item and add to DB
	 * \param tableName string
	 * \param bind the bind to doBind and doUpdate to the BindSource
	 */
	DBStorageCallReturn doAddItem(const std::string &tableName, RowItem &item);

	/// Remove item in the db storage
	/**
	 * \param tableName string
	 * \param bind the bind to doBind and doUpdate to the BindSource
	 */
	DBStorageCallReturn doRemoveItem(const std::string &tableName, const RowItem &item);

	/// Update an item in the db storage
	/**
	 * Requires the original item and the updated item.
	 * It wont find the item to update if no original item.
	 * \param tableName string
	 * \param orig_item reference to RowItem()
	 * \param update_item reference to RowItem()
	 */
	DBStorageCallReturn doUpdateItem(const std::string& tableName, const RowItem &orig_item, const RowItem &updt_item);


};

} /* namespace Storage */

#endif /* DBSTORAGE_H_ */
