#ifndef D_BINDITERATOR_H
#define D_BINDITERATOR_H

#include "RowBind.h"
#include <vector>
#include <algorithm>


namespace Storage
{
/// Bind_iterator<DataType>: Used to move data with a bind doUpdate()
/**
 * An input_iterator over a DataType that has Bind knowledge.
 * Used in containers that transfer data from a source to destination.
 * The operator* performs the Binds doUpdate transfer of data.
 * <b>Note:</b> the reference to item returned by operator* should be copied to new variable
 * if you want to keep a copy of it in your program. The pointer will be overwritten in the next
 * forward iteration.
 * The iterator is still not fully implemnted, it is enough to use in std algorithms.
 *
 * \tparam T Data type used in the iterator
 *
 */
template <class T>
class Bind_iterator
{
public:
    ///@{
    //typedef __PTRDIFF_TYPE__ difference_type;  
    typedef size_t size_type;                  
    typedef T value_type;                      
    typedef T & reference;                     
    typedef T * pointer;            
    typedef std::input_iterator_tag iterator_category; 
    ///@}
    typedef Bind_iterator self_type;    ///< Self typedef
    
    /// Constructor of empty iterator
    Bind_iterator ( ) :item ( nullptr ), index ( 0 ), bind ( nullptr ) {};
    /// Constructor of the iterator
    /**
     * \param citem Data type pointer
     * \param cindex size_t size of the list
     * \param newbind pointer to the Bind with Data type fo citem
     */
    Bind_iterator ( pointer citem, size_type cindex, Bind<T>* newbind = nullptr ) :
        item ( citem ),
        index ( cindex ),
        bind ( newbind ) {
    };
    /// Copy constructor of the iterator
    Bind_iterator ( const self_type& i ) :item ( i.item ), index ( i.index ), bind ( i.bind ) {};
    /// Virtual deconstructor of the iterator
    ~Bind_iterator() {};
    /// Assignment operator of the iterator
    self_type& operator= ( const self_type& it ) {
        item = it.item;
        index = it.index;
        bind = it.bind;
        return *this;
    };
    /// Equality operator of the iterator
    bool operator== ( const self_type& it ) const {
        return ( index == it.index );
    };
    /// Inequality operator of the iterator
    bool operator!= ( const self_type& it ) const {
        return ( index != it.index );

    };
    /// Forward iteration operator
    self_type& operator++() {
        index++;
        return ( *this );
    };
    /// Dereference operator of Data type and causes doUpdate on item
    /**
     * Note the reference to item returned should be copied if used in an iteration to another
     * object as the returned item is a pointer to the same object in the container using
     * the Bind_iterator.
     * \return reference to item member.
     */
    reference operator*() const {
        if ( bind != nullptr ) {
            bind->doUpdate ( *item, index );
            return *item;
        }
        return *item;
    };
    /*pointer operator->() const {
        return item;
    };*/
    /// Assignment operator of an item to write at iterator
    /**
     * \param item reference
     * \sa operator =
     */
    void update ( const reference item ) const {
        if ( bind != nullptr ) {
            bind->doBindUpdate( item, index );
        }
    };

    /// Operator = for the T item reference to set its value at the bind location
    /**
     * \param item reference
     * \sa update(item)
     */
    void operator= ( const reference item ) const {
        if ( bind != nullptr ) {
            bind->doBindUpdate( item, index );
        }
    };

private:
    pointer item;
    int index{0};
    Bind<T>* bind;  //Used to update source from a component.
};

}
#endif /* D_BINDITERATOR_H */
