/*
 * ParseDatabaseStatement.h
 */

#ifndef D_PARSEDATABASESTATEMENT_H_
#define D_PARSEDATABASESTATEMENT_H_


#include <string>
#include <sstream>
#include <unordered_map>
#include <vector>
#include "DatabaseStatement.h"
#include "DatabaseConnection.h"

namespace Storage
{



/// Parse Database Statement 
/**
 */
class ParseDatabaseStatement
{
    const char *m_xml{};
    std::stringstream m_xmlerror{};   ///< what was xml parser problem
    std::string statement_dbo_name{}; ///< statement data type (set in constructor and required)
    MapStatement m_statements{};
    std::string m_a_entity{"entity"}, m_a_name{"name"}, m_a_key{"key"};
    std::string m_a_type{"type"};
	
public:
    /// Constructor
    /**
     * \param datatype const reference to string of the datatype name
     * \param xml const char xml
     */
    ParseDatabaseStatement ( const std::string &datatype, const char *xml );
    /// Virtual deconstructor
    ~ParseDatabaseStatement();

    /// Public Read Statements method
    /**
     * \return map of DatabaseStatement for datatype
     * \sa ParseDatabaseStatement(), Storage::DatabaseStatement()
     */
    MapStatement ReadStatements ();
    /// Get database statements xml read errors if any
    /**
     * Empty if no error
     * \return string of xml errors
     */
    std::string getXmlErrors ();
    
private:

    // trim white spaces
    inline static std::string trimWhitespaces ( std::string value );
    // to Uppercase a name
    inline static std::string toUppercase ( const char *name );
  	bool find_attributes(map_attrs name_value);
  	std::tuple<StatementType, bool> statement_type(std::string type);
	std::tuple<FieldType, bool> field_type(std::string type);

};

}

#endif /* D_PARSEDATABASESTATEMENT_H_*/
