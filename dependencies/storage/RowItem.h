/*
 * RowItem.h
 */
#ifndef D_ROWITEM_H
#define D_ROWITEM_H

#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>

namespace Storage {
    /// Field type of row item field
    class DBStorage;

    /// FieldType
    enum class FieldType : char {
        STRING = 1, ///< String std::string
        INTEGER = 2, ///< Integer int
		AUTOGENINT = 3 ///< Auto generated integer
    };

    /// Relation type of relation
    enum class RelationType : char {
        ONETOONE = 1, ///< String std::string
        ONETOMANY = 2 ///< Integer int
    };

    /// Field detail mapped with index of field
    struct FieldDetail {
    	/// Default constructor
    	FieldDetail() {};
    	/// Constructor
    	/**
    	 * Construct an FieldType::INTEGER field details
    	 * \param name
    	 * \param value
    	 */
        FieldDetail(std::string name, int value) {
    		field_name = name;
    		int_value = value;
    		field_type = FieldType::INTEGER;
    		is_set = true;
    	}
    	/// Constructor
    	/**
    	 * Construct an FieldType::STRING field details
    	 * \param name
    	 * \param value
    	 */
        FieldDetail(std::string name, std::string value) {
    		field_name = name;
    		str_value = value;
    		field_type = FieldType::STRING;
    		is_set = true;
    	}
        std::string field_name{""};                       ///< field name
        FieldType field_type{Storage::FieldType::STRING}; ///< field type
        int int_value{0};                                 ///< int value
        std::string str_value{""};           ///< string value
        bool key_field{false};               ///< a key field
        bool is_set{false};                  ///< empty then dont use this index value
        bool is_autogen{false}; ///< is an auto generated field in insert

        /// details of the FieldDetail structure
        /**
         * \return string
         * */
        std::string detailed() const {
            std::stringstream value;
            value << "[" << field_name << " ";
            switch (field_type) {
                case Storage::FieldType::INTEGER:
                    value << "INTEGER ";
                    break;
                case Storage::FieldType::STRING:
                    value << "STRING ";
                    break;
                 default:
                    /* nothing */
                     break;
            }
            if (key_field == true) {
                value << "ISKEY ";
            }
            if (is_set == true) {
                value << "ISSET ";
            }
            if (is_autogen == true) {
                value << "AUTOGEN ";
            }
            value << "STR:" << str_value << " ";
            value << "INT:" << int_value << " ";
            value << "]";
            return value.str();
        }

         /// Equality operator
        /**
         * \param rhs right hand side
         * \return true if equal
         */
       bool operator==(const FieldDetail &rhs) const {
            if (field_name.compare(rhs.field_name) == 0 &&
                    field_type == rhs.field_type &&
                    int_value == rhs.int_value &&
                    str_value.compare(rhs.str_value) == 0 &&
                    key_field == rhs.key_field &&
                    is_set == rhs.is_set) {
                return true;
            } else {
                return false;
            }
        }
        /// Inequality operator
        /**
         * \param rhs right hand side
         * \return true if not equal
         */
        bool operator!=( const FieldDetail &rhs) const {
            return !(*this == rhs);
        }
    };

    /// Relation detail mapped with index of field

    struct RelationDetail {
    	/// Default constructor
    	RelationDetail() {};
    	/// Constructor
    	/**
    	 * Construct an FieldType::INTEGER field details
    	 * \param name
    	 * \param value
    	 */
        RelationDetail(std::string name, Storage::RelationType value) {
    		field_name = name;
    		relation_type = value;
    	}

    	std::string field_name{""};                               ///< field name -
        RelationType relation_type{Storage::RelationType::ONETOONE}; ///< relation type
        FieldType field_type_pk{Storage::FieldType::INTEGER}; ///< field type of foreign key
        FieldType field_type_fk{Storage::FieldType::INTEGER}; ///< field type of foreign key
        std::string field_pk{""};                          ///< primary key
        std::string field_fk{""};                          ///< foreign key
        std::string foreign_table{""};                     ///< foreign table
        std::string foreign_pk{""};                        ///< foreign primary key
        std::string foreign_fk{""};                        ///< foreign foreign key
        Storage::DBStorage* db{0};                         ///< pointer of DBStorage

        /// details of the FieldDetail structure
        /**
         * \return string
         * */
        std::string detailed() const {
            std::stringstream value;
            if (field_name.compare("") != 0) {
				value << "[" << field_name << " ";
				switch (relation_type) {
					case Storage::RelationType::ONETOONE:
						value << "One ";
						break;
					case Storage::RelationType::ONETOMANY:
						value << "Many ";
						break;
	                 default:
	                    /* nothing */
	                     break;
				}
				if (field_pk.compare("") != 0) {
					value << "PK";
					switch (field_type_pk) {
						case Storage::FieldType::INTEGER:
							value << "(INT) ";
							break;
						case Storage::FieldType::STRING:
							value << "(STR) ";
							break;
		                 default:
		                    /* nothing */
		                     break;
					}
					value << field_pk << " ";
					value << "FK";
					switch (field_type_fk) {
						case Storage::FieldType::INTEGER:
							value << "(INT) ";
							break;
						case Storage::FieldType::STRING:
							value << "(STR) ";
							break;
		                 default:
		                    /* nothing */
		                     break;
					}
					value << field_fk << " ";
					value << " To table " << foreign_table << " ";
					value << "PK " << foreign_pk << " ";
					value << "FK " << foreign_fk;
				}
				value << "]";
            }
            return value.str();
        }

         /// Equality operator
        /**
         * \param rhs right hand side
         * \return true if equal
         */
       bool operator==(const RelationDetail &rhs) const {
            if (field_name.compare(rhs.field_name) == 0 &&
                    relation_type == rhs.relation_type &&
                    field_type_pk == rhs.field_type_pk &&
                    field_type_fk == rhs.field_type_fk &&
					field_pk.compare(rhs.field_pk) == 0 &&
					field_fk.compare(rhs.field_fk) == 0 &&
					foreign_table.compare(rhs.foreign_table) == 0 &&
					foreign_pk.compare(rhs.foreign_pk) == 0 &&
					foreign_fk.compare(rhs.foreign_fk) == 0 &&
					db == rhs.db) {
                return true;
            } else {
                return false;
            }
        }
        /// Inequality operator
        /**
         * \param rhs right hand side
         * \return true if not equal
         */
        bool operator!=( const RelationDetail &rhs) const {
            return !(*this == rhs);
        }
    };

    ///Row fields by index so to field name and type
    typedef std::unordered_map<unsigned short, FieldDetail> RowFields;
    ///Row fields by index so to field name and type value type for iterating
    typedef std::unordered_map<unsigned short, FieldDetail>::value_type RowField_value_type;

    /// Knows how to read and write a single data row

    class RowItem {
        RowFields m_fields;

    public:
        /// Create an empty row
        RowItem();
        /// Create a row item with Row fields
        RowItem(const RowFields &fields);

        /// Virtual deconstructor
        virtual ~RowItem();

        /// get number of fields
        /**
         * \return vector of field string
         * */
        unsigned short TotalFields();
        /// Set field
        /**
         * \param index col index as short
         * \param value string value of field
         * \throw std::out_of_range
         */
        void setField(unsigned short index, const std::string &value);
        /// Set field
        /**
         * \param index col index as short
         * \param value integer value of field
         * \throw std::out_of_range
         */
        void setField(unsigned short index, int value);
        /// Get field std::string
        /**
         * \param[in] index int of col index
         * \param[out] value reference to string
         * \param[out] is_set is the field set with a value or empty
         * \throw std::out_of_range
         */
        void getField(unsigned short index, std::string &value, bool &is_set) const;
        /// Get field one
        /**
         * \param[in] index int of col index
         * \param[out] value int value
         * \param[out] is_set is the field set with a value or empty
         * \throw std::out_of_range
         */
        void getField(unsigned short index, int &value, bool &is_set) const;
        /// Get field one
        /**
         * \param[in] index int of col index
         * \return reference to FieldDetail at index
         * \throw std::out_of_range
         */
        const FieldDetail &getFieldDetail(unsigned short index) const;
        /// get number of feilds
        /** 
         * \return vector of field string
         * */
        unsigned short TotalFields() const;
        ///  get Item details as string value
        /** 
         * \return string
         * */
        std::string ItemDetail();

        /// Equality operator
        /**
         * \param lhs left hand side
         * \param rhs right hand side
         * \return true if equal
         */
        friend bool operator==(const RowItem &lhs, const RowItem &rhs);
        /// Inequality operator
        /**
         * \param lhs left hand side
         * \param rhs right hand side
         * \return true if not equal
         */
        friend bool operator!=(const RowItem &lhs, const RowItem &rhs);
        /// Field name and type equality
        /**
         * \param lhs left hand side
         * \param rhs right hand side
         * \return true if not equal
         */
        friend bool compareRowItemFields(const RowItem &lhs, const RowItem &rhs);

    };

    /// Equality operator

    /**
     * \param lhs left hand side
     * \param rhs right hand side
     * \return true if not equal
     */
    inline bool operator!=(const RowItem &lhs, const RowItem &rhs) {
        return !(lhs == rhs);
    }


}
#endif  /* D_ROWITEM_H */
