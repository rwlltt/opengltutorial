/*
 * RowBind.h
 *
 *  Created on: 4/06/2017
 *      Author: rodney
 */

#ifndef SRC_STORAGE_ROWBIND_H_
#define SRC_STORAGE_ROWBIND_H_

#include "RowItem.h"
#include "EntityMap.h"
#include <string>
#include <sstream>
#include <iostream>


namespace Storage {


/// Bind
/**
 * \tparam T type of entity
 */
template<class T>
class Bind {
public:
	virtual ~Bind(){};
public:
    virtual void doBind(const T &item) = 0;                     ///< abstract method
    virtual void doBindUpdate(const T &item, int index) = 0;    ///< abstract method
    virtual void doBindClear(const T &item, int index) = 0;     ///< abstract method
	virtual void doUpdate(T & item, int index) = 0;             ///< abstract method

};
/// DBStorage::RowBind
/**
 * \tparam T type of entity
 * \tparam A entity map
 * \tparam M Entity triat
 */
template<class T, typename A = Storage::EntityMap<T>, typename M = Storage::EntityTraits<T>>
class RowBind: public Bind<RowItem>
{
    A m_entityMap;
    M m_entityDataTrait;
    std::vector<T> *destSource;

public:
    /// Constructor
	RowBind ( const std::string &tableName, std::vector<T>* dest ): m_entityMap(tableName), destSource(dest) {};
    virtual ~RowBind(){};

    /// doBind - when an item is added use doBind to do it
    /**
     * The index is incremented in the container so used for new item to bind into
     * the container
     * \param[in] item RowItem
     */
    void doBind(const Storage::RowItem &item)
    {
	    T entity = m_entityMap.createEntity();
		bindItem(item, entity, m_entityMap);
		destSource->push_back(entity);
	}

    /// doBindUpdate - when an item is to be updated and requires the index
    /**
     * \param[in] item
     * \param[in] index integer of item
     */
    void doBindUpdate(const Storage::RowItem &item, int index)
    {
		T entity = destSource->at(index);
		bindItem(item, entity, m_entityMap);
		destSource->at(index) = entity;
	}

    /// doBindClear - when an item is removed and the index is required to be known
    /**
     * \param[in] item
     * \param[in] index
     */
    void doBindClear(const Storage::RowItem &/*item*/, int index)
    {
		T entity = m_entityMap.createEntity();
		destSource->at(index) = entity;
	}

	/// doUpdate - update an item from the bind to the item
    /**
     * \param[in,out] item item is updated from the bind component to the RowItem
     * \param[in] index
     */
	void doUpdate(Storage::RowItem & item, int index) {
		T entity = destSource->at(index);
	//	bindItem(item, entity);
		unsigned short total = item.TotalFields();
		for (unsigned short findex = 1; findex <= total; ++findex) {
			FieldDetail fd = item.getFieldDetail(findex);
			int intValue;
			std::string strValue;
			switch (fd.field_type) {
	        case Storage::FieldType::AUTOGENINT:
			case Storage::FieldType::INTEGER:
				m_entityMap.getField(entity, fd.field_name, intValue);
				item.setField(findex, intValue);
				break;
			case Storage::FieldType::STRING:
				m_entityMap.getField(entity, fd.field_name, strValue);
				item.setField(findex, strValue);
				break;
			}
		}
	}

private:
    //
    // Private
    // bindItem - when an item is add or set with data - flow to destination source
    //void bindItem (const Storage::RowItem &item, T & entity) {
	   // // we always do index from 1 and sequentially + 1 to Total fields
	   // unsigned short total = item.TotalFields();
	   // for (unsigned short index = 1; index <= total; ++index) {
	   // 	FieldDetail fd = item.getFieldDetail(index);
	   //     int intValue;
	   //     std::string strValue;
	   //     bool isSet;
	   //     switch (fd.field_type) {
	   //     case Storage::FieldType::INTEGER:
	   //     case Storage::FieldType::AUTOGENINT:
	   //         item.getField(index, intValue, isSet);
	   //         if (isSet == true) {
	   //     	    m_entityMap.setField(entity, fd.field_name, intValue);
	   //         }
	   //         break;
	   //     case Storage::FieldType::STRING:
	   //         item.getField(index, strValue, isSet);
	   //         if (isSet == true) {
	   //     	    m_entityMap.setField(entity, fd.field_name, strValue);
	   //         }
	   //         break;
	   //     }
	   // }

    //}

};

} /* namespace Storage */

#endif /* SRC_STORAGE_ROWBIND_H_ */
