#ifndef D_FILERECORD_H
#define D_FILERECORD_H

#include <fstream>

namespace Storage
{
/// Base class for storage read and write to streams
class FileRecord
{
public:
    /// Create a new empty file record
    FileRecord ();
    /// Virtual deconstructor
    virtual ~FileRecord();
    /// template to write the size of data to stream
    template<typename T>
    std::ostream &binary_write ( std::ostream &stream, const T &value ) {
        return stream.write ( reinterpret_cast<const char *> ( &value ), sizeof ( T ) );
    };
    /// template to read the size of data from stream
    template<typename T>
    std::istream &binary_read ( std::istream &stream, T &value ) {
        return stream.read ( reinterpret_cast<char *> ( &value ), sizeof ( T ) );
    };

protected:
    /// set field char array fixed size with string
    void setField ( char *field, const char *value, int size );

};


}
#endif  /* D_FILERECORD_H */
