/*
 * Hero.h
 */

#ifndef HERO_H_
#define HERO_H_

#include "EntityMap.h"
#include <string>

namespace Module
{

/// Hero
class Hero
{
	std::string m_table_name{"Hero"};
public:
    /// Constructor
	Hero(const std::string &table_name):m_table_name{table_name}
	{
	}
	/// Deconstructor
    virtual ~Hero() {};

	ENTITY_INT_ATTRIBUTE(HeroID)
	ENTITY_INT_ATTRIBUTE(PersonID)
	ENTITY_INT_ATTRIBUTE(StatuteID)

};


class Statute {
	std::string m_table_name{"Statute"};
public:
    /// Constructor
	Statute(const std::string &table_name):m_table_name{table_name} {}
	/// Constructor from person
	/**
	 * \param newid integer
	 * \param title const string
	 */
	Statute(int newid, const std::string &title):
	     m_StatuteID(newid), m_Title(title)
	{
	}
	/// virtual deconstructor
	virtual ~Statute(){};

	ENTITY_INT_ATTRIBUTE(StatuteID)
	ENTITY_STRING_ATTRIBUTE(Title)
	ENTITY_INT_ATTRIBUTE(Quota)
};

class ChestItems;
class Chest {
	std::string m_table_name{"Chest"};
public:
    /// Constructor
	Chest(const std::string &table_name):m_table_name{table_name} {}

	ENTITY_INT_ATTRIBUTE(ChestID)
	ENTITY_INT_ATTRIBUTE(PersonID)
	ENTITY_STRING_ATTRIBUTE(Name)
private:
	std::vector<ChestItems> chestitems{};
public:
	const std::vector<ChestItems> &getChestItems() const {	return chestitems; }
	void addChestItem(const ChestItems &newvalue){ chestitems.push_back(newvalue); }

};

class ChestItems2;
class Chest2 {
	std::string m_table_name{"Chest2"};
public:
    /// Constructor
	Chest2(const std::string &table_name):m_table_name{table_name} {}
	ENTITY_INT_ATTRIBUTE(ChestID)
	ENTITY_INT_ATTRIBUTE(PersonID)
	ENTITY_STRING_ATTRIBUTE(Name)
private:
	std::vector<ChestItems2> chestitems{};
public:
	const std::vector<ChestItems2> &getChestItems() const {	return chestitems; }
	void addChestItem(const ChestItems2 &newvalue){ chestitems.push_back(newvalue); }

};

class Person {
	std::string m_table_name{"Person"};
public:
    /// Constructor
	Person(const std::string &table_name):m_table_name{table_name} {}
	ENTITY_INT_ATTRIBUTE(PersonID)
	ENTITY_STRING_ATTRIBUTE(Firstname)
	ENTITY_STRING_ATTRIBUTE(Surname)
	ENTITY_INT_ATTRIBUTE(ChestID)

private:
	Chest chest{"Chest"};
public:
	const Chest getChest() const {	return chest; }
	void setChest(Chest newvalue){ chest = newvalue; }
};


class InventoryCount {
	std::string m_table_name{"InventoryCount"};
public:
    /// Constructor
	InventoryCount(const std::string &table_name):m_table_name{table_name} {}
	ENTITY_INT_ATTRIBUTE(InventoryID)
	ENTITY_INT_ATTRIBUTE(Quota)
	ENTITY_STRING_ATTRIBUTE(Name)
	ENTITY_INT_ATTRIBUTE(CountItem)
};

class Inventory {
	std::string m_table_name{"Inventory"};
public:
    /// Constructor
	Inventory(const std::string &table_name):m_table_name{table_name} {}

	ENTITY_INT_ATTRIBUTE(InventoryID)
	ENTITY_INT_ATTRIBUTE(Quota)
	ENTITY_STRING_ATTRIBUTE(Name)
};

class Inventory2 {
	std::string m_table_name{"Inventory2"};
public:
    /// Constructor
	Inventory2(const std::string &table_name):m_table_name{table_name} {}

	ENTITY_INT_ATTRIBUTE(InventoryID)
	ENTITY_INT_ATTRIBUTE(Quota)
	ENTITY_STRING_ATTRIBUTE(Name)
	ENTITY_INT_ATTRIBUTE(ChestID)
};


class ChestItems {
	std::string m_table_name{"ChestItems"};
public:
    /// Constructor
	ChestItems(const std::string &table_name):m_table_name{table_name} {}

	ENTITY_INT_ATTRIBUTE(ChestItemID)
	ENTITY_INT_ATTRIBUTE(InventoryID)
	ENTITY_INT_ATTRIBUTE(ChestID)

private:
	Inventory inventory{"Inventory"};
public:
	const Inventory getInventory() const { return inventory; }
	void setInventory(Inventory newvalue){ inventory = newvalue; }
};

class ChestItems2 {
	std::string m_table_name{"ChestItems2"};
public:
    /// Constructor
	ChestItems2(const std::string &table_name):m_table_name{table_name} {}

	ENTITY_INT_ATTRIBUTE(ChestItemID)
	ENTITY_INT_ATTRIBUTE(InventoryID)
	ENTITY_INT_ATTRIBUTE(ChestID)

private:
	Inventory2 inventory{"Inventory2"};
public:
	const Inventory2 getInventory() const { return inventory; }
	void setInventory(Inventory2 newvalue){ inventory = newvalue; }
};

} /* namespace Module */


#endif /* HERO_H_ */
