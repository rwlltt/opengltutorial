#ifndef APP_DATA_DEFINE_H
#define APP_DATA_DEFINE_H

#include <map>
#include <vector>
#include <memory>

namespace Storage
{
	class Entity;

	/// Field detail
	enum class DataDefine : char {
		DETAIL = 0,  ///< Key to 
		DATA = 1,    ///< Key to 
	};

	typedef std::vector<std::string> FieldOrder;
	typedef std::map<std::string, FieldOrder> ModelTable;
	// The data row from model is put into DataItem all strings
	typedef std::map<std::string, std::string> DataItem;
	typedef std::map<std::string, std::unique_ptr<Entity>> DataOne2OneNodes;
	typedef std::map<std::string, std::vector<std::unique_ptr<Entity>>> DataOne2ManyNodes;


}


#endif /* APP_DATA_DEFINE_H */
