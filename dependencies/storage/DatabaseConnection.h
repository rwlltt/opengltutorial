/*
 * DatabaseConnection.h
 */

#ifndef D_DATABASECONNECTION_H_
#define D_DATABASECONNECTION_H_

#include <string>
#include <map>

/// Storage namespace
namespace Storage {

	typedef std::map<std::string,std::string> map_attrs;


    /// Connection type

    /**
     * Specify a Connection Type.
     */
    enum class ConnectionType : char {
        LIVE, ///< Live connection
        TEST ///< Test connections
    };

    /// DatabaseConnection

    /**
     * Database connection to a database query.
     */
    class DatabaseConnection {
        std::string m_driver;
        std::string m_server;
        std::string m_dbFile;
        std::string m_charSet;

    public:
        /// Constructor
        /**
         * \param driver const reference of string
         * \param server const reference of string
         * \param dbFile const reference of string
         * \param charSet const reference of string
         */
        DatabaseConnection(const std::string &driver, 
                const std::string &server, 
                const std::string &dbFile, 
                const std::string &charSet);

        /// Driver
        /**
         * \return string
         */
        std::string getDriver() const;
        /// Server
        /**
         * \return string
         */
        std::string getServer();
        /// DBFile
        /**
         * \return string
         */
        std::string getDBFile();
        /// CharSet
        /**
         * \return string
         * */
        std::string getCharSet();

        /// Equality operator
        /**
         * \param lhs left hand side
         * \param rhs right hand side
         * \return true if equal
         */
        friend bool operator==(const DatabaseConnection &lhs, const DatabaseConnection &rhs);
        /// Inequality operator
        /**
         * \param lhs left hand side
         * \param rhs right hand side
         * \return true if not equal
         */
        friend bool operator!=(const DatabaseConnection &lhs, const DatabaseConnection &rhs);

    private:

        friend class ParseDatabaseConnection;

    };

    /// Equality operator
    inline bool operator==(const DatabaseConnection &lhs, const DatabaseConnection &rhs) {
        //    bool result = false;
        return (lhs.m_driver.compare(rhs.m_driver) == 0 &&
                lhs.m_dbFile.compare(rhs.m_dbFile) == 0 &&
                lhs.m_server.compare(rhs.m_server) == 0 &&
                lhs.m_charSet.compare(rhs.m_charSet) == 0);
    }
    /// Equality operator
    inline bool operator!=(const DatabaseConnection &lhs, const DatabaseConnection &rhs) {
        return !(lhs == rhs);
    }

} /* namespace Storage */

#endif /* D_DATABASECONNECTION_H_ */
