cmake_minimum_required(VERSION 3.12)
project(opengl_tut_lightson)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /std:c++17")

add_compile_definitions(_WINDOWS _UNICODE UNICODE LOKI_CLASS_LEVEL_THREADING LOKI_FUNCTOR_IS_NOT_A_SMALLOBJECT IMGUI_IMPL_OPENGL_LOADER_GLAD)

include_directories(../dependencies)
include_directories(../vendor)
include_directories(src)
include_directories(src/tests)

add_executable(opengl_tut_lightson
        ../dependencies/glad.c
        ../vendor/glm/detail/glm.cpp
        ../vendor/imgui/imgui.cpp
        ../vendor/imgui/imgui_demo.cpp
        ../vendor/imgui/imgui_draw.cpp
        ../vendor/imgui/imgui_impl_glfw.cpp
        ../vendor/imgui/imgui_impl_opengl3.cpp
        ../vendor/imgui/imgui_widgets.cpp
        ../vendor/stb_image/stb_image.cpp
		../vendor/MatrixStack.cpp
		../vendor/MousePoles.cpp
		../vendor/framework/rapidxml.hpp
		../vendor/framework/Mesh.cpp
		../vendor/framework/framework.cpp
		../vendor/framework/Timer.cpp
        src/AddRequest.cpp
        src/AddResponse.cpp
        src/Application.cpp
        src/ConsoleResponse.cpp
        src/Context.cpp
        src/dice.cpp
        src/GlobalUniformBuffer.cpp
		src/IndexBuffer.cpp
        src/InitAppRequest.cpp
        src/InitAppResponse.cpp
        src/main.cpp
        src/Model.cpp
        src/Module.cpp
        src/Renderer.cpp
        src/Request.cpp
        src/Response.cpp
        src/Shader.cpp
        src/Texture.cpp
        src/tree.cpp
        src/VertexArray.cpp
        src/VertexBuffer.cpp
        src/VertexBufferLayout.cpp
        src/tests/WorldScene.cpp
        src/tests/WorldScene.h
		src/Geometry.cpp
		src/Geometry.h src/Orientation.cpp src/Orientation.h src/AppFactory.cpp src/AppFactory.h)

target_compile_options(opengl_tut_lightson PRIVATE
		$<$<CXX_COMPILER_ID:MSVC>:/W3 >
		$<$<NOT:$<CXX_COMPILER_ID:MSVC>>:-Wall -Wextra -pedantic >)
#-Werror /WX warning as error
find_library(ioutility io_utility.lib PATH C:\\Users\\rodne\\Desktop\\io_utility\\x64\\Release)
find_library(storagemodule storagemodule.lib PATH C:\\Users\\rodne\\Desktop\\StorageModule\\x64\\Release)
find_library(pugixml pugixml.lib PATH C:\\Users\\rodne\\Desktop\\opengl_app\\lib\\Release)
find_library(glfw3 glfw3.lib PATH C:\\Users\\rodne\\Desktop\\opengl_app\\lib\\Release)

find_library(ws2_32 ws2_32.lib PATH C:\\Windows\\System32)
find_library(opengl32 opengl32.lib PATH C:\\Windows\\System32)
find_library(winmm winmm.lib PATH C:\\Windows\\System32)

target_link_libraries(opengl_tut_lightson ${ioutility} ${pugixml} ${glfw3} ${storagemodule} ${ws2_32} ${opengl32} ${winmm})

file(COPY res DESTINATION ${CMAKE_BINARY_DIR})
