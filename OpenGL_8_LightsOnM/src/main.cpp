﻿
//#ifdef _WIN32
//#define APIENTRY __stdcall
//#endif

/* Modern OpenGL with GLAD */
//#include <GLAD/glad.h> /* GLAD before glfw3 for GL.h */
//#include <GLFW/glfw3.h>

#include "Application.h"
#include <iostream>
//#include "linmath.h"
//#include "Renderer.h"


int main(int argc, char** argv)
{
	try {
		TheApp::Instance().Init();

		//std::thread run([] {TheApp::Instance().Run();});
		//run.join();
		TheApp::Instance().Run();
	}
	catch (std::exception &e) {
		std::cout << e.what() << "\n";
	}
	return EXIT_SUCCESS;
}


//std::vector<std::thread> v(10);
//for(auto& t: v)
//t = std::thread([]{ for(int n = 0; n < 10; ++n)
//{
//    std::cout << "Not interleaved " << n <<"\n";
//};
//});
//for (auto& t: v)
//t.join();
