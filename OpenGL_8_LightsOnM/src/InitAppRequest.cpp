#include "InitAppRequest.h"
#include "AppFactory.h"
#include "io_utility.h"
#include "loki/Singleton.h"
#include "InitAppResponse.h"

using namespace io_utility;

namespace {
	Request* CreateInitAppRequest() {
		std::string user;
		Environment env;
		return new InitAppRequest(user, env);
	}

	const bool registered = RequestFactory::Instance().Register("InitAppRequest", CreateInitAppRequest);
}


InitAppRequest::InitAppRequest(const std::string &user, const Environment &env) : Request(user, env)
{
	if (registered) {

	}
}


InitAppRequest::~InitAppRequest()
{
}


std::unique_ptr<Response> InitAppRequest::DoRequest(const std::string &user) {
	SetUser(user);
	// Here the user name is a new user and we create a user to work in the aplication
	// Add the user to the application users
	auto consoleres = std::unique_ptr<InitAppResponse>(dynamic_cast<InitAppResponse*>(ResponseFactory::Instance().CreateObject("InitAppResponse")));
	//consoleres->movieID = "2";
	return consoleres;

}
