#ifndef TUT_RESPONSE_
#define TUT_RESPONSE_

#include "Request.h"
#include <memory>
#include <map>
#include <set>

class Response
{
	std::unique_ptr<Request> req_;

public:
	Response();
	virtual ~Response();

public:
	void SetRequest(std::unique_ptr<Request> req) {
		req_ = std::move(req);
	}
	virtual void Display() const = 0;
	virtual std::tuple < std::string, std::set<char>, std::map<char, std::string>> Prompt() const = 0;
	const std::string &RequestUser() const { 
		return req_->RequestUser();  
	}

	virtual void OnUpdate(float deltaTime) {}
	virtual void OnRender() {}
	virtual void OnImGuiRender() {}

};

#endif /* TUT_RESPONSE_ */