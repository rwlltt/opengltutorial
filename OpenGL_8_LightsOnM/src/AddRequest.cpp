
#include "AppFactory.h"
#include "io_utility.h"
#include "AddRequest.h"
#include "AddResponse.h"

using namespace io_utility;

namespace {
	Request* CreateAddRequest() {
		std::string user_name;
		Environment env;
		return new AddRequest(user_name, env);
	}

	const bool registered = RequestFactory::Instance().Register("AddRequest", CreateAddRequest);
}


AddRequest::AddRequest(const std::string &user, const Environment &env) : Request(user, env)
{
	if (registered) {

	}
}


AddRequest::~AddRequest()
{
}


std::unique_ptr<Response> AddRequest::DoRequest(const std::string &user) {
	auto res = std::unique_ptr<AddResponse>(dynamic_cast<AddResponse*>(ResponseFactory::Instance().CreateObject("AddResponse")));
	res->movieID = "2";
	
	return res;

}
