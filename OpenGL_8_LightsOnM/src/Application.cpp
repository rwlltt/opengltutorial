
#include "Application.h"
#include "AppFactory.h"
#include "loki/Singleton.h"
#include "Renderer.h"
#include "Model.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
#include "GlobalUniformBuffer.h"
#include <chrono>
#include <future>
#include <iostream>

std::unique_ptr<test::WorldScene> Application::worldScene{};

glutil::ViewData Application::initialViewData =
        {
                glm::vec3(0.0f, 0.5f, 0.0f),
                glm::fquat(0.92387953f, 0.3826834f, 0.0f, 0.0f),
                5.0f,
                0.0f
        };

glutil::ViewScale Application::viewScale =
        {
                3.0f, 20.0f,
                1.5f, 0.5f,
                0.0f, 0.0f,		//No camera movement.
                2.0f/350.0f
        };

glutil::ObjectData Application::initialObjectData =
        {
                glm::vec3(0.0f, 0.5f, 0.0f),
                glm::fquat(1.0f, 0.0f, 0.0f, 0.0f),
        };

glutil::ViewPole Application::ViewPole{Application::initialViewData,
                                       Application::viewScale, glutil::MB_LEFT_BTN};
glutil::ObjectPole Application::ObjectPole{Application::initialObjectData,
                                           2.0f/350.0f, glutil::MB_RIGHT_BTN, &Application::ViewPole};
#define SMALL_ANGLE_INCREMENT 9.0f

Application::Application()
{
}

Application::~Application()
{
	std::cout << "App destructor\n";
}

void Application::Init() {
	//signal(SIGINT, signal_handler); // Catch Ctrl c to stop abrubt end.

	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		throw std::exception("GlfwInit failed");

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(640, 480, "Lights On", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		throw std::exception("GlfwCreate window failed");
	}
	glfwSetKeyCallback(window, key_callback);
    // Mouse
    glfwSetCursorPosCallback(window, MouseMotion);
	glfwSetMouseButtonCallback(window, MouseButton);
    glfwSetScrollCallback(window, MouseWheel);

    //glfwSetWindowSizeCallback(window, reshape_callback);
	glfwMakeContextCurrent(window);
	auto result = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	GLCall(std::cout << glGetString(GL_VERSION) << "\n");
	glfwSwapInterval(1);

	try {

		WSADATA wsaData;
		if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0) {
			fprintf(stderr, "WSAStartup failed.\n");
			throw std::exception("No WinSock");
		}
		
		m_user = std::make_unique<User>(m_user_name);
		Model::Instance().create_tables();
		SendAppMessage("InitAppRequest");
        //worldScene = std::make_unique<test::WorldScene>();
	}
	catch (const std::exception& e) {
		throw std::exception(e.what());
	}


}

void Application::Run() {
	{
		GLCall(glEnable(GL_BLEND));
		GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

		ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO(); (void)io;
		ImGui::StyleColorsDark();
		ImGui_ImplGlfw_InitForOpenGL(window, true);
		ImGui_ImplOpenGL3_Init(glsl_version);
		std::string message_in{ "" };
		std::map<std::string, std::future<std::unique_ptr<Response>>> mapped;
		int width, height;
        //bool show_demo_window = false;
        // Store start time of drawings
        std::chrono::time_point<std::chrono::steady_clock> startTimer = std::chrono::high_resolution_clock::now();
        Renderer renderer;
        worldScene = std::make_unique<test::WorldScene>();

        GlobalUniformBuffer globalUniformBuffer(2);
        worldScene->SetGlobalUniform(iGlobalMatricesBindingIndex, &globalUniformBuffer);

        globalUniformBuffer.SetGlobalUniform(iGlobalMatricesBindingIndex);
        renderer.RegisterGlobalUniform(&globalUniformBuffer);
        //worldScene->NotifyRenderer(renderer);

        GLCall(glEnable(GL_CULL_FACE));
        GLCall(glCullFace(GL_BACK));
        GLCall(glFrontFace(GL_CW));

        GLCall(glEnable(GL_DEPTH_TEST));
        GLCall(glDepthMask(GL_TRUE));
        GLCall(glDepthFunc(GL_LEQUAL));
        GLCall(glDepthRange(0.0f, 1.0f));
        GLCall(glEnable(GL_DEPTH_CLAMP));

		while (true)
		{
			if (glfwWindowShouldClose(window)) {
                std::cout << "Window Close\n";
				SendAppMessage("Quit");
				break;
			}

			{
				Lock lock;
				if (messages.size() > 0) {
					message_in = messages.front();
					messages.pop();
					if (message_in.compare("Quit") == 0)
						break;
				}
			}
			if (message_in.compare("") != 0) {
				if (mapped.count(message_in) == 1) { // A completed async task which is complete and is mapped
					if (mapped[message_in].valid()) {
						auto res = mapped[message_in].get();
						m_user->SetResponse(std::move(res));
					}
				}
				else {
					mapped[message_in + "Done"] = DoRequest(m_user_name, message_in);
				}
			}
			// Calculate deltatime from epoch
            auto ticker = std::chrono::high_resolution_clock::now();
            std::chrono::duration<float> diff = ticker-startTimer;
            float deltaTime  = diff.count();

            glfwGetFramebufferSize(window, &width, &height);
			renderer.ViewPort(width, height);
			renderer.Clear();

			// For all mapped users do update render and imguiupdate
            m_user->OnUpdate(0.0f);
            m_user->OnRender();

            worldScene->OnUpdate(deltaTime);
            worldScene->OnRender();

            // Start the Dear ImGui frame
			ImGui_ImplOpenGL3_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();

            worldScene->OnImGuiRender();

            // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()!
            // You can browse its code to learn more about Dear ImGui!).
            //ImGui::ShowDemoWindow(&show_demo_window);
			{
			  ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			}

			ImGui::Render();
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

            glfwSwapBuffers(window);
			glfwPollEvents();
		}

		glfwSetWindowShouldClose(window, 1);
	}
	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwDestroyWindow(window);
	glfwTerminate();
}

std::future<std::unique_ptr<Response>> Application::DoRequest(const std::string &user, const std::string &request)
{
	auto req = std::unique_ptr<Request>(RequestFactory::Instance().CreateObject(request));
	req->SetUser(user);
	std::future<std::unique_ptr<Response>> result = std::async(
		std::launch::async, [](std::unique_ptr<Request> req, const std::string &user, const std::string &request) {
		                    auto res = req->DoRequest(user);
							res->SetRequest(std::move(req));
							res->Display();
							TheApp::Instance().SendAppMessage(request +"Done");
							return res;
	                  }, std::move(req), user, request);
	return result;
}

void Application::SendAppMessage(const std::string &request) {
	Lock lock;
	messages.push(request);
}

void Application::MouseMotion(GLFWwindow* window, double x, double y)
{
    x = x + 0.5 - (x<0); //
    y = y + 0.5 - (y<0); //
    Framework::ForwardMouseMotion(ViewPole, (int)x, (int)y);
    Framework::ForwardMouseMotion(ObjectPole, (int)x, (int)y);
    worldScene->SetViewMatrix(ViewPole.CalcMatrix());
    worldScene->SetObjectMatrix(ObjectPole.CalcMatrix());
}

void Application::MouseButton(GLFWwindow* window, int button, int action, int mods)
{
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    x = x + 0.5 - (x<0); //
    y = y + 0.5 - (y<0); //
    Framework::ForwardMouseButton(ViewPole, button, action, (int)x, (int)y, mods);
    Framework::ForwardMouseButton(ObjectPole, button, action, (int)x, (int)y, mods);
    worldScene->SetViewMatrix(ViewPole.CalcMatrix());
    worldScene->SetObjectMatrix(ObjectPole.CalcMatrix());
}

void Application::MouseWheel(GLFWwindow* window,  double x, double y)
{
    x = x + 0.5 - (x<0); //
    y = y + 0.5 - (y<0); //
    //printf("Mouse wheel: %d, %d\n", (int) x, (int)y);
    Framework::ForwardMouseWheel(ViewPole, (int)y);
    Framework::ForwardMouseWheel(ObjectPole, (int)y);
    worldScene->SetViewMatrix(ViewPole.CalcMatrix());
    worldScene->SetObjectMatrix(ObjectPole.CalcMatrix());
}

void Application::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch (action)
    {
        case GLFW_PRESS :
            switch (key) {
                case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, GLFW_TRUE); break;
                case GLFW_KEY_SPACE:
                    {
                        worldScene->g_bScaleCyl = !worldScene->g_bScaleCyl;
                    }
                    break;
                case GLFW_KEY_A: {
                    worldScene->g_bShowAmbient = !worldScene->g_bShowAmbient;
                        if(worldScene->g_bShowAmbient)
                            printf("Ambient Lighting On.\n");
                        else
                            printf("Ambient Lighting Off.\n");
                    }
                    break;
                case GLFW_KEY_T: {
                    worldScene->g_bDoInvTranspose = !worldScene->g_bDoInvTranspose;
                    if (worldScene->g_bDoInvTranspose)
                        printf("Doing Inverse Transpose.\n");
                    else
                        printf("Bad lighting.\n");
                    }
                    break;
                case GLFW_KEY_C: {
                        worldScene->g_bDrawColoredCyl = !worldScene->g_bDrawColoredCyl;
                    }
                    break;
            default: /* Do nothing */ break;
        }
        break;
        default: /* Do nothing */ break;
    }

    // Continuous press no need to have key press each time
//    switch (mods) {
//        case GLFW_MOD_SHIFT: // Uppercase Letter
//            break;
  //      default: // lowercase letter
//            switch (key) {
//                case GLFW_KEY_T: {
//                        worldScene->g_bDoInvTranspose = !worldScene->g_bDoInvTranspose;
//                        if (worldScene->g_bDoInvTranspose)
//                            printf("Doing Inverse Transpose.\n");
//                        else
//                            printf("Bad lighting.\n");
//                    }
//                    break;
//                case GLFW_KEY_C: {
//                        worldScene->g_bDrawColoredCyl = !worldScene->g_bDrawColoredCyl;
//                    }
//                    break;
//                default: /* Do nothing */ break;
//            }
    //        break;
    //}

//    char c;
//    switch (key) {
//        case GLFW_KEY_Q: c='q'; break;
//        case GLFW_KEY_W: c='w'; break;
//        case GLFW_KEY_E: c='e'; break;
//        case GLFW_KEY_R: c='r'; break;
//        case GLFW_KEY_T: c='t'; break;
//        case GLFW_KEY_Y: c='y'; break;
//        case GLFW_KEY_U: c='u'; break;
//        default: c='Z'; break;
//    }
}
