/*
 * Module.cpp
 */

#include "Module.h"
#include <iostream>

namespace Module {

extern const char *dbVersion = "Module DBStorage 1.0 - September 6, 2016";
extern const char *dbConfigXml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
					"<module-dbstorage>\n"
					"  <database>\n"
					"   <file role=\"live\" driver=\"Sqlite\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
					"   <file role=\"test\" driver=\"Sqlite\" name=\"test.db\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
					"  </database>\n"
					"<table name=\"Movies\" key=\"M_ID\">\n"
					"  <database-statements>\n"
					"      <statement type=\"select\">\n"
					"        <field name=\"M_ID\" type=\"integer\"/>\n"
					"        <field name=\"Movie\" type=\"string\"/>\n"
					"      </statement>\n"
					"  </database-statements>\n"
					"</table>\n"
					"</module-dbstorage>\n"; ///< Module db config xml

extern const int dbNumberOfTables = 3;					
extern const char* dbTableNames[] = { "Movies", "orange", "peach" };

}