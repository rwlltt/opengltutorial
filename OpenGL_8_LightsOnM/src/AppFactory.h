//
// Created by rodney on 21/06/2020.
//

#ifndef OPENGL_TUT_LIGHTSON_APPFACTORY_H
#define OPENGL_TUT_LIGHTSON_APPFACTORY_H

#include "loki/Factory.h"
#include "loki/Singleton.h"
#include "Request.h"
#include "Response.h"

using namespace Loki;

template
        <
                class AbstractProduct,
                typename IdentifierType,
                typename ProductCreator = AbstractProduct * (*)(),
                template<typename, class> class FactoryErrorPolicy = DefaultFactoryError
        >
class AppFactory : public FactoryErrorPolicy<IdentifierType, AbstractProduct>
{
    typedef std::map<IdentifierType, ProductCreator> IdToProductMap;
    IdToProductMap associations_;

public:

    AppFactory()
            : associations_()
    {
    }

    ~AppFactory()
    {
        associations_.erase(associations_.begin(), associations_.end());
    }

    bool Register(const IdentifierType& id, ProductCreator creator)
    {
        return associations_.insert(
                typename IdToProductMap::value_type(id, creator)).second != 0;
    }

    bool Unregister(const IdentifierType& id)
    {
        return associations_.erase(id) != 0;
    }

    std::vector<IdentifierType> RegisteredIds()
    {
        std::vector<IdentifierType> ids;
        for (typename IdToProductMap::iterator it = associations_.begin();
             it != associations_.end(); ++it)
        {
            ids.push_back(it->first);
        }
        return ids;
    }

    AbstractProduct* CreateObject(const IdentifierType& id)
    {
        typename IdToProductMap::iterator i = associations_.find(id);
        if (i != associations_.end())
            return (i->second)();
        return this->OnUnknownType(id);
    }

};

typedef SingletonHolder< AppFactory<Request, std::string>, Loki::CreateUsingNew, Loki::DefaultLifetime, Loki::SingleThreaded > RequestFactory;
typedef SingletonHolder< AppFactory<Response, std::string>, Loki::CreateUsingNew, Loki::DefaultLifetime, Loki::SingleThreaded > ResponseFactory;

#endif //OPENGL_TUT_LIGHTSON_APPFACTORY_H
