#ifndef TUT_APPLICATION_
#define TUT_APPLICATION_

/* Modern OpenGL with GLAD */
#include <GLAD/glad.h> /* GLAD before glfw3 for GL.h */
#include <GLFW/glfw3.h>

#include "loki/Singleton.h"
#include "Response.h"
#include <queue>
#include <winsock.h>
#include <signal.h>
#include <future>
#include "framework/MousePole.h"
#include "tests/WorldScene.h"

using namespace Loki;

/* Application class */
class Application : public ClassLevelLockable<Application>
{
private:
	Application();
	virtual ~Application();

	std::queue<std::string> messages{};
	std::unique_ptr<User> m_user{};
	std::string m_user_name{ "anonymous" };
    std::future < std::unique_ptr<Response>> DoRequest(const std::string &user, const std::string &request);

	const char* glsl_version{ "#version 330" }; // GL 3.0 + GLSL 130
	GLFWwindow* window{ nullptr };
    static glutil::ViewData initialViewData;
    static glutil::ViewScale viewScale;
    static glutil::ObjectData initialObjectData;
    static std::unique_ptr<test::WorldScene> worldScene;
    static const int iGlobalMatricesBindingIndex = 0;

public:
    friend class CreateUsingNew<Application>;

    Application(const Application& other) = delete;
    Application& operator= (const Application &other) = delete;
    Application* operator&(Application object) = delete;

    void Init();
    void Run();
    void SendAppMessage(const std::string &request);

    static void error_callback(int error, const char* description)
    {
        fprintf(stderr, "Error: %s\n", description);
    }
    static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

    static void MouseMotion(GLFWwindow* window, double xpos, double ypos);
    static void MouseButton(GLFWwindow* window, int button, int action, int mods);
    static void MouseWheel(GLFWwindow* window,  double xoffset, double yoffset);
    static glutil::ViewPole ViewPole;
    static glutil::ObjectPole ObjectPole;

};

/* TheApp and Model as singleton instance (Loki methods)  */
typedef SingletonHolder< Application, Loki::CreateUsingNew, Loki::DefaultLifetime, Loki::ClassLevelLockable > TheApp;
//namespace {
//	void signal_handler(int sig) {
//		TheApp::Instance().SendAppMessage("Quit");
//	}
//}

#endif /* TUT_APPLICATION_ */