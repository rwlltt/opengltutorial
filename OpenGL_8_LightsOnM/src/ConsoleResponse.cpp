#include "ConsoleResponse.h"
#include "AppFactory.h"
#include "Model.h"
#include "io_utility.h"
#include "storage/Entity.h" 

using namespace io_utility;

namespace {
	Response* CreateConsoleResponse() {
		return new ConsoleResponse();
	}

	const bool registered = ResponseFactory::Instance().Register("ConsoleResponse", CreateConsoleResponse);
}


ConsoleResponse::ConsoleResponse()
{
}


ConsoleResponse::~ConsoleResponse()
{
//	std::cout << "Destruct ConsoleResponse.\n";
}

void ConsoleResponse::Display() const {
	auto result = Model::Instance().movie_list();
	while (!result.empty()) {
		auto item = result.next<Storage::Entity>();
		print({ item.getField("M_ID"), " ", item.getField("Movie") });
	}
	print({ "Size: ", std::to_string(result.row_count()) });
	print({ "Movie ID: ", movieID });
}

std::tuple<std::string, std::set<char>, std::map<char, std::string>> ConsoleResponse::Prompt() const {
	std::string prompt("Continue [AaQqCc]:");
	std::set<char> choices{ 'a', 'A', 'q', 'Q' };
	std::map<char, std::string> reqs{ {'a', "AddRequest"}, {'A', "AddRequest"}, {'q', "Quit"}, {'Q', "Quit"} };
	return std::make_tuple(prompt, choices, reqs);
}
