#ifndef TUT_TEST_ARMATURE
#define TUT_TEST_ARMATURE

#include "Test.h"
#include <Shader.h>
#include "framework/Mesh.h"
#include "GlobalUniformBuffer.h"

namespace test {

    class WorldScene : public Test {
	public:
        static bool g_bDrawColoredCyl;
        static bool g_bScaleCyl;
        static bool g_bDoInvTranspose;
        static bool g_bShowAmbient;

    private:
        Shader WhiteDiffuseColor;
        Shader VertexDiffuseColor;
        Shader WhiteAmbDiffuseColor;
        Shader VertexAmbDiffuseColor;
        Framework::Mesh CylinderMesh;
        Framework::Mesh PlaneMesh;
        glm::vec4 m_lightDirection;
        glm::vec3 translationA;
        glm::vec3 translationB;
        GlobalUniformBuffer *ubo;
        glm::mat4 viewMatrix;
        glm::mat4 objectMatrix;


    public:
        WorldScene();
		~WorldScene();

		void OnUpdate(float deltaTime) override;
		void OnRender() override ;
		void OnImGuiRender() override;
        void SetGlobalUniform(int iGlobalMatricesBindingIndex, GlobalUniformBuffer* ubo);
        void SetViewMatrix(glm::mat4 matrix);
        void SetObjectMatrix(glm::mat4 matrix);

    };


}

#endif /* TUT_TEST_ARMATURE*/

