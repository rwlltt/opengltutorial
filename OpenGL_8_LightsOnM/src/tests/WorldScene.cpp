
#include "WorldScene.h"
#include "imgui/imgui.h"
#include <glutil/MatrixStack.h>

//define GLM_ENABLE_EXPERIMENTAL
//include <glm/gtx/string_cast.hpp>

#define ARRAY_COUNT( array ) (sizeof( array ) / (sizeof( array[0] ) * (sizeof( array ) != sizeof(void*) || sizeof( array[0] ) <= sizeof(void*))))

namespace test {

    bool WorldScene::g_bDrawColoredCyl = true;
    bool WorldScene::g_bScaleCyl = false;
    bool WorldScene::g_bDoInvTranspose = true;
    bool WorldScene::g_bShowAmbient = false;

    WorldScene::WorldScene():
            WhiteDiffuseColor("res/shaders/DirVertexLighting_PN.shader"),
            VertexDiffuseColor("res/shaders/DirVertexLighting_PCN.shader"),
            WhiteAmbDiffuseColor("res/shaders/DirAmbVertexLighting_PN.shader"),
            VertexAmbDiffuseColor("res/shaders/DirAmbVertexLighting_PCN.shader"),
            CylinderMesh("UnitCylinder.xml"),
            PlaneMesh("LargePlane.xml"),
            m_lightDirection{0.866f, 0.5f, 0.0f, 0.0f},
            translationA {1.0, 1.0, -1.0},
            translationB {1.0, 1.0, -1.2},
            ubo(nullptr),
            viewMatrix{},
            objectMatrix{}

    {
	}

    WorldScene::~WorldScene()
	{
	}

//    void WorldScene::NotifyRenderer(Renderer &renderer) {
//        renderer.RegisterShader(&WhiteDiffuseColor);
//        renderer.RegisterShader(&VertexDiffuseColor);
//    }

    void WorldScene::SetGlobalUniform(int iGlobalMatricesBindingIndex, GlobalUniformBuffer* ubo) {
        WhiteDiffuseColor.SetGlobalUniform("GlobalMatrices", iGlobalMatricesBindingIndex);
        VertexDiffuseColor.SetGlobalUniform("GlobalMatrices", iGlobalMatricesBindingIndex);
        this->ubo = ubo;
    }

	void WorldScene::OnUpdate(float deltaTime)
	{
        // Store the delta
	}

	void WorldScene::OnRender()
	{
        glutil::MatrixStack modelMatrix;
        modelMatrix.SetMatrix(viewMatrix);

        glm::vec4 lightDirCameraSpace = modelMatrix.Top() * m_lightDirection;

        Shader &whiteDiffuse = g_bShowAmbient ? WhiteAmbDiffuseColor : WhiteDiffuseColor;
        Shader &vertexDiffuse = g_bShowAmbient ? VertexAmbDiffuseColor : VertexDiffuseColor;

        if (g_bShowAmbient) {
            whiteDiffuse.Bind();
            whiteDiffuse.SetUniform4f("lightIntensity", 0.8f, 0.8f, 0.8f, 1.0f);
            whiteDiffuse.SetUniform4f("ambientIntensity", 0.2f, 0.2f, 0.2f, 1.0f);
            whiteDiffuse.UnBind();
            vertexDiffuse.Bind();
            vertexDiffuse.SetUniform4f("lightIntensity", 0.8f, 0.8f, 0.8f, 1.0f);
            vertexDiffuse.SetUniform4f("ambientIntensity", 0.2f, 0.2f, 0.2f, 1.0f);
            vertexDiffuse.UnBind();

        } else {
            whiteDiffuse.Bind();
            whiteDiffuse.SetUniform4f("lightIntensity", 1.0f, 1.0f, 1.0f, 1.0f);
            whiteDiffuse.UnBind();
            vertexDiffuse.Bind();
            vertexDiffuse.SetUniform4f("lightIntensity", 1.0f, 1.0f, 1.0f, 1.0f);
            vertexDiffuse.UnBind();
        }

        whiteDiffuse.Bind();
        whiteDiffuse.SetUniform3f("dirToLight", lightDirCameraSpace.x, lightDirCameraSpace.y, lightDirCameraSpace.z);
        whiteDiffuse.UnBind();
        vertexDiffuse.Bind();
        vertexDiffuse.SetUniform3f("dirToLight", lightDirCameraSpace.x, lightDirCameraSpace.y, lightDirCameraSpace.z);
        vertexDiffuse.UnBind();

        {
            glutil::PushStack push(modelMatrix);
            //Render the ground plane.
            {
                glutil::PushStack push(modelMatrix);

                whiteDiffuse.Bind();
                whiteDiffuse.SetUniformMat4f("modelToCameraMatrix", modelMatrix.Top());
                glm::mat3 normMatrix(modelMatrix.Top());
                whiteDiffuse.SetUniformMat3f("normalModelToCameraMatrix", normMatrix);
                //whiteDiffuse.SetUniform4f("lightIntensity", 1.0f, 1.0f, 1.0f, 1.0f);
                PlaneMesh.Render();
                whiteDiffuse.UnBind();
            }
            //Render the Cylinder
            {
                glutil::PushStack push(modelMatrix);

                modelMatrix.ApplyMatrix(objectMatrix);

                if (g_bDrawColoredCyl) {
                    if(g_bScaleCyl)
                        modelMatrix.Scale(1.0f, 1.0f, 0.2f);

                    vertexDiffuse.Bind();
                    vertexDiffuse.SetUniformMat4f("modelToCameraMatrix", modelMatrix.Top());
                    glm::mat3 normMatrix(modelMatrix.Top());
                    if(g_bDoInvTranspose)
                    {
                        normMatrix = glm::transpose(glm::inverse(normMatrix));
                    }

                    vertexDiffuse.SetUniformMat3f("normalModelToCameraMatrix", normMatrix);
                    //vertexDiffuse.SetUniform4f("lightIntensity", 1.0f, 1.0f, 1.0f, 1.0f);
                    CylinderMesh.Render("lit-color");
                    vertexDiffuse.UnBind();

                } else {
                    whiteDiffuse.Bind();
                    whiteDiffuse.SetUniformMat4f("modelToCameraMatrix", modelMatrix.Top());
                    glm::mat3 normMatrix(modelMatrix.Top());
                    whiteDiffuse.SetUniformMat3f("normalModelToCameraMatrix", normMatrix);
                    //whiteDiffuse.SetUniform4f("lightIntensity", 1.0f, 1.0f, 1.0f, 1.0f);
                    CylinderMesh.Render("lit");
                    whiteDiffuse.UnBind();
                }
            }
        }
    }

	void WorldScene::OnImGuiRender()
	{
        ImGui::SliderFloat3("Offset 1", &translationA.x, -3.0f, 2.0f);
        ImGui::SliderFloat3("Offset 2", &translationB.x, -3.0f, 2.0f);
	}

    void WorldScene::SetViewMatrix(glm::mat4 matrix) {
        viewMatrix = matrix;
    }

    void WorldScene::SetObjectMatrix(glm::mat4 matrix) {
        objectMatrix = matrix;
    }


}