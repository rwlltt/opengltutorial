
#include "Renderer.h"
#include <iostream>
#include <glutil/glutil.h>
//#define GLM_ENABLE_EXPERIMENTAL
//#include <glm/gtx/string_cast.hpp>

void  GLClearError() {
	while (glGetError() != GL_NO_ERROR);
}

bool GLLogCall(const char* function, const char* file, int line) {
	while (GLenum error = glGetError()) {
		std::cout << "[OpenGl Error](" << std::hex << error << std::dec << "): " <<
			function << " " << file << ":" << line << '\n';
		return false;
	}
	return true;
}

Renderer::Renderer():
        fFrustumScale(CalcFrustumScale(20.0f)),
        fzNear{1.0f}, fzFar{1000.0f},
        cameraToClipMatrix(glm::vec4(fFrustumScale, 0.0f, 0.0f, 0.0f),
                           glm::vec4(0.0f, fFrustumScale, 0.0f, 0.0f),
                           glm::vec4(0.0f, 0.0f, -(fzFar + fzNear) / (fzNear - fzFar), -1.0f),
                           glm::vec4(0.0f, 0.0f, (2 * fzFar * fzNear) / (fzNear - fzFar), 0.0f)),
//        cameraToClipMatrix(),
        ubo(nullptr) {

//    cameraToClipMatrix[0].x = fFrustumScale;
//    cameraToClipMatrix[1].y = fFrustumScale;
//    cameraToClipMatrix[2].z = (fzFar + fzNear) / (fzNear - fzFar);
//    cameraToClipMatrix[2].w = -1.0f;
//    cameraToClipMatrix[3].z = (2 * fzFar * fzNear) / (fzNear - fzFar);

}

Renderer::~Renderer() {
}

void Renderer::ViewPort(int width, int height)
{
    glutil::MatrixStack persMatrix;
    persMatrix.Perspective(geometry.DegToRad(45.0f)/*45.0f*/, (width / (float)height), fzNear, fzFar);
    //cameraToClipMatrix[0].x = fFrustumScale * (height / (float)width);
    //cameraToClipMatrix[1].y = fFrustumScale;
    //std::cout << "CameratoClip Matrix: " << glm::to_string(cameraToClipMatrix) << std::endl;
    //std::cout << "Perspective  Matrix: " << glm::to_string(persMatrix.Top()) << std::endl;

    // Global Uniforms
    if (ubo != nullptr) {
        ubo->Bind();
        ubo->SetGlobalUniformDataIndex0(persMatrix.Top());
//        ubo->SetGlobalUniformDataIndex0(cameraToClipMatrix);
        ubo->UnBind();
    }

    GLCall(glViewport(0, 0, (GLsizei)width, (GLsizei)height));
}

void Renderer::Clear() const
{
    GLCall(glClearColor(0.0f, 0.0f, 0.0f, 0.0f));
    GLCall(glClearDepth(1.0f));
	GLCall(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
}

void Renderer::Draw(const VertexArray & va, const IndexBuffer & ib, const Shader & shader) const
{
	shader.Bind();
	va.Bind();
	ib.Bind();
	GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));

}

void Renderer::RegisterShader(Shader* shader) {
    shaders.push_back(shader);
}

void Renderer::RegisterGlobalUniform(GlobalUniformBuffer *ubo) {
    this->ubo = ubo;
}

float Renderer::CalcFrustumScale(float fFovDeg)
{
    const float degToRad = 3.14159f * 2.0f / 360.0f;
    float fFovRad = fFovDeg * degToRad;
    return 1.0f / tan(fFovRad / 2.0f);
}

