//
// Created by rodney on 17/06/2020.
//

#ifndef OPENGL_TUT_ORIENTATIONINTERPOLATION_ORIENTATION_H
#define OPENGL_TUT_ORIENTATIONINTERPOLATION_ORIENTATION_H


#include "glm/glm.hpp"
#include <vector>
#include <framework/Timer.h>
#include <glm/gtc/quaternion.hpp>

class Orientation {
private:
    bool m_bIsAnimating;
    int m_ixCurrOrient;
    bool m_bSlerp;
    std::vector<glm::fquat> orients;

public:
    Orientation(std::vector<glm::fquat> Orients);
    void UpdateTime();
    glm::fquat GetOrient() const;
    bool ToggleSlerp();
    inline bool IsAnimating() const {return m_bIsAnimating; };
    void AnimateToOrient(int ixDestination);

private:
    class Animation
    {
        std::vector<glm::fquat> m_orients;

    public:
        Animation(const std::vector<glm::fquat> &orients) : m_orients{orients}
        {
        }
        glm::vec4 Vectorize(const glm::fquat &theQuat) const
        {
            glm::vec4 ret;

            ret.x = theQuat.x;
            ret.y = theQuat.y;
            ret.z = theQuat.z;
            ret.w = theQuat.w;

            return ret;
        }

        glm::fquat Lerp(const glm::fquat &v0, const glm::fquat &v1, float alpha) const
        {
            glm::vec4 start = Vectorize(v0);
            glm::vec4 end = Vectorize(v1);
            glm::vec4 interp = glm::mix(start, end, alpha);

            printf("alpha: %f, (%f, %f, %f, %f)\n", alpha, interp.w, interp.x, interp.y, interp.z);

            interp = glm::normalize(interp);
            return glm::fquat(interp.w, interp.x, interp.y, interp.z);
        }

        glm::fquat Slerp(const glm::fquat &v0, const glm::fquat &v1, float alpha) const
        {
            float dot = glm::dot(v0, v1);

            const float DOT_THRESHOLD = 0.9995f;
            if (dot > DOT_THRESHOLD)
                return Lerp(v0, v1, alpha);

            glm::clamp(dot, -1.0f, 1.0f);
            float theta_0 = acosf(dot);
            float theta = theta_0*alpha;

            glm::fquat v2 = v1 + -(v0*dot);
            v2 = glm::normalize(v2);

            return v0*cos(theta) + v2*sin(theta);
        }

        //Returns true if the animation is over.
        bool UpdateTime()
        {
            return m_currTimer.Update();
        }

        glm::fquat GetOrient(const glm::fquat &initial, bool bSlerp) const
        {
            glm::fquat result;
            if(bSlerp)
            {
                result = Slerp(initial, m_orients[m_ixFinalOrient], m_currTimer.GetAlpha());
            }
            else
            {
                result = Lerp(initial, m_orients[m_ixFinalOrient], m_currTimer.GetAlpha());
            }
            return result;
        }

        void StartAnimation(int ixDestination, float fDuration)
        {
            m_ixFinalOrient = ixDestination;
            m_currTimer = Framework::Timer(Framework::Timer::TT_SINGLE, fDuration);
        }

        int GetFinalIx() const {return m_ixFinalOrient;}

    private:
        int m_ixFinalOrient;
        Framework::Timer m_currTimer;
    };

private:
    Animation m_anim;

};


#endif //OPENGL_TUT_ORIENTATIONINTERPOLATION_ORIENTATION_H
