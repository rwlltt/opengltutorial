//
// Created by rodney on 1/06/2020.
//

#ifndef OPENGL_TUT_HIERARCHYMODEL_GEOMETRY_H
#define OPENGL_TUT_HIERARCHYMODEL_GEOMETRY_H


#include <glm/glm.hpp>

class Geometry {

public:
    Geometry();
    ~Geometry();

    inline float DegToRad(float fAngDeg)
    {
        const float fDegToRad = 3.14159f * 2.0f / 360.0f;
        return fAngDeg * fDegToRad;
    };

    inline float Clamp(float fValue, float fMinValue, float fMaxValue)
    {
        if(fValue < fMinValue)
            return fMinValue;

        if(fValue > fMaxValue)
            return fMaxValue;

        return fValue;
    };

    glm::mat3 RotateX(float fAngDeg);

    glm::mat3 RotateY(float fAngDeg);

    glm::mat3 RotateZ(float fAngDeg);

    float ComputeAngleRad(float fElapsedTime, float fLoopDuration);
};


#endif //OPENGL_TUT_HIERARCHYMODEL_GEOMETRY_H
