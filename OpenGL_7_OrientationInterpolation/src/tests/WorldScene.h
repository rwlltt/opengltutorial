#ifndef TUT_TEST_ARMATURE
#define TUT_TEST_ARMATURE

#include <Shader.h>
#include <Renderer.h>
#include <VertexBuffer.h>
#include <VertexArray.h>
#include <VertexBufferLayout.h>
#include <glutil/MatrixStack.h>
#include "Test.h"
#include "framework/Mesh.h"
#include "Geometry.h"
#include "Orientation.h"

namespace test {
    struct TreeData
    {
        float fXPos;
        float fZPos;
        float fTrunkHeight;
        float fConeHeight;
    };

//    enum GimbalAxis
//    {
//        GIMBAL_X_AXIS,
//        GIMBAL_Y_AXIS,
//        GIMBAL_Z_AXIS,
//    };
//
//    struct GimbalAngles
//    {
//        GimbalAngles()
//                : fAngleX(0.0f)
//                , fAngleY(0.0f)
//                , fAngleZ(0.0f)
//        {}
//
//        float fAngleX;
//        float fAngleY;
//        float fAngleZ;
//    };

    enum OffsetRelative
    {
        MODEL_RELATIVE,
        WORLD_RELATIVE,
        CAMERA_RELATIVE,

        NUM_RELATIVES,
    };


    class WorldScene : public Test {
	public:
        static bool bDrawLookatPoint;
        static glm::vec3 camTarget;
        //In spherical coordinates.
        static glm::vec3 sphereCamRelPos;
//        static bool bDrawGimbals;
//        static bool bRightMultiply;
        static int iOffset;
//        GimbalAngles g_angles;


    private:
        const TreeData forest[98];

        Shader UniformColor;
        Shader ObjectColor;
        Shader UniformColorTint;

        Framework::Mesh ConeMesh;
        Framework::Mesh CylinderMesh;
        Framework::Mesh CubeTintMesh;
        Framework::Mesh CubeColorMesh;
        Framework::Mesh PlaneMesh;
       // std::unique_ptr<Framework::Mesh> Gimbals[3];
        Framework::Mesh ShipObject;

        float deltaTime = 0.0f;
        glm::vec3 translationA;
        glm::vec3 translationB;
        GlobalUniformBuffer *ubo;
        Geometry geometry;
        glm::fquat orientation;
        Orientation orient;
        char OrientKeys[7];


    public:
        WorldScene();
		~WorldScene();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;
        void SetGlobalUniform(int iGlobalMatricesBindingIndex, GlobalUniformBuffer* ubo);
        void NotifyRenderer(Renderer &renderer);

        void AdjBase(bool bIncrement);
        void AdjUpperArm(bool bIncrement);
        void AdjLowerArm(bool bIncrement);
        void AdjWristPitch(bool bIncrement);
        void AdjWristRoll(bool bIncrement);
        void AdjFingerOpen(bool bIncrement);
        void WritePose();
        void OffsetOrientation(const glm::vec3 &_axis, float fAngDeg);
        bool ToggleSlerp()  { return orient.ToggleSlerp(); };
        void ApplyOrientation(char key);

    private:
        glm::vec3 ResolveCamPosition();
        glm::mat4 CalcLookAtMatrix(const glm::vec3 &cameraPt, const glm::vec3 &lookPt, const glm::vec3 &upPt);
        void DrawForest(glutil::MatrixStack &modelMatrix);
        //Trees are 3x3 in X/Z, and fTrunkHeight+fConeHeight in the Y.
        void DrawTree(glutil::MatrixStack &modelMatrix, float fTrunkHeight = 2.0f, float fConeHeight = 3.0f);
        void DrawParthenon(glutil::MatrixStack &modelMatrix);
        //Columns are 1x1 in the X/Z, and fHieght units in the Y.
        void DrawColumn(glutil::MatrixStack &modelMatrix, float fHeight = 5.0f);
        //void DrawGimbal(glutil::MatrixStack &currMatrix, GimbalAxis eAxis, glm::vec4 baseColor);

    };


}

#endif /* TUT_TEST_ARMATURE*/

