//
// Created by rodney on 17/06/2020.
//

#include "Orientation.h"

//std::vector<glm::fquat> Orientation::orients = {};

Orientation::Orientation(std::vector<glm::fquat> Orients):
        m_bIsAnimating(false)
        , m_ixCurrOrient(0)
        , m_bSlerp(false)
        , orients(Orients)
        , m_anim(orients)

{}

void Orientation::UpdateTime() {
    if(m_bIsAnimating)
    {
        bool bIsFinished = m_anim.UpdateTime();
        if(bIsFinished)
        {
            m_bIsAnimating = false;
            m_ixCurrOrient = m_anim.GetFinalIx();
        }
    }

}

glm::fquat Orientation::GetOrient() const
{
    if(m_bIsAnimating)
        return m_anim.GetOrient(orients[m_ixCurrOrient], m_bSlerp);
    else {
//        for (glm::quat q : orients) {
//            printf("Q Orient: %f, %f, %f, %f\n",q.w, q.x, q.y, q.z);
//        }
//        printf("GetOrient: %f, %f\n",orients[m_ixCurrOrient],m_ixCurrOrient);
        return orients[m_ixCurrOrient];
    }
}

bool Orientation::ToggleSlerp() {
    m_bSlerp = !m_bSlerp;
    return m_bSlerp;
}

void Orientation::AnimateToOrient(int ixDestination) {
    if(m_ixCurrOrient == ixDestination)
        return;
    m_anim.StartAnimation(ixDestination, 1.0f);
    m_bIsAnimating = true;
}
