#shader vertex
#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 color;

smooth out vec4 interpColor;

layout(std140) uniform GlobalMatrices
{
	mat4 cameraToClipMatrix;
	mat4 worldToCameraMatrix;
};

uniform mat4 modelToWorldMatrix; // should benamed modelToCameraMatrix

void main()
{
	vec4 temp = modelToWorldMatrix * position;
	//temp = worldToCameraMatrix * temp;
	gl_Position = cameraToClipMatrix * temp;
	interpColor = color;
}


#shader fragment
#version 330 core

smooth in vec4 interpColor;

out vec4 outputColor;

void main()
{
	outputColor = interpColor;
}


