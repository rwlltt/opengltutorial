#shader vertex
#version 330 core

layout(location = 0) in vec4 position;

layout(std140) uniform GlobalMatrices
{
	mat4 cameraToClipMatrix;
	mat4 worldToCameraMatrix;
};

uniform mat4 modelToWorldMatrix;

void main()
{
	vec4 temp = modelToWorldMatrix * position;
	//temp = worldToCameraMatrix * temp;
	gl_Position = cameraToClipMatrix * temp;
}


#shader fragment
#version 330 core

uniform vec4 baseColor;

out vec4 outputColor;

void main()
{
	outputColor = baseColor;
}


