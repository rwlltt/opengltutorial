#ifndef TUT_INDEXBUFFER_
#define TUT_INDEXBUFFER_

class IndexBuffer {
	unsigned int m_RendererID;
	unsigned int m_Count;

public:
	IndexBuffer(const unsigned int* data, unsigned int count);
    IndexBuffer(const unsigned short* data, unsigned int count);
	~IndexBuffer();

	void Bind() const;
	void UnBind() const;

	inline unsigned int GetCount() const { return m_Count; }
};


#endif