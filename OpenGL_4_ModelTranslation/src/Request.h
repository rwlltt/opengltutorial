#ifndef TUT_REQUEST_
#define TUT_REQUEST_

#include <string>
#include "loki/Threads.h"
#include <thread>
#include <atomic>
#include <map>

using namespace Loki;
class Response;

class Environment 
{
	std::string folder_;
public:
	Environment();
	virtual ~Environment();
	/* Default copy assignment cstrs */
	friend class Application;
};

class User : public ObjectLevelLockable<User>
{
	std::string name_;
	bool shall_stop_{ false };
	bool has_stopped_{ true };
	std::atomic<bool> shall_prompt_{ false };
	std::map<std::string, std::string> commands_;
	std::string prompt_text_{ "" };
	std::unique_ptr<Response> res_;

public:
	//	User(const User& other) = delete;
//	User& operator= (const User &other) = delete;
//	User* operator&(User object) = delete;

public:
	User(const std::string &name = "anonymous");
	virtual ~User();

public:
	void Commands(const std::string prompt, const std::map<std::string, std::string> commands, bool shall_prompt) {
		Lock lock(this);
		prompt_text_ = prompt;
		commands_ = commands;
		shall_prompt_ = shall_prompt;
	}
	void SetResponse(std::unique_ptr<Response> res) {
		Lock lock(this);
		res_ = std::move(res);
		shall_prompt_ = true;
	}

	void OnUpdate(float deltaTime);
	void OnRender();
	void OnImGuiRender();


};

class Request
{
	std::string user_name_;
	Environment env_;

public:
	Request(const std::string &user_name, const Environment &env);
	virtual ~Request();
	/* Default copy assignment cstrs */
	
	const Environment &RequestEnvironment() {
		return env_;
	}
	void SetEnvironment(const Environment &env) {
		env_ = env;
	}
	const std::string  &RequestUser() {
		return user_name_;
	}
	void SetUser(const std::string &user_name) {
		user_name_ = user_name;
	}

	virtual std::unique_ptr<Response> DoRequest(const std::string &user) = 0;
};

#endif /* TUT_REQUEST_*/