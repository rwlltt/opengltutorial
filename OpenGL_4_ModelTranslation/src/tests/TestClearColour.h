#ifndef TUT_TEST_CLEARCOLOUR_
#define TUT_TEST_CLEARCOLOUR_

#include "Test.h"

namespace test {

	class TestClearColour : public Test {
	private:
		float m_clearColour[4];

	public:
		TestClearColour();
		~TestClearColour();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;

	};


}

#endif /* TUT_TEST_CLEARCOLOUR_*/

