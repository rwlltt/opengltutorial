//
// Created by rodney on 10/05/2020.
//

#ifndef OPENGL_TUT_MODELTRANSLATION_WEDGEINSTANCE_H
#define OPENGL_TUT_MODELTRANSLATION_WEDGEINSTANCE_H

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>


class WedgeInstance {
public:
    WedgeInstance() {};
    virtual ~WedgeInstance() {};
    virtual glm::vec3 OffsetXYZ(float fElapsedTime);

};


#endif //OPENGL_TUT_MODELTRANSLATION_WEDGEINSTANCE_H
