#include "WedgeBaseIndex.h"
#include <Renderer.h>
#include "imgui/imgui.h"
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "StationaryWedge.h"
#include "OvalWedge.h"
#include "BottomCircleWedge.h"

#define ARRAY_COUNT( array ) (sizeof( array ) / (sizeof( array[0] ) * (sizeof( array ) != sizeof(void*) || sizeof( array[0] ) <= sizeof(void*))))
#define GREEN_COLOR 0.75f, 0.75f, 1.0f, 1.0f
#define BLUE_COLOR 	0.0f, 0.5f, 0.0f, 1.0f
#define RED_COLOR 1.0f, 0.0f, 0.0f, 1.0f
#define GREY_COLOR 0.8f, 0.8f, 0.8f, 1.0f
#define BROWN_COLOR 0.5f, 0.5f, 0.0f, 1.0f

namespace test {


    WedgeBaseIndex::WedgeBaseIndex()
		: vertexData {
                    +1.0f, +1.0f, +1.0f,
                    -1.0f, -1.0f, +1.0f,
                    -1.0f, +1.0f, -1.0f,
                    +1.0f, -1.0f, -1.0f,

                    -1.0f, -1.0f, -1.0f,
                    +1.0f, +1.0f, -1.0f,
                    +1.0f, -1.0f, +1.0f,
                    -1.0f, +1.0f, +1.0f,

                    GREEN_COLOR,
                    BLUE_COLOR,
                    RED_COLOR,
                    BROWN_COLOR,

                    GREEN_COLOR,
                    BLUE_COLOR,
                    RED_COLOR,
                    BROWN_COLOR,
            }, indexData {
                    0, 1, 2,
                    1, 0, 3,
                    2, 3, 0,
                    3, 2, 1,

                    5, 4, 6,
                    4, 5, 7,
                    7, 6, 4,
                    6, 7, 5,
            },
          shader {"res/shaders/WedgeCube.shader"},
          vb {vertexData, 56 * sizeof(vertexData[0]), false },
          ib {indexData, 24},
          numberOfVertices{8},
          wedgeInstances{std::make_unique<StationaryWedge>(), std::make_unique<OvalWedge>(), std::make_unique<BottomCircleWedge>() },
          translationA {1.0, 1.0, -1.0},
          translationB {1.0, 1.0, -1.2}

    {
        size_t colorDataOffset = sizeof(float) * 3 * numberOfVertices;
        vb.Bind(); // set the GL_ARRAY_BUFFER binding once

        va.Bind(); // element buffer binding is part of the VAO state,
        GLCall(glEnableVertexAttribArray(0));
        GLCall(glEnableVertexAttribArray(1));
        GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0));
        GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)colorDataOffset));
        ib.Bind();  //  GL_ELEMENT_ARRAY_BUFFER is set for each VAO
        va.UnBind();
	}

    WedgeBaseIndex::~WedgeBaseIndex()
	{
	}

	void WedgeBaseIndex::OnUpdate(float deltaTime)
	{
        // Store the delta
        this->deltaTime = deltaTime;
        //ComputePositionOffsets(deltaTime);
	}

	void WedgeBaseIndex::OnRender()
	{
        shader.Bind();
        va.Bind();

        //float fElapsedTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;
        for(int iLoop = 0; iLoop < ARRAY_COUNT(wedgeInstances); iLoop++)
        {
            ////
//            const float fLoopDuration = 12.0f;
//            const float fScale = 3.14159f * 2.0f / fLoopDuration;
//
//            float fCurrTimeThroughLoop = fmodf(deltaTime, fLoopDuration);
//
//            glm::vec3 tmp = glm::vec3(cosf(fCurrTimeThroughLoop * fScale) * 5.f,
//                             -3.5f,
//                             sinf(fCurrTimeThroughLoop * fScale) * 5.f - 20.0f);
            //////
            auto currInst = wedgeInstances[iLoop].get();
            glm::mat4 theMat(1.0f);
            //theMat[3] = glm::vec4(tmp, 1.0f);

            theMat[3] = glm::vec4(currInst->OffsetXYZ(deltaTime), 1.0f);
            //const glm::mat4 &transformMatrix = theMat;
            //std::cout << theMat[0][0] << " " << deltaTime << "\n";
            shader.SetUniformMat4f("modelToCameraMatrix", theMat);
            GLCall(glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0));
        }

//        shader.SetUniform3f("offset", translationA.x, translationA.y, translationA.z );
//        GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, 0));

        va.UnBind();
	}

	void WedgeBaseIndex::OnImGuiRender()
	{
        ImGui::SliderFloat3("Offset 1", &translationA.x, -3.0f, 2.0f);
        ImGui::SliderFloat3("Offset 2", &translationB.x, -3.0f, 2.0f);
	}

    void WedgeBaseIndex::ComputePositionOffsets(float fElapsedTime)
    {
        const float fLoopDuration = 5.0f;
        const float fScale = 3.14159f * 2.0f / fLoopDuration;

        // glutGet is replaced with deltatime calc with std chrono class
        //float fElapsedTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;

        float fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);

        //fXOffset = cosf(fCurrTimeThroughLoop * fScale) * 0.75f;
        //fYOffset = sinf(fCurrTimeThroughLoop * fScale) * 0.75f;
        //fprintf(stderr, "ComputePositionOffsets %f %f %f\n", fXOffset, fYOffset, fCurrTimeThroughLoop );
    }

    void WedgeBaseIndex::AdjustVertexData()
    {
        std::vector<float> fNewData(ARRAY_COUNT(vertexData));
        memcpy(&fNewData[0], vertexData, sizeof(vertexData));

        for(int iVertex = 0; iVertex < ARRAY_COUNT(vertexData); iVertex += 4)
        {
            //fNewData[iVertex] += fXOffset;
            //fNewData[iVertex + 1] += fYOffset;
        }

        vb.AdjustVertexData(&fNewData[0], sizeof(vertexData));
    }
}