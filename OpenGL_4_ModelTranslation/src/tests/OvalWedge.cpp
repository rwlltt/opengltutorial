//
// Created by rodney on 10/05/2020.
//

#include "OvalWedge.h"

glm::vec3 OvalWedge::OffsetXYZ(float fElapsedTime) {
    const float fLoopDuration = 3.0f;
    const float fScale = 3.14159f * 2.0f / fLoopDuration;

    float fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);

    return glm::vec3(cosf(fCurrTimeThroughLoop * fScale) * 4.f,
                     sinf(fCurrTimeThroughLoop * fScale) * 6.f,
                     -20.0f);
}
