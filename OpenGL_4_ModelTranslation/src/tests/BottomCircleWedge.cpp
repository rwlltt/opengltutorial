//
// Created by rodney on 10/05/2020.
//

#include "BottomCircleWedge.h"

glm::vec3 BottomCircleWedge::OffsetXYZ(float fElapsedTime) {
    const float fLoopDuration = 12.0f;
    const float fScale = 3.14159f * 2.0f / fLoopDuration;

    float fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);

    return glm::vec3(cosf(fCurrTimeThroughLoop * fScale) * 5.f,
                     -3.5f,
                     sinf(fCurrTimeThroughLoop * fScale) * 5.f - 20.0f);
}
