//
// Created by rodney on 10/05/2020.
//

#include <iostream>
#include "StationaryWedge.h"

glm::vec3 StationaryWedge::OffsetXYZ(float fElapsedTime) {
    //std::cout << "StationaryWedge offset\n";
    return glm::vec3(0.0f, 0.0f, -20.0f);
}
