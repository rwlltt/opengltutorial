#ifndef MODULE_DEFINE_H
#define MODULE_DEFINE_H

#include <vector>
#include <set>

namespace Module
{


extern const char *dbVersion;
extern const char *dbConfigXml; ///< Module db config xml
extern const int dbNumberOfTables; 
extern const char* dbTableNames[];


}


#endif /* MODULE_DEFINE_H */
