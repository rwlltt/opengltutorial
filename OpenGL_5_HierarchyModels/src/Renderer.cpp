
#include "Renderer.h"
#include <iostream>
#include <glm/gtc/type_ptr.hpp>


void  GLClearError() {
	while (glGetError() != GL_NO_ERROR);
}

bool GLLogCall(const char* function, const char* file, int line) {
	while (GLenum error = glGetError()) {
		std::cout << "[OpenGl Error](" << std::hex << error << std::dec << "): " <<
			function << " " << file << ":" << line << '\n';
		return false;
	}
	return true;
}

Renderer::Renderer(): fFrustumScale{CalcFrustumScale(45.0f)}, cameraToClipMatrix{0.0f} {
    float fzNear = 1.0f; float fzFar = 100.0f;
    cameraToClipMatrix[0].x = fFrustumScale;
    cameraToClipMatrix[1].y = fFrustumScale;
    cameraToClipMatrix[2].z = (fzFar + fzNear) / (fzNear - fzFar);
    cameraToClipMatrix[2].w = -1.0f;
    cameraToClipMatrix[3].z = (2 * fzFar * fzNear) / (fzNear - fzFar);
}

Renderer::~Renderer() {

}

void Renderer::ViewPort(int width, int height)
{
	// Change to the view port with perspective aspect in shader perperpective
    cameraToClipMatrix[0].x = fFrustumScale * (height / (float)width);
    cameraToClipMatrix[1].y = fFrustumScale;
    // Set all registered shaders uniform camera to clip matrix.
    for (auto shader1 : shaders) {
        shader1->Bind();
        shader1->SetUniformMat4f("cameraToClipMatrix", cameraToClipMatrix);
        shader1->UnBind();
    }
    GLCall(glViewport(0, 0, (GLsizei)width, (GLsizei)height));
}

void Renderer::Clear() const
{
    GLCall(glClearColor(0.0f, 0.0f, 0.0f, 0.0f));
    GLCall(glClearDepth(1.0f));
	GLCall(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
}

void Renderer::Draw(const VertexArray & va, const IndexBuffer & ib, const Shader & shader) const
{
	shader.Bind();
	va.Bind();
	ib.Bind();
	GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));

}

void Renderer::RegisterShader(Shader* shader) {
    shaders.push_back(shader);
}

float Renderer::CalcFrustumScale(float fFovDeg)
{
    const float degToRad = 3.14159f * 2.0f / 360.0f;
    float fFovRad = fFovDeg * degToRad;
    return 1.0f / tan(fFovRad / 2.0f);
}

