#ifndef TUT_TEST_JEWELTRANSLATION
#define TUT_TEST_JEWELTRANSLATION

#include <Shader.h>
#include <VertexBuffer.h>
#include <VertexArray.h>
#include <VertexBufferLayout.h>
#include "Test.h"
#include "WedgeInstance.h"

namespace test {

	class JewelTranslation : public Test {
	private:
        float vertexData[56];
        GLushort indexData[24];
        Shader shader;
        VertexBuffer vb;
        VertexArray va;
        //VertexArray va2;
        VertexBufferLayout layout;
        IndexBuffer ib;
        int numberOfVertices;
        float deltaTime = 0.0f;
        std::unique_ptr<WedgeInstance> wedgeInstances[3];
        glm::vec3 translationA;
        glm::vec3 translationB;
        Geometry geometry;

    public:
        JewelTranslation();
		~JewelTranslation();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;
		Shader &GetShader() { return shader;};
      //  VertexArray &GetVertexArray() { return va;};
      //  IndexBuffer &GetIndexBuffer() { return ib;};

    };


}

#endif /* TUT_TEST_JEWELTRANSLATION*/

