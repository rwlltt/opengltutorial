//
// Created by rodney on 10/05/2020.
//

#ifndef OPENGL_TUT_MODELTRANSLATION_WEDGEINSTANCE_H
#define OPENGL_TUT_MODELTRANSLATION_WEDGEINSTANCE_H

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <Geometry.h>


class WedgeInstance {
protected:
    Geometry geometry;
    glm::vec3 offset;

public:
    WedgeInstance(glm::vec3 offset);
    virtual ~WedgeInstance() {};
    virtual glm::vec3 OffsetXYZ(float fElapsedTime);
    virtual glm::vec3 CalcScale(float fElapsedTime);
    virtual glm::mat3 CalcRotation(float fElapsedTime);
    virtual glm::vec3 getOffset();

};


#endif //OPENGL_TUT_MODELTRANSLATION_WEDGEINSTANCE_H
