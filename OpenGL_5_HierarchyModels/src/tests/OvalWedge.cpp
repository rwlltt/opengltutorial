//
// Created by rodney on 10/05/2020.
//

#include <iostream>
#include "OvalWedge.h"

OvalWedge::OvalWedge(glm::vec3 offset):WedgeInstance(offset) {

}

glm::vec3 OvalWedge::OffsetXYZ(float fElapsedTime) {
    const float fLoopDuration = 3.0f;
    const float fScale = 3.14159f * 2.0f / fLoopDuration;

    float fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);

    glm::vec3 offset = glm::vec3(cosf(fCurrTimeThroughLoop * fScale) * 4.0f,
                     sinf(fCurrTimeThroughLoop * fScale) * 6.0f,
                     -20.0f);
 //   std::cout << "OvalWedge offset " << offset.x << " " << offset.y << " " << offset.z << "\n";
    return offset;
}

glm::mat3 OvalWedge::CalcRotation(float fElapsedTime){
    const float fLoopDuration = 3.0f;
    float fAngRad = geometry.ComputeAngleRad(fElapsedTime, fLoopDuration);
    float fCos = cosf(fAngRad);
    float fSin = sinf(fAngRad) ;
    float fCosNeg = cosf(-fAngRad);
    float fSinNeg = sinf(-fAngRad) ;

    glm::mat3 theMat(1.0f);
    theMat[0].x = fCos; theMat[1].x = -fSin;
    theMat[0].y = fSin; theMat[1].y = fCos;

    glm::mat3 theMat2(1.0f);
    theMat2[0].x = fCosNeg; theMat2[1].x = -fSinNeg;
    theMat2[0].y = fSinNeg; theMat2[1].y = fCosNeg;

    return theMat;//*theMat2;
    //    glm::mat3 rotation;
//    float fAngRad = geometry.ComputeAngleRad(fElapsedTime, 3.0);
//    float fCos = cosf(fAngRad);
//    float fSin = sinf(fAngRad);
//
//    glm::mat3 theMat(1.0f);
//    theMat[1].y = fCos; theMat[2].y = -fSin;
//    theMat[1].z = fSin; theMat[2].z = fCos;
//    rotation = theMat;

//            break;
//        }
//        case 3: {
//            float fAngRad = ComputeAngleRad(fElapsedTime, 2.0);
//            float fCos = cosf(fAngRad);
//            float fSin = sinf(fAngRad);
//
//            glm::mat3 theMat(1.0f);
//            theMat[0].x = fCos; theMat[2].x = fSin;
//            theMat[0].z = -fSin; theMat[2].z = fCos;
//            rotation = theMat;
//            break;
//        }
//        case 4: {
//            float fAngRad = ComputeAngleRad(fElapsedTime, 2.0);
//            float fCos = cosf(fAngRad);
//            float fSin = sinf(fAngRad);
//
//            glm::mat3 theMat(1.0f);
//            theMat[0].x = fCos; theMat[1].x = -fSin;
//            theMat[0].y = fSin; theMat[1].y = fCos;
//            rotation = theMat;
//            break;
//        }
//        case 5: {
//            float fAngRad = ComputeAngleRad(fElapsedTime, 2.0);
//            float fCos = cosf(fAngRad);
//            float fInvCos = 1.0f - fCos;
//            float fSin = sinf(fAngRad);
//            float fInvSin = 1.0f - fSin;
//
//            glm::vec3 axis(1.0f, 1.0f, 1.0f);
//            axis = glm::normalize(axis);
//
//            glm::mat3 theMat(1.0f);
//            theMat[0].x = (axis.x * axis.x) + ((1 - axis.x * axis.x) * fCos);
//            theMat[1].x = axis.x * axis.y * (fInvCos) - (axis.z * fSin);
//            theMat[2].x = axis.x * axis.z * (fInvCos) + (axis.y * fSin);
//
//            theMat[0].y = axis.x * axis.y * (fInvCos) + (axis.z * fSin);
//            theMat[1].y = (axis.y * axis.y) + ((1 - axis.y * axis.y) * fCos);
//            theMat[2].y = axis.y * axis.z * (fInvCos) - (axis.x * fSin);
//
//            theMat[0].z = axis.x * axis.z * (fInvCos) - (axis.y * fSin);
//            theMat[1].z = axis.y * axis.z * (fInvCos) + (axis.x * fSin);
//            theMat[2].z = (axis.z * axis.z) + ((1 - axis.z * axis.z) * fCos);
//            rotation = theMat;
//        }
//        default:
//            // nothing
//            break;
//    }
}
