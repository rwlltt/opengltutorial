//
// Created by rodney on 10/05/2020.
//

#ifndef OPENGL_TUT_MODELTRANSLATION_BOTTOMCIRCLEWEDGE_H
#define OPENGL_TUT_MODELTRANSLATION_BOTTOMCIRCLEWEDGE_H


#include "WedgeInstance.h"

class BottomCircleWedge : public WedgeInstance {
public:
    BottomCircleWedge(glm::vec3 offset);
    glm::vec3 OffsetXYZ(float fElapsedTime) override;
    glm::mat3 CalcRotation(float fElapsedTime);
};


#endif //OPENGL_TUT_MODELTRANSLATION_BOTTOMCIRCLEWEDGE_H
