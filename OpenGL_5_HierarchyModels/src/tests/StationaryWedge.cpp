//
// Created by rodney on 10/05/2020.
//

#include <iostream>
#include "StationaryWedge.h"

StationaryWedge::StationaryWedge(glm::vec3 offset):WedgeInstance(offset) {

}

glm::vec3 StationaryWedge::OffsetXYZ(float fElapsedTime) {
    return offset;
}

glm::mat3 StationaryWedge::CalcRotation(float fElapsedTime){
    glm::mat3 rotation;
    rotation = glm::mat3(1.0f);
    return rotation;
}
