#include "Armature.h"
#include <Renderer.h>
#include "imgui/imgui.h"
#include <iostream>

#define ARRAY_COUNT( array ) (sizeof( array ) / (sizeof( array[0] ) * (sizeof( array ) != sizeof(void*) || sizeof( array[0] ) <= sizeof(void*))))
#define RED_COLOR 1.0f, 0.0f, 0.0f, 1.0f
#define GREEN_COLOR 0.0f, 1.0f, 0.0f, 1.0f
#define BLUE_COLOR 	0.0f, 0.0f, 1.0f, 1.0f

#define YELLOW_COLOR 1.0f, 1.0f, 0.0f, 1.0f
#define CYAN_COLOR 0.0f, 1.0f, 1.0f, 1.0f
#define MAGENTA_COLOR 	1.0f, 0.0f, 1.0f, 1.0f

#define STANDARD_ANGLE_INCREMENT 11.25f
#define SMALL_ANGLE_INCREMENT 9.0f

namespace test {

    Armature::Armature():
        posBase(glm::vec3(-3.0f, -5.0f, -40.0f))
        , angBase(-45.0f)
        , posBaseLeft(glm::vec3(2.0f, 0.0f, 0.0f))
        , posBaseRight(glm::vec3(-2.0f, 0.0f, 0.0f))
        , scaleBaseZ(3.0f)
        , angUpperArm(-33.75f)
        , sizeUpperArm(9.0f)
        , posLowerArm(glm::vec3(0.0f, 0.0f, 8.0f))
        , angLowerArm(146.25f)
        , lenLowerArm(5.0f)
        , widthLowerArm(1.5f)
        , posWrist(glm::vec3(0.0f, 0.0f, 5.0f))
        , angWristRoll(0.0f)
        , angWristPitch(67.5f)
        , lenWrist(2.0f)
        , widthWrist(2.0f)
        , posLeftFinger(glm::vec3(1.0f, 0.0f, 1.0f))
        , posRightFinger(glm::vec3(-1.0f, 0.0f, 1.0f))
        , angFingerOpen(180.0f)
        , lenFinger(2.0f)
        , widthFinger(0.5f)
        , angLowerFinger(45.0f),
		vertexData {
                    //Front
                    +1.0f, +1.0f, +1.0f,
                    +1.0f, -1.0f, +1.0f,
                    -1.0f, -1.0f, +1.0f,
                    -1.0f, +1.0f, +1.0f,

                    //Top
                    +1.0f, +1.0f, +1.0f,
                    -1.0f, +1.0f, +1.0f,
                    -1.0f, +1.0f, -1.0f,
                    +1.0f, +1.0f, -1.0f,

                    //Left
                    +1.0f, +1.0f, +1.0f,
                    +1.0f, +1.0f, -1.0f,
                    +1.0f, -1.0f, -1.0f,
                    +1.0f, -1.0f, +1.0f,

                    //Back
                    +1.0f, +1.0f, -1.0f,
                    -1.0f, +1.0f, -1.0f,
                    -1.0f, -1.0f, -1.0f,
                    +1.0f, -1.0f, -1.0f,

                    //Bottom
                    +1.0f, -1.0f, +1.0f,
                    +1.0f, -1.0f, -1.0f,
                    -1.0f, -1.0f, -1.0f,
                    -1.0f, -1.0f, +1.0f,

                    //Right
                    -1.0f, +1.0f, +1.0f,
                    -1.0f, -1.0f, +1.0f,
                    -1.0f, -1.0f, -1.0f,
                    -1.0f, +1.0f, -1.0f,


                    GREEN_COLOR,
                    GREEN_COLOR,
                    GREEN_COLOR,
                    GREEN_COLOR,

                    BLUE_COLOR,
                    BLUE_COLOR,
                    BLUE_COLOR,
                    BLUE_COLOR,

                    RED_COLOR,
                    RED_COLOR,
                    RED_COLOR,
                    RED_COLOR,

                    YELLOW_COLOR,
                    YELLOW_COLOR,
                    YELLOW_COLOR,
                    YELLOW_COLOR,

                    CYAN_COLOR,
                    CYAN_COLOR,
                    CYAN_COLOR,
                    CYAN_COLOR,

                    MAGENTA_COLOR,
                    MAGENTA_COLOR,
                    MAGENTA_COLOR,
                    MAGENTA_COLOR,
            }, indexData {
                0, 1, 2,
                2, 3, 0,

                4, 5, 6,
                6, 7, 4,

                8, 9, 10,
                10, 11, 8,

                12, 13, 14,
                14, 15, 12,

                16, 17, 18,
                18, 19, 16,

                20, 21, 22,
                22, 23, 20,
            },
          shader {"res/shaders/Armature.shader"},
          vb {vertexData, 168 * sizeof(vertexData[0]), false },
          ib {indexData, 36},
          numberOfVertices{24},
//          wedgeInstances{std::make_unique<SampleWedge>(glm::vec3(0.0f, 0.0f, -25.0f), 1),
//                         std::make_unique<SampleWedge>(glm::vec3(-5.0f, -5.0f, -25.0f), 2),
//                         std::make_unique<SampleWedge>(glm::vec3(-5.0f, 5.0f, -25.0f), 3),
//                         std::make_unique<SampleWedge>(glm::vec3(5.0f, 5.0f, -25.0f), 4),
//                         std::make_unique<SampleWedge>(glm::vec3(5.0f, -5.0f, -25.0f), 5)  },
          translationA {1.0, 1.0, -1.0},
          translationB {1.0, 1.0, -1.2}

    {
        size_t colorDataOffset = sizeof(float) * 3 * numberOfVertices;
        vb.Bind(); // set the GL_ARRAY_BUFFER binding once

        va.Bind(); // element buffer binding is part of the VAO state,
        GLCall(glEnableVertexAttribArray(0));
        GLCall(glEnableVertexAttribArray(1));
        GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0));
        GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)colorDataOffset));
        ib.Bind();  //  GL_ELEMENT_ARRAY_BUFFER is set for each VAO
        va.UnBind();
	}

    Armature::~Armature()
	{
	}

	void Armature::OnUpdate(float deltaTime)
	{
        // Store the delta
        this->deltaTime = deltaTime;
        //ComputePositionOffsets(deltaTime);
	}

	void Armature::OnRender()
	{
        MatrixStack modelToCameraStack;

        shader.Bind();
        va.Bind();

        modelToCameraStack.Translate(posBase);
        modelToCameraStack.RotateY(angBase);

        //Draw left base.
        {
            modelToCameraStack.Push();
            modelToCameraStack.Translate(posBaseLeft);
            modelToCameraStack.Scale(glm::vec3(1.0f, 1.0f, scaleBaseZ));

            //glUniformMatrix4fv(modelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr(modelToCameraStack.Top()));
            glm::mat4 top = modelToCameraStack.Top();
            shader.SetUniformMat4f("modelToCameraMatrix", top);

            GLCall(glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0));
            //glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0);
            modelToCameraStack.Pop();
        }

        //Draw right base.
        {
            modelToCameraStack.Push();
            modelToCameraStack.Translate(posBaseRight);
            modelToCameraStack.Scale(glm::vec3(1.0f, 1.0f, scaleBaseZ));

            //glUniformMatrix4fv(modelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr(modelToCameraStack.Top()));
            glm::mat4 top = modelToCameraStack.Top();
            shader.SetUniformMat4f("modelToCameraMatrix", top);
            GLCall(glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0));
            //glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0);
            modelToCameraStack.Pop();
        }

        //Draw main arm.
        DrawUpperArm(modelToCameraStack);

        //glBindVertexArray(0);
        va.UnBind();
        shader.UnBind();
        //glUseProgram(0);

	}

	void Armature::OnImGuiRender()
	{
        ImGui::SliderFloat3("Offset 1", &translationA.x, -3.0f, 2.0f);
        ImGui::SliderFloat3("Offset 2", &translationB.x, -3.0f, 2.0f);
	}

    // Keyboards
    void Armature::AdjBase(bool bIncrement)
    {
        angBase += bIncrement ? STANDARD_ANGLE_INCREMENT : -STANDARD_ANGLE_INCREMENT;
        angBase = fmodf(angBase, 360.0f);
    }

    void Armature::AdjUpperArm(bool bIncrement)
    {
        angUpperArm += bIncrement ? STANDARD_ANGLE_INCREMENT : -STANDARD_ANGLE_INCREMENT;
        angUpperArm = geometry.Clamp(angUpperArm, -90.0f, 0.0f);
    }

    void Armature::AdjLowerArm(bool bIncrement)
    {
        angLowerArm += bIncrement ? STANDARD_ANGLE_INCREMENT : -STANDARD_ANGLE_INCREMENT;
        angLowerArm = geometry.Clamp(angLowerArm, 0.0f, 146.25f);
    }

    void Armature::AdjWristPitch(bool bIncrement)
    {
        angWristPitch += bIncrement ? STANDARD_ANGLE_INCREMENT : -STANDARD_ANGLE_INCREMENT;
        angWristPitch = geometry.Clamp(angWristPitch, 0.0f, 90.0f);
    }

    void Armature::AdjWristRoll(bool bIncrement)
    {
        angWristRoll += bIncrement ? STANDARD_ANGLE_INCREMENT : -STANDARD_ANGLE_INCREMENT;
        angWristRoll = fmodf(angWristRoll, 360.0f);
    }

    void Armature::AdjFingerOpen(bool bIncrement)
    {
        angFingerOpen += bIncrement ? SMALL_ANGLE_INCREMENT : -SMALL_ANGLE_INCREMENT;
        angFingerOpen = geometry.Clamp(angFingerOpen, 9.0f, 180.0f);
    }

    void Armature::WritePose()
    {
        printf("angBase:\t%f\n", angBase);
        printf("angUpperArm:\t%f\n", angUpperArm);
        printf("angLowerArm:\t%f\n", angLowerArm);
        printf("angWristPitch:\t%f\n", angWristPitch);
        printf("angWristRoll:\t%f\n", angWristRoll);
        printf("angFingerOpen:\t%f\n", angFingerOpen);
        printf("\n");
    }


    // Private drawing

    void Armature::DrawFingers(MatrixStack &modelToCameraStack)
    {
        //Draw left finger
        modelToCameraStack.Push();
        modelToCameraStack.Translate(posLeftFinger);
        modelToCameraStack.RotateY(angFingerOpen);

        modelToCameraStack.Push();
        modelToCameraStack.Translate(glm::vec3(0.0f, 0.0f, lenFinger / 2.0f));
        modelToCameraStack.Scale(glm::vec3(widthFinger / 2.0f, widthFinger/ 2.0f, lenFinger / 2.0f));

        glm::mat4 top = modelToCameraStack.Top();
        shader.SetUniformMat4f("modelToCameraMatrix", top);
        GLCall(glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0));
        modelToCameraStack.Pop();

        {
            //Draw left lower finger
            modelToCameraStack.Push();
            modelToCameraStack.Translate(glm::vec3(0.0f, 0.0f, lenFinger));
            modelToCameraStack.RotateY(-angLowerFinger);

            modelToCameraStack.Push();
            modelToCameraStack.Translate(glm::vec3(0.0f, 0.0f, lenFinger / 2.0f));
            modelToCameraStack.Scale(glm::vec3(widthFinger / 2.0f, widthFinger/ 2.0f, lenFinger / 2.0f));
            glm::mat4 top = modelToCameraStack.Top();
            shader.SetUniformMat4f("modelToCameraMatrix", top);
            GLCall(glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0));
            modelToCameraStack.Pop();

            modelToCameraStack.Pop();
        }

        modelToCameraStack.Pop();

        //Draw right finger
        modelToCameraStack.Push();
        modelToCameraStack.Translate(posRightFinger);
        modelToCameraStack.RotateY(-angFingerOpen);

        modelToCameraStack.Push();
        modelToCameraStack.Translate(glm::vec3(0.0f, 0.0f, lenFinger / 2.0f));
        modelToCameraStack.Scale(glm::vec3(widthFinger / 2.0f, widthFinger/ 2.0f, lenFinger / 2.0f));
        top = modelToCameraStack.Top();
        shader.SetUniformMat4f("modelToCameraMatrix", top);
        GLCall(glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0));
        modelToCameraStack.Pop();

        {
            //Draw right lower finger
            modelToCameraStack.Push();
            modelToCameraStack.Translate(glm::vec3(0.0f, 0.0f, lenFinger));
            modelToCameraStack.RotateY(angLowerFinger);

            modelToCameraStack.Push();
            modelToCameraStack.Translate(glm::vec3(0.0f, 0.0f, lenFinger / 2.0f));
            modelToCameraStack.Scale(glm::vec3(widthFinger / 2.0f, widthFinger/ 2.0f, lenFinger / 2.0f));
            glm::mat4 top = modelToCameraStack.Top();
            shader.SetUniformMat4f("modelToCameraMatrix", top);
            GLCall(glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0));
            modelToCameraStack.Pop();

            modelToCameraStack.Pop();
        }

        modelToCameraStack.Pop();
    }

    void Armature::DrawWrist(MatrixStack &modelToCameraStack)
    {
        modelToCameraStack.Push();
        modelToCameraStack.Translate(posWrist);
        // Reverse the two rotations
        modelToCameraStack.RotateX(angWristPitch);
        modelToCameraStack.RotateZ(angWristRoll);

        modelToCameraStack.Push();
        modelToCameraStack.Scale(glm::vec3(widthWrist / 2.0f, widthWrist/ 2.0f, lenWrist / 2.0f));
        glm::mat4 top = modelToCameraStack.Top();
        shader.SetUniformMat4f("modelToCameraMatrix", top);
        GLCall(glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0));
        modelToCameraStack.Pop();

        DrawFingers(modelToCameraStack);

        modelToCameraStack.Pop();
    }

    void Armature::DrawLowerArm(MatrixStack &modelToCameraStack)
    {
        modelToCameraStack.Push();
        modelToCameraStack.Translate(posLowerArm);
        modelToCameraStack.RotateX(angLowerArm);

        modelToCameraStack.Push();
        modelToCameraStack.Translate(glm::vec3(0.0f, 0.0f, lenLowerArm / 2.0f));
        modelToCameraStack.Scale(glm::vec3(widthLowerArm / 2.0f, widthLowerArm / 2.0f, lenLowerArm / 2.0f));
        glm::mat4 top = modelToCameraStack.Top();
        shader.SetUniformMat4f("modelToCameraMatrix", top);
        GLCall(glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0));
        modelToCameraStack.Pop();

        DrawWrist(modelToCameraStack);

        modelToCameraStack.Pop();
    }

    void Armature::DrawUpperArm(MatrixStack &modelToCameraStack)
    {
        modelToCameraStack.Push();
        modelToCameraStack.RotateX(angUpperArm);

        {
            modelToCameraStack.Push();
            modelToCameraStack.Translate(glm::vec3(0.0f, 0.0f, (sizeUpperArm / 2.0f) - 1.0f));
            modelToCameraStack.Scale(glm::vec3(1.0f, 1.0f, sizeUpperArm / 2.0f));
            glm::mat4 top = modelToCameraStack.Top();
            shader.SetUniformMat4f("modelToCameraMatrix", top);
            GLCall(glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0));
            modelToCameraStack.Pop();
        }

        DrawLowerArm(modelToCameraStack);

        modelToCameraStack.Pop();
    }

}