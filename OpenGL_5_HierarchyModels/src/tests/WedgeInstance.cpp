//
// Created by rodney on 10/05/2020.
//

#include "WedgeInstance.h"
WedgeInstance::WedgeInstance(glm::vec3 offset):offset{offset} {}

glm::vec3 WedgeInstance::OffsetXYZ(float fElapsedTime) {
    return glm::vec3();
}

glm::vec3 WedgeInstance::CalcScale(float fElapsedTime) {
    return glm::vec3();
}

glm::mat3 WedgeInstance::CalcRotation(float fElapsedTime) {
    return glm::mat3();
}

glm::vec3 WedgeInstance::getOffset() {
    return offset;
}

