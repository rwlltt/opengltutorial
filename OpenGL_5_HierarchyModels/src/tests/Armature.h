#ifndef TUT_TEST_ARMATURE
#define TUT_TEST_ARMATURE

#include <Shader.h>
#include <VertexBuffer.h>
#include <VertexArray.h>
#include <VertexBufferLayout.h>
#include <MatrixStack.h>
#include "Test.h"
#include "WedgeInstance.h"

namespace test {

	class Armature : public Test {
	private:
        glm::vec3		posBase;
        float			angBase;

        glm::vec3		posBaseLeft, posBaseRight;
        float			scaleBaseZ;

        float			angUpperArm;
        float			sizeUpperArm;

        glm::vec3		posLowerArm;
        float			angLowerArm;
        float			lenLowerArm;
        float			widthLowerArm;

        glm::vec3		posWrist;
        float			angWristRoll;
        float			angWristPitch;
        float			lenWrist;
        float			widthWrist;

        glm::vec3		posLeftFinger, posRightFinger;
        float			angFingerOpen;
        float			lenFinger;
        float			widthFinger;
        float			angLowerFinger;

        float vertexData[168];
        GLushort indexData[36];
        Shader shader;
        VertexBuffer vb;
        VertexArray va;
        //VertexArray va2;
        VertexBufferLayout layout;
        IndexBuffer ib;
        int numberOfVertices;
        float deltaTime = 0.0f;
        std::unique_ptr<WedgeInstance> wedgeInstances[5];
        glm::vec3 translationA;
        glm::vec3 translationB;
        Geometry geometry;

    public:
        Armature();
		~Armature();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;
		Shader & UpdateShader() { return shader;};

        void AdjBase(bool bIncrement);
        void AdjUpperArm(bool bIncrement);
        void AdjLowerArm(bool bIncrement);
        void AdjWristPitch(bool bIncrement);
        void AdjWristRoll(bool bIncrement);
        void AdjFingerOpen(bool bIncrement);
        void WritePose();

    private:
        void DrawFingers(MatrixStack &modelToCameraStack);
        void DrawWrist(MatrixStack &modelToCameraStack);
        void DrawLowerArm(MatrixStack &modelToCameraStack);
        void DrawUpperArm(MatrixStack &modelToCameraStack);


    };


}

#endif /* TUT_TEST_ARMATURE*/

