//
// Created by rodney on 10/05/2020.
//

#include <iostream>
#include "BottomCircleWedge.h"

BottomCircleWedge::BottomCircleWedge(glm::vec3 offset):WedgeInstance(offset) {

}

glm::vec3 BottomCircleWedge::OffsetXYZ(float fElapsedTime) {
    const float fLoopDuration = 12.0f;
    const float fScale = 3.14159f * 2.0f / fLoopDuration;

    float fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);

    glm::vec3 offset =  glm::vec3(cosf(fCurrTimeThroughLoop * fScale) * 5.f,
                                  -3.5f,
                                  sinf(fCurrTimeThroughLoop * fScale) * 5.f - 20.0f);
//    std::cout << "BottomCircleWedge offset " << offset.x << " " << offset.y << " " << offset.z << "\n";
    return offset;
}


glm::mat3 BottomCircleWedge::CalcRotation(float fElapsedTime){
    const float fLoopDuration = 12.0f;
    float fAngRad = geometry.ComputeAngleRad(fElapsedTime, fLoopDuration);
    float fCos = cosf(fAngRad);
    float fSin = sinf(fAngRad) ;

    glm::mat3 theMat(1.0f);
    theMat[0].x = fCos; theMat[2].x = -fSin;
    theMat[0].z = fSin; theMat[2].z = fCos;
    return theMat;
}
