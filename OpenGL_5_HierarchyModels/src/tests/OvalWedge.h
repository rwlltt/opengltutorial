//
// Created by rodney on 10/05/2020.
//

#ifndef OPENGL_TUT_MODELTRANSLATION_OVALWEDGE_H
#define OPENGL_TUT_MODELTRANSLATION_OVALWEDGE_H


#include "WedgeInstance.h"

class OvalWedge : public WedgeInstance {
public:
    OvalWedge(glm::vec3 offset);
    glm::vec3 OffsetXYZ(float fElapsedTime) override;
    glm::mat3 CalcRotation(float fElapsedTime) override;
};


#endif //OPENGL_TUT_MODELTRANSLATION_OVALWEDGE_H
