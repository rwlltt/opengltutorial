//
// Created by rodney on 20/05/2020.
//

#include "SampleWedge.h"


SampleWedge::SampleWedge(glm::vec3 offset, short scale_Type):
    WedgeInstance{offset}, scale_type(scale_Type) {

}

glm::vec3 SampleWedge::getOffset() {
    return offset;
};

glm::vec3 SampleWedge::CalcScale(float fElapsedTime){
    glm::vec3 scale;
    float fLoopDuration = 3.0f;
    float fXLoopDuration = 3.0f;
    float fZLoopDuration = 5.0f;
    switch (scale_type) {
        case 1: {
            scale = glm::vec3(1.0f, 1.0f, 1.0f);
            break;
        }
        case 2: {
            scale = glm::vec3(4.0f, 4.0f, 4.0f);
            break;
        }
        case 3: {
            scale = glm::vec3(0.5f, 1.0f, 10.0f);
            break;
        }
        case 4: {
            scale = glm::vec3(glm::mix(1.0f, 4.0f, CalcLerpFactor(fElapsedTime, fLoopDuration)));
            break;
        }
        case 5: {
            scale = glm::vec3(glm::mix(1.0f, 0.5f, CalcLerpFactor(fElapsedTime, fXLoopDuration)),
                              1.0f,
                              glm::mix(1.0f, 10.0f, CalcLerpFactor(fElapsedTime, fZLoopDuration)));
            break;
        }
        default:
            // nothing
            break;
    }
    return scale;
}

glm::mat3 SampleWedge::CalcRotation(float fElapsedTime){
    glm::mat3 rotation;
    switch (scale_type) {
        case 1: {
            rotation = glm::mat3(1.0f);
            break;
        }
        case 2: {
            float fAngRad = ComputeAngleRad(fElapsedTime, 3.0);
            float fCos = cosf(fAngRad);
            float fSin = sinf(fAngRad);

            glm::mat3 theMat(1.0f);
            theMat[1].y = fCos; theMat[2].y = -fSin;
            theMat[1].z = fSin; theMat[2].z = fCos;
            rotation = theMat;
            break;
        }
        case 3: {
            float fAngRad = ComputeAngleRad(fElapsedTime, 2.0);
            float fCos = cosf(fAngRad);
            float fSin = sinf(fAngRad);

            glm::mat3 theMat(1.0f);
            theMat[0].x = fCos; theMat[2].x = fSin;
            theMat[0].z = -fSin; theMat[2].z = fCos;
            rotation = theMat;
            break;
        }
        case 4: {
            float fAngRad = ComputeAngleRad(fElapsedTime, 2.0);
            float fCos = cosf(fAngRad);
            float fSin = sinf(fAngRad);

            glm::mat3 theMat(1.0f);
            theMat[0].x = fCos; theMat[1].x = -fSin;
            theMat[0].y = fSin; theMat[1].y = fCos;
            rotation = theMat;
            break;
        }
        case 5: {
            float fAngRad = ComputeAngleRad(fElapsedTime, 2.0);
            float fCos = cosf(fAngRad);
            float fInvCos = 1.0f - fCos;
            float fSin = sinf(fAngRad);
            float fInvSin = 1.0f - fSin;

            glm::vec3 axis(1.0f, 1.0f, 1.0f);
            axis = glm::normalize(axis);

            glm::mat3 theMat(1.0f);
            theMat[0].x = (axis.x * axis.x) + ((1 - axis.x * axis.x) * fCos);
            theMat[1].x = axis.x * axis.y * (fInvCos) - (axis.z * fSin);
            theMat[2].x = axis.x * axis.z * (fInvCos) + (axis.y * fSin);

            theMat[0].y = axis.x * axis.y * (fInvCos) + (axis.z * fSin);
            theMat[1].y = (axis.y * axis.y) + ((1 - axis.y * axis.y) * fCos);
            theMat[2].y = axis.y * axis.z * (fInvCos) - (axis.x * fSin);

            theMat[0].z = axis.x * axis.z * (fInvCos) - (axis.y * fSin);
            theMat[1].z = axis.y * axis.z * (fInvCos) + (axis.x * fSin);
            theMat[2].z = (axis.z * axis.z) + ((1 - axis.z * axis.z) * fCos);
            rotation = theMat;
        }
        default:
            // nothing
            break;
    }
    return rotation;
}

float SampleWedge::CalcLerpFactor(float fElapsedTime, float fLoopDuration)
{
    float fValue = fmodf(fElapsedTime, fLoopDuration) / fLoopDuration;
    if(fValue > 0.5f)
        fValue = 1.0f - fValue;

    return fValue * 2.0f;
}

float SampleWedge::ComputeAngleRad(float fElapsedTime, float fLoopDuration)
{
    const float fScale = 3.14159f * 2.0f / fLoopDuration;
    float fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);
    return fCurrTimeThroughLoop * fScale;
}
