#include "JewelTranslation.h"
#include <Renderer.h>
#include "imgui/imgui.h"
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "StationaryWedge.h"
#include "OvalWedge.h"
#include "BottomCircleWedge.h"

#define ARRAY_COUNT( array ) (sizeof( array ) / (sizeof( array[0] ) * (sizeof( array ) != sizeof(void*) || sizeof( array[0] ) <= sizeof(void*))))
#define GREEN_COLOR 0.75f, 0.75f, 1.0f, 1.0f
#define BLUE_COLOR 	0.0f, 0.5f, 0.0f, 1.0f
#define RED_COLOR 1.0f, 0.0f, 0.0f, 1.0f
#define GREY_COLOR 0.8f, 0.8f, 0.8f, 1.0f
#define BROWN_COLOR 0.5f, 0.5f, 0.0f, 1.0f

namespace test {


    JewelTranslation::JewelTranslation()
            : vertexData{
            +1.0f, +1.0f, +1.0f,
            -1.0f, -1.0f, +1.0f,
            -1.0f, +1.0f, -1.0f,
            +1.0f, -1.0f, -1.0f,

            -1.0f, -1.0f, -1.0f,
            +1.0f, +1.0f, -1.0f,
            +1.0f, -1.0f, +1.0f,
            -1.0f, +1.0f, +1.0f,

            GREEN_COLOR,
            BLUE_COLOR,
            RED_COLOR,
            BROWN_COLOR,

            GREEN_COLOR,
            BLUE_COLOR,
            RED_COLOR,
            BROWN_COLOR,
    }, indexData{
            0, 1, 2,
            1, 0, 3,
            2, 3, 0,
            3, 2, 1,

            5, 4, 6,
            4, 5, 7,
            7, 6, 4,
            6, 7, 5,
    },
              shader{"res/shaders/JewelTranslation.shader"},
              vb{vertexData, 56 * sizeof(vertexData[0]), false},
              ib{indexData, 24},
              numberOfVertices{8},
              wedgeInstances{std::make_unique<StationaryWedge>(glm::vec3(0.0f, 0.0f, -20.0f)),
                             std::make_unique<OvalWedge>(glm::vec3(-5.0f,  0.0f, -20.0f)),
                             std::make_unique<BottomCircleWedge>(glm::vec3(0.0f, 5.0f, -20.0f))},
              translationA{0.0, 0.0, 0.0},
              translationB{0.0, -5.0, -10.0} {
        size_t colorDataOffset = sizeof(float) * 3 * numberOfVertices;
        vb.Bind(); // set the GL_ARRAY_BUFFER binding once

        va.Bind(); // element buffer binding is part of the VAO state,
        GLCall(glEnableVertexAttribArray(0));
        GLCall(glEnableVertexAttribArray(1));
        GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0));
        GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void *) colorDataOffset));
        ib.Bind();  //  GL_ELEMENT_ARRAY_BUFFER is set for each VAO
        va.UnBind();
    }

    JewelTranslation::~JewelTranslation() {
    }

    void JewelTranslation::OnUpdate(float deltaTime) {
        // Store the delta
        this->deltaTime = deltaTime;
    }

    void JewelTranslation::OnRender() {
        shader.Bind();
        va.Bind();

        for (int iLoop = 0; iLoop < ARRAY_COUNT(wedgeInstances); iLoop++) {
            auto currInst = wedgeInstances[iLoop].get();

            glm::vec3 offset = glm::vec3(currInst->OffsetXYZ(deltaTime));
            glm::mat4 rotate(1.0f);
            rotate[3] = glm::vec4(offset, 1.0f);

            glm::mat3 rotMatrix = currInst->CalcRotation(deltaTime);
            glm::mat4 theMat(rotMatrix);
            theMat[3] = glm::vec4(translationA, 1.0f); // w col filled with translate to pos

            glm::mat4 m2c = rotate * theMat;
            shader.SetUniformMat4f("modelToCameraMatrix", m2c);
            GLCall(glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0));
        }

//        shader.SetUniform3f("offset", translationA.x, translationA.y, translationA.z );
//        GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, 0));

        va.UnBind();
    }

    void JewelTranslation::OnImGuiRender() {
        ImGui::SliderFloat3("Offset 1", &translationA.x, -20.0f, 20.0f);
        ImGui::SliderFloat3("Offset 2", &translationB.x, -3.0f, 2.0f);
    }

}