#ifndef TUT_SHADER_
#define TUT_SHADER_

#include <string>
#include "linmath.h"
#include <unordered_map>
#include "glm/glm.hpp"

struct ShaderProgramSource {
	std::string VertexSource;
	std::string FragmentSource;
};

class Shader {
	std::string m_FilePath;
	unsigned int m_RendererID;
	// caching for uniforms
	std::unordered_map<std::string, int> m_UniformLocationCache;

public:
	Shader(const std::string &filepath);
	~Shader();

	void Bind() const;
	void UnBind() const;

	//Set uniforms
	void SetUniform1i(const std::string& name, int value);
	void SetUniform1f(const std::string& name, float value);
    void SetUniform2f(const std::string& name, float value1, float value2);
    void SetUniform3f(const std::string& name, float value1, float value2, float value3);
	void SetUniformM4f(const std::string& name, const mat4x4 &matrix);
	void SetUniformMat4f(const std::string& name, const glm::mat4& matrix);
private:
	ShaderProgramSource ParseShader(const std::string & filepath);
	unsigned int CompileShader(unsigned int type, const std::string & source);
	unsigned int CreateShader(const std::string & vertexShader, const std::string & fragmentShader);
	int GetUniformLocation(const std::string& name);

};


#endif