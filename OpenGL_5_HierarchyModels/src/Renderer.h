#ifndef TUT_RENDERER_
#define TUT_RENDERER_

#include <GLAD/glad.h> 
#include "VertexArray.h"
#include "IndexBuffer.h"
#include "Shader.h"

#define ASSERT(x) if (!(x)) __debugbreak();
#define GLCall(x) GLClearError();\
	x;\
	ASSERT(GLLogCall(#x, __FILE__, __LINE__))


void  GLClearError();

bool GLLogCall(const char* function, const char* file, int line);

class Renderer {
private:
    const float fFrustumScale;
    //const float fzNear;
    //const float fzFar;
    glm::mat4 cameraToClipMatrix;
    //Simple list of shaders registered
    std::vector<Shader*> shaders;

public:
    Renderer();
    ~Renderer();
	void ViewPort(int width, int height);
	void Clear() const;
	void Draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader) const;
    void RegisterShader(Shader *shader);
private:
    float CalcFrustumScale(float fFovDeg);
};


#endif
