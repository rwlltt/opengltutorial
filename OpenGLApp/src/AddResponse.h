#ifndef TUT_ADDRESPONSE_
#define TUT_ADDRESPONSE_

#include "Response.h"
#include "storage/DBStorage.h" 
#include "Context.h"

class AddResponse : public Response
{
	std::stringstream output_;

public:
	AddResponse();
	virtual ~AddResponse();

	AddResponse& operator= (const AddResponse &other) = delete;
	AddResponse* operator&(AddResponse object) = delete;


public:
	void Display() const;
	std::tuple < std::string, std::set<char>, std::map<char, std::string>> Prompt() const ;

	std::string movieID{ "" };

};

#endif /* TUT_ADDRESPONSE_ */