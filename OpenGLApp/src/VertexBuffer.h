#ifndef TUT_VERTEXBUFFER_
#define TUT_VERTEXBUFFER_

class VertexBuffer {
	unsigned int m_RendererID;

public:
	VertexBuffer(const void* data, unsigned int size);
	~VertexBuffer();

	void Bind() const;
	void UnBind() const;

};


#endif