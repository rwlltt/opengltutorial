#ifndef TUT_TEST_TRIANGLE
#define TUT_TEST_TRIANGLE

#include <Shader.h>
#include <VertexBuffer.h>
#include <VertexArray.h>
#include <VertexBufferLayout.h>
#include "Test.h"

namespace test {

	class TestTriangle : public Test {
	private:
        float vertexPositions[36];
        Shader shader;
        VertexBuffer vb;
        VertexArray va;
        VertexBufferLayout layout;

    public:
        TestTriangle();
		~TestTriangle();

		void OnUpdate(float deltaTime) override;
		void OnRender() override;
		void OnImGuiRender() override;

	};


}

#endif /* TUT_TEST_TRIANGLE*/

