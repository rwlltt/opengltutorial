#include <imgui/imgui.h>
#include "TestSquare.h"
#include "glm/gtc/matrix_transform.hpp"

namespace test {

    TestSquare::TestSquare()
             : vertices{
                     -75.0f, -75.0f, 0.0f, 0.0f, 1.f, 0.f, 0.f ,
                      75.0f, -75.0f, 1.0f, 0.0f, 0.f, 1.f, 0.f ,
                      75.0f,  75.0f, 1.0f, 1.0f, 0.f, 0.f, 1.f ,
                     -75.0f,  75.0f, 0.0f, 1.0f, 0.f, 1.f, 1.f },
              indices {
                            0, 1, 2,
                            2, 3, 0
                    },
              shader {"res/shaders/Basic.shader"},
              texture {"res/textures/wooden-texture.png"},
              vb {vertices, 28 * sizeof(vertices[0]) },
              ib {indices, 6},
              proj {glm::ortho(0.0f, 640.0f, 0.0f, 480.0f, -1.0f, 1.0f)},
              view {glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0.2))},
              translationA {220, 120, 0},
              translationB{290, 120, 0}
    {
        // Three locations in vertex shader
        layout.Push<float>(2);
        layout.Push<float>(2);
        layout.Push<float>(3);
        va.AddBuffer(vb, layout);
    }

    TestSquare::~TestSquare()
    {
    }

    void TestSquare::OnUpdate(float deltaTime)
    {
    }

    void TestSquare::OnRender()
    {
        texture.Bind();

        shader.Bind();
        va.Bind();
        {
            glm::mat4 model = glm::translate(glm::mat4(1.0f), translationA);
            glm::mat4 s_mvp = proj * view * model;
            shader.Bind();
            shader.SetUniformMat4f("u_MVP", s_mvp);
            //shader.Bind();
            va.Bind();
            ib.Bind();
            GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));

        }
        {
            glm::mat4 model = glm::translate(glm::mat4(1.0f), translationB);
            glm::mat4 s_mvp = proj * view * model;
            shader.Bind();
            shader.SetUniformMat4f("u_MVP", s_mvp);
            //shader.Bind();
            va.Bind();
            ib.Bind();
            GLCall(glDrawElements(GL_TRIANGLES, ib.GetCount(), GL_UNSIGNED_INT, nullptr));
        }
    }

    void TestSquare::OnImGuiRender()
    {
        //ImGui::ColorEdit4("Clear Colour", m_clearColour);
        {
            ImGui::SliderFloat3("Translation A", &translationA.x, 0.0f, 640.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
            ImGui::SliderFloat3("Translation B", &translationB.x, 0.0f, 640.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
        }

    }

}