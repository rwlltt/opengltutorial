#ifndef TUT_APPLICATION_
#define TUT_APPLICATION_

/* Modern OpenGL with GLAD */
#include <GLAD/glad.h> /* GLAD before glfw3 for GL.h */
#include <GLFW/glfw3.h>

#include <iostream>
#include "io_utility.h"
#include "loki/Singleton.h"
#include "loki/Factory.h"
//#include "loki/Functor.h"
#include "Request.h"
#include "Response.h"
#include "Model.h"
#include "Renderer.h"
#include <map>
#include <queue>
#include <winsock.h>
#include <signal.h>


using namespace Loki;


template
<
	class AbstractProduct,
	typename IdentifierType,
	typename ProductCreator = AbstractProduct * (*)(),
	template<typename, class> class FactoryErrorPolicy = DefaultFactoryError
>
class AppFactory : public FactoryErrorPolicy<IdentifierType, AbstractProduct>
{
	typedef std::map<IdentifierType, ProductCreator> IdToProductMap;
	IdToProductMap associations_;

public:

	AppFactory()
		: associations_()
	{
	}

	~AppFactory()
	{
		associations_.erase(associations_.begin(), associations_.end());
	}

	bool Register(const IdentifierType& id, ProductCreator creator)
	{
		return associations_.insert(
			typename IdToProductMap::value_type(id, creator)).second != 0;
	}

	bool Unregister(const IdentifierType& id)
	{
		return associations_.erase(id) != 0;
	}

	std::vector<IdentifierType> RegisteredIds()
	{
		std::vector<IdentifierType> ids;
		for (typename IdToProductMap::iterator it = associations_.begin();
			it != associations_.end(); ++it)
		{
			ids.push_back(it->first);
		}
		return ids;
	}

	AbstractProduct* CreateObject(const IdentifierType& id)
	{
		typename IdToProductMap::iterator i = associations_.find(id);
		if (i != associations_.end())
			return (i->second)();
		return this->OnUnknownType(id);
	}

};

typedef SingletonHolder< AppFactory<Request, std::string>, Loki::CreateUsingNew, Loki::DefaultLifetime, Loki::SingleThreaded > RequestFactory;
typedef SingletonHolder< AppFactory<Response, std::string>, Loki::CreateUsingNew, Loki::DefaultLifetime, Loki::SingleThreaded > ResponseFactory;

struct AFunctor {
	void operator()(int) {

	}
};

extern AFunctor fn;

class Application : public ClassLevelLockable<Application>
{
private:
	typedef CreateUsingNew<Application> Creator;
	Application();
	virtual ~Application();

	std::queue<std::string> messages{};
	std::unique_ptr<User> m_user{};
	std::string m_user_name{ "anonymous" };

	const char* glsl_version{ "#version 330" }; // GL 3.0 + GLSL 130
	GLFWwindow* window{ nullptr };


public:
	Application(const Application& other) = delete;
	Application& operator= (const Application &other) = delete; 
	Application* operator&(Application object) = delete;

	friend class Creator;


public:
	void Init();
	void Run();
	void SendAppMessage(const std::string &request);

private:
	std::future < std::unique_ptr<Response>> DoRequest(const std::string &user, const std::string &request);

public:
	static void error_callback(int error, const char* description)
	{
		fprintf(stderr, "Error: %s\n", description);
	}

	static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
			glfwSetWindowShouldClose(window, GLFW_TRUE);
	}

//    static void reshape_callback(GLFWwindow* window, int x, int y)
//    {
//        //renderer2.ViewPort(x,y);
//        fprintf(stderr, "reshape_callback.\n");
//    }

};


/*    */
typedef SingletonHolder< Module::Model, Loki::CreateUsingNew, Loki::DefaultLifetime, Loki::ClassLevelLockable> Model;
typedef SingletonHolder< Application, Loki::CreateUsingNew, Loki::DefaultLifetime, Loki::ClassLevelLockable > TheApp;


namespace {
	void signal_handler(int sig) {
		TheApp::Instance().SendAppMessage("Quit");
	}
}

#endif /* TUT_APPLICATION_ */